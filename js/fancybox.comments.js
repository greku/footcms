$(document).ready(function() {
	
    $("a.fancy_comments").fancybox({
		'width'				: 550,
		'height'			: '75%',
        'autoScale'     	: false,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});

	
});
