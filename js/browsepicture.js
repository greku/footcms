$(document).ready(function() {

    $(".fileBrowser a.browsepicture").fancybox({
            'width'             : 750,
            'height'            : '75%',
            'autoScale'         : false,
            'transitionIn'      : 'none',
            'transitionOut'     : 'none',
            'type'              : 'iframe',
            'showCloseButton'   : false,
            'onCleanup'         : function(e) {
                var filepath = $('#fancybox-frame').contents().find('#fileselector input[name=selectedFilePath]').val();
                var fileid = $('#fancybox-frame').contents().find('#fileselector input[name=selectedFileId]').val();
                if (fileid!='cancel') {
                    if (fileid!='') {
                        $(e).parent('.fileBrowser').children('span.selPath').html(filepath);
                        $(e).parent('.fileBrowser').children('span.noSelection').hide();
                        $(e).parent('.fileBrowser').children('input.fileId').val(fileid);
                        $(e).parent('.fileBrowser').children('input.filePath').val(filepath);
                    }
                    else {
                        $(e).parent('.fileBrowser').children('span.selPath').html('');
                        $(e).parent('.fileBrowser').children('span.noSelection').show();
                        $(e).parent('.fileBrowser').children('input.fileId').val('0');
                        $(e).parent('.fileBrowser').children('input.filePath').val('');
                    }
                }
                //~ $('.fileExplorer span').html(path);
             }

    });

    $(".fileBrowser input.fileId").each(function(){
        if ($(this).val()!="" && $(this).val()!=0){
            $(this).parent('.fileBrowser').children('span.noSelection').hide();
        }
    });

    $(".fileBrowser a.nofile").click(function () {
        $(this).parent('.fileBrowser').children('span.selPath').html('');
        $(this).parent('.fileBrowser').children('span.noSelection').show();
        $(this).parent('.fileBrowser').children('input.fileId').val('0');
        $(this).parent('.fileBrowser').children('input.filePath').val('');
    });
});
