/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Select file in explorer
 */

$(document).ready(function () {
    $('#fileselector div.file').click(function() {
        $('#fileselector div.file').removeClass('fileSelected');
        $(this).addClass('fileSelected');
        var filename=$(this).children('span').html();
        var folderpath = $('#fileselector span.folderPath').html();
        var numbtxt=$(this).children('span').attr('class');
        var fileId = numbtxt.split("-")[1];
        $('#fileselector input[name=selectedFilePath]').attr('value', folderpath+filename);
        $('#fileselector input[name=selectedFileId]').attr('value', fileId);
    });
    $("#fileselector div.file").hover(
      function () {
        $(this).addClass('fileHover');
      },
      function () {
        $(this).removeClass('fileHover');
      }
    );

    $("#fileselector input#buttonOK").click(function() {
        parent.jQuery.fancybox.close();
    });

    $("#fileselector input#buttonCancel").click(function() {
        $('#fileselector input[name=selectedFileId]').attr('value', 'cancel');
        parent.jQuery.fancybox.close();
    });
});
