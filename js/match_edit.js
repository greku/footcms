/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * Match edit js
 */

function report_refresh_options1() {
    var options='';
    $('#players1 :checkbox').each(function(){
            var num=this.value;
            if($('#players1 :checkbox[value='+num+']:checked').size()>0){
                if($('[id^=goaler1] option[value='+num+']').size()<1){
                    $('[id^=goaler1]').append('<option value="'+num+'">'+$('#nameplayers1'+num).text()+'</option>\n');
                }
                if($('[id^=cardplayer1] option[value='+num+']').size()<1){
                    $('[id^=cardplayer1]').append('<option value="'+num+'">'+$('#nameplayers1'+num).text()+'</option>\n');
                }
            }
            else{
                if($('[id^=goaler1] option[value='+num+']').size()>0){
                    $('[id^=goaler1] option[value='+num+']').remove();
                }
                if($('[id^=cardplayer1] option[value='+num+']').size()>0){
                    $('[id^=cardplayer1] option[value='+num+']').remove();
                }
            }
        });
}

function report_refresh_options2() {
    var options='';
    $('#players2 :checkbox').each(function(){
            var num=this.value;
            if($('#players2 :checkbox[value='+num+']:checked').size()>0){
                if($('[id^=goaler2] option[value='+num+']').size()<1){
                    $('[id^=goaler2]').append('<option value="'+num+'">'+$('#nameplayers2'+num).text()+'</option>\n');
                }
                if($('[id^=cardplayer2] option[value='+num+']').size()<1){
                    $('[id^=cardplayer2]').append('<option value="'+num+'">'+$('#nameplayers2'+num).text()+'</option>\n');
		}
            }
            else{

                if($('[id^=goaler2] option[value='+num+']').size()>0){
                    $('[id^=goaler2] option[value='+num+']').remove();
                }
                if($('[id^=cardplayer2] option[value='+num+']').size()>0){
                    $('[id^=cardplayer2] option[value='+num+']').remove();
		}
            }
        });
}

function changeScore(id){
    var score=$('#score'+id).val();
    if(parseInt(score)!=parseFloat(score) || !isFinite(score) || score==""){
        $('#score'+id).val('');
        $('#scoreerror').show();
        return false;
    }
    //$('#debug').html($('#debug').html());
    //$('#debug').html('blabli');
    $('#scoreerror').hide();
    var nbgoalers=$('#goalersList'+id+' select').size();
    if(nbgoalers>score){

        for(var i=score;i<nbgoalers;i++){
            $('#goalersList'+id+' span[id=gspan'+id+i+']').remove();
        }
    }
    else if (nbgoalers<score){
        //making the player list
        var players="";
        players+='<option value="0">&nbsp;</option>';
        $('#players'+id+' :checkbox').each(function(){
            var num=this.value;
            if($('#players'+id+' :checkbox[value='+num+']:checked').size()>0){
                players+='<option value="'+num+'">'+$('#nameplayers'+id+num).text()+'</option>\n';
            }
        });

        for(var i=nbgoalers;i<score;i++){
            $('#gspan'+id+'type').clone().attr('id','gspan'+id+i).appendTo('#goalersList'+id).show();
            $('#goalersList'+id+' span[id=gspan'+id+i+'] select').attr('name','goaler'+id+i).attr('id','goaler'+id+i).html(players);
            $('#goalersList'+id+' span[id=gspan'+id+i+'] :text').attr('name','goalmin'+id+i);
            
            //$('#goalersList'+id+' span[id=gspan'+id+i+']').append('<span id=""><select id=""></select></span>');
        }
    }
    return true;
}

function addCardField(id)
{
    //making the player list
    var players="";
    players+='<option value="0">&nbsp;</option>';
    $('#players'+id+' :checkbox').each(function(){
        var num=this.value;
        if($('#players'+id+' :checkbox[value='+num+']:checked').size()>0){
            players+='<option value="'+num+'">'+$('#nameplayers'+id+num).text()+'</option>\n';
        }
    });
    //last card id
    var nbcards=$('#cardsList'+id+' select[id^=cardplayer]').size();
    //add a card field
    $('#cspan'+id+'type').clone().attr('id','cspan'+id+nbcards).appendTo('#cardsList'+id).show();
    $('#cardsList'+id+' span[id=cspan'+id+nbcards+'] select[name=player]').attr('name','cardplayer'+id+nbcards).attr('id','cardplayer'+id+nbcards).html(players);
    $('#cardsList'+id+' span[id=cspan'+id+nbcards+'] select[name=color]').attr('name','cardcolor'+id+nbcards).attr('id','cardcolor'+id+nbcards);
    $('#cardsList'+id+' span[id=cspan'+id+nbcards+'] :text').attr('name','cardmin'+id+nbcards);
}

$(document).ready(function () {
    $('#scoreerror').hide();
    $('#gspan1type').hide();
    $('#gspan2type').hide();
    $('#cspan1type').hide();
    $('#cspan2type').hide();
    $('#players1 :checkbox').change(function () {report_refresh_options1()});
    $('#players2 :checkbox').change(function () {report_refresh_options2()});
    $('#score1').change(function () {changeScore('1')});
    $('#score2').change(function () {changeScore('2')});
    $('#addCard1').click(function () {addCardField('1')});
    $('#addCard2').click(function () {addCardField('2')});
    //report_refresh_story();
    report_refresh_forfeit();
    report_refresh_options1();
    report_refresh_options2();
});
