<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class FCMS_Config extends CI_Config {
    private $fcmsConfig;

    function __construct(){
        parent::__construct();
        $fcmsConfig=array();
    }

    function loadDb(&$db){
        $request="SELECT `settings`.`name`, `settings`.`value`,`modules`.`name` AS `moduleName` "."\n"
            ."FROM `".$db->dbprefix."settings` AS `settings`"."\n"
            ."LEFT JOIN `".$db->dbprefix."modules` AS `modules`"."\n"
            ."ON `modules`.`id`=`settings`.`moduleId`";
        $query = $db->query($request);
        if ($query===false)
            die ('Database not accessible');
        $data = $query->result_array();
        $this->fcmsConfig=array();
        foreach($data as $item)
            $this->fcmsConfig[$item['moduleName'].$item['name']] = $item['value'];
        log_message('debug', 'Config from db loaded');
        return TRUE;
    }

    function fcms($name, $module=null){
        return isset($this->fcmsConfig[$module.$name])?$this->fcmsConfig[$module.$name]:null;
    }
}

?>
