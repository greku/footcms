<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/* load the MX_Loader class */
require APPPATH."core/MCI_Loader.php";

class FCMS_Loader extends MCI_Loader {

    var $templateStyle;
    var $jsFiles;
    var $cssFiles;

    public function __construct(){
        $this->templateStyle='default';
        $this->jsFiles=array();
        $this->cssFiles=array();
        parent::__construct();
    }

    public function view($view, $vars = array(), $return = FALSE) {
        $view=$this->templateStyle.'/'.$view;
        $vars['images_path']=base_url().'images/'.$this->templateStyle.'/';
        return parent::view($view, $vars, $return);
    }

    /*
     * Load a javascript script
     */
    public function javascript($lib){
        if(!is_array($lib))
            $lib=array($lib);
        foreach($lib as $item){
            if(!array_key_exists($item, $this->jsFiles)){
                if(file_exists(FCPATH.'js/'.$this->templateStyle.'/'.$item.'.js')){
                    $this->jsFiles[$item]=array();
                    $this->jsFiles[$item]['src']=base_url().'js/'.$this->templateStyle.'/'.$item.'.js';
                }
                else if(file_exists(FCPATH.'js/'.$item.'.js')){
                    $this->jsFiles[$item]=array();
                    $this->jsFiles[$item]['src']=base_url().'js/'.$item.'.js';
                }
            }
        }
    }

    public function getJsFiles(){
        return $this->jsFiles;
    }

    /*
     * Load css file
     */
    public function stylesheet($file){
        if(!is_array($file))
            $file=array($file);
        foreach($file as $item){
            if(!array_key_exists($item, $this->cssFiles)){
                if(file_exists(FCPATH.'css/'.$this->templateStyle.'/'.$item.'.css')){
                    $this->cssFiles[$item]=array();
                    $this->cssFiles[$item]['src']=base_url().'css/'.$this->templateStyle.'/'.$item.'.css';
                }
                else if(file_exists(FCPATH.'css/'.$item.'.css')){
                    $this->cssFiles[$item]=array();
                    $this->cssFiles[$item]['src']=base_url().'css/'.$item.'.css';
                }
            }
        }
    }

    public function getCssFiles(){
        return $this->cssFiles;
    }
}
