<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

require APPPATH.'core/MCI_Controller.php';


define('FCMS_VERSION_MAJOR', 1);
define('FCMS_VERSION_MINOR', 4);
define('FCMS_VERSION_REVISION', 2);

class FCMS_Controller extends MCI_Controller {
    private $layout_data;
    private $zone;
    private $zone_template;
    private $fcmsConfig;
    private $CI;

    function __construct(){
        parent::__construct();
        $this->CI =& get_instance();
        /* Init */
        $this->zone_template=array(
                'site'=>'site',
                'site_frame'=>'site_frame',
                'admin'=>'admin',
                'admin_frame'=>'admin_frame'
                );
        $this->zone='site';

        /* Load libs */
        $this->load->database();
        $this->load->library('session');
        $this->load->library('authentication');
        $this->load->helper('language');
        $this->load->helper('url');
        $this->load->helper('common');

        /* Load database settings */
        $this->config->loadDb($this->db);

        /* Load language */
        $langconf=$this->config->fcms('language');
        if(!is_null($langconf))
            $this->config->set_item('language', $langconf);
        $this->lang->load('site');

        /* Set timezone */
        date_default_timezone_set('Europe/Brussels');
        setlocale(LC_ALL, lang('setting_locale').'.'.'utf8');

        /* Set template */
        $template=$this->config->fcms('template');
        if(is_null($template))
            $template='default';
        $this->CI->load->templateStyle=$template;

        /* Init layout data */
        $this->layout_data['club_name']=$this->config->fcms('myClubName');
        $this->layout_data['header_css']=array();
        $this->layout_data['header_js']=array();
        $this->layout_data['css_folder']=base_url().'css/'.$template.'/';
        $this->layout_data['header_title']=$this->config->fcms('myClubName');
        $this->layout_data['header_more']='';
        $this->layout_data['navigation_menu']=array();
        $this->layout_data['breadcrumb']=array();
        $this->layout_data['page_content']='';
        $this->layout_data['footer_content']='<a href="http://footcms.org">footcms</a> '.FCMS_VERSION_MAJOR.'.'.FCMS_VERSION_MINOR.'.'.FCMS_VERSION_REVISION.' - Page rendered in {elapsed_time} seconds';
        /* Check version */
        $this->checkVersion();
        /* Check install */
        $this->checkInstall();
        /* Check authentication */
        $this->authentication->checkAuthentication();
    }

    /*
     * Set content of the page
     */
    public function setContent($content){
        $this->layout_data['page_content']=$content;
    }

    /*
     * Set breadcrumb of the page
     */
    public function setBreadcrumb($breadcrumb){
        $this->layout_data['breadcrumb']=$breadcrumb;
    }

    /*
     * Set breadcrumb of the page
     */
    public function appendBreadcrumb($name, $link=''){
        $this->layout_data['breadcrumb'][]=array('name'=>$name, 'link'=>$link);
    }

    /*
     * Set permissions (return true or render not allowed and exit)
     */
    public function setPermissions($permissions, $module=null){
        if(!is_array($permissions))
            $permissions=array($permissions);
        foreach ($permissions as $permission) {
            if(!$this->authentication->hasPermission($permission, $module)) {
                /* Instanciate an error controller */
                if($this->zone=='site')
                    $this->setContent($this->load->view('access_forbidden', array(), true));
                elseif($this->zone=='admin')
                    $this->setContent($this->load->view('admin/access_forbidden', array(), true));
                $this->render();
                global $OUT;
                /* Render and exit */
                $OUT->_display();
                exit();
            }
        }
        return true;
    }

    /*
     * Set zone (for layout view)
     */
    public function setZone($zone){
        if(array_key_exists($zone, $this->zone_template)){
            if($zone=='admin' || $zone=='admin_frame'){
                if(!$this->authentication->isAuthenticated()){
                    $this->zone='site';
                    /* Instanciate an error controller */
                    include(APPPATH.'controllers/session'.EXT);
                    $CI2 = new Session();
                    /* Call show_404 */
                    $CI2->login();
                    global $OUT;
                    /* Render and exit */
                    $OUT->_display();
                    exit();
                }
                elseif (!$this->authentication->hasPermission('viewadmin')){
                    $this->zone='site';
                    /* Instanciate an error controller */
                    $this->setContent($this->load->view('access_forbidden', array(), true));
                    $this->render();
                    global $OUT;
                    /* Render and exit */
                    $OUT->_display();
                    exit();
                }
                else {
                    $this->lang->load('admin');
                    $this->zone=$zone;
                }
            }
            else {
                $this->zone=$zone;
            }
        }
        else
            return false;
    }

    /*
     * Render the page
     */
    public function render(){
        foreach($this->CI->load->getJsFiles() as $item)
            $this->layout_data['header_js'][]=$item['src'];
        foreach($this->CI->load->getCssFiles() as $item)
            $this->layout_data['header_css'][]=$item['src'];
        $this->layout_data['navigation_menu']=$this->loadMenu();
        header('Content-Type: text/html; charset=utf-8');
        $this->load->view('layouts/'.$this->zone_template[$this->zone], $this->layout_data);
    }



    /*
     * Private methods
     */
    private function loadMenu(){
        if($this->zone == 'site'){
            $request="SELECT `menu`.`text` as `name`, `menu`.`link` as `link`, `modules`.`name` as `moduleName` "."\n"
                ."FROM `".$this->db->dbprefix."menu` as `menu`"."\n"
                ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
                ."ON `modules`.`id`=`menu`.`moduleId`"."\n"
                ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
                ."AND `show`=1 AND `admin`=0"."\n"
                ."ORDER BY `rank` ASC";
            $query = $this->db->query($request);
            $result = $query->result_array();
            foreach ($result as $r){
                if(!is_null($r['moduleName'])){
                    if(file_exists(str_replace('system/','', BASEPATH).'application/modules/'.$r['moduleName'])){
                        $this->load->module($r['moduleName']);
                        $this->$r['moduleName']->lang('mini');
                    }
                }
             }
            return $result;
        }
        elseif ($this->zone == 'admin'){
            $request="SELECT `menu`.`id`, `menu`.`text` as `name`,"."\n"
                ."`menu`.`link` as `link`, `menu`.`parent`, "."\n"
                ."`modules`.`name` AS `moduleName` "."\n"
                ."FROM `".$this->db->dbprefix."menu` as `menu`"."\n"
                ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
                ."ON `modules`.`id`=`menu`.`moduleId`"."\n"
                ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
                ."AND `show`=1 AND `admin`=1"."\n"
                ."ORDER BY `rank` ASC, `parent` ASC";
            $query = $this->db->query($request);
            $elements = $query->result_array();
            $menu = array();
            $parents = array();
            foreach ($elements as $item){
                if((int)$item['parent']==0){
                    $menu[$item['id']]=$item;
                    unset($menu[$item['id']]['id']);
                    unset($menu[$item['id']]['parent']);
                }
                else {
                    $sub_menu=$item;
                    unset($sub_menu['id']);
                    unset($sub_menu['parent']);
                    $menu[$item['parent']]['sub_menu'][]=$sub_menu;
                }
            }
            foreach ($elements as $r){
                if(!is_null($r['moduleName'])){
                    if(file_exists(str_replace('system/','', BASEPATH).'application/modules/'.$r['moduleName'])){
                        $this->load->module($r['moduleName']);
                        $this->$r['moduleName']->lang('mini');
                    }
                }
             }
            return $menu;
        }
    }

    /*
     * Check version
     */
    private function checkVersion(){
        if($this->config->fcms('version_major')!=FCMS_VERSION_MAJOR ||
           $this->config->fcms('version_minor')!=FCMS_VERSION_MINOR ||
           $this->config->fcms('version_revision')!=FCMS_VERSION_REVISION){
            // bad version!
            $this->zone='site';
            /* Instanciate an error controller */
            $this->setContent($this->load->view('version_incompatible', array(), true));
            $this->render();
            global $OUT;
            /* Render and exit */
            $OUT->_display();
            exit();
        }
        return true;
    }

    /*
     * Check install controller
     */
    private function checkInstall(){
        if ($this->config->fcms('ignoreInstall')=='yes')
            return true;
        if(file_exists(str_replace('system/','', BASEPATH).'application/controllers/install/install.php')){
            // install file must be deleted
            $this->zone='site';
            /* Instanciate an error controller */
            $this->setContent($this->load->view('install_file_present', array(), true));
            $this->render();
            global $OUT;
            /* Render and exit */
            $OUT->_display();
            exit();
        }
        return true;
    }
}
?>
