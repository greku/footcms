<?php

$lang['install_Footcms_install']='Installation de Footcms';
$lang['install_Change_language']='Changer la langue d\'installation';
$lang['install_Footcms_version_being_installed']='Version de Footcms en cours d\'installation';
$lang['install_result_OK']='OK';
$lang['install_result_NOK']='PAS OK';
$lang['install_Compatibility_check']='Vérification de la compatibilité';
$lang['install_Database_connection']='Connexion à la base de données';
$lang['install_Database_selection']='Sélection de la base de données';
$lang['install_Tables_not_in_use']='Tables non-utilisées';
$lang['install_Table with name %s already exists']='La table ayant le nom %s existe déjà.';
$lang['install_GD library']='Librairie GD';
$lang['install_JPEG support']='Support JPEG';
$lang['install_GIF support']='Support GIF';
$lang['install_PNG support']='Support PNG';
$lang['install_Upload folder']='Dossier Upload';
$lang['install_Connection to database failed (host=%s, username=%s, password=%s)']='La connexion à la base de données est impossible (hôte=%s, utilisateur=%s, mot de passe=%s)';
$lang['install_Database selection failed (database=%s)']='La sélection de la base de données est impossible (base de données=%s)';
$lang['install_JPEG support of GD is not available']='GD ne supporte pas JPEG';
$lang['install_GIF support of GD is not available']='GD ne supporte pas GIF';
$lang['install_PNG support of GD is not available']='GD ne supporte pas PNG';
$lang['install_upload folder must be readable and writable']='Le dossier Upload doit être accessible en lecture et écriture.';
$lang['install_Update_from_version_%s_not_supported']='Mise à jour de la version %s non supporté.';
$lang['install_Currently_installed_version_supported_for_update']='Mise à jour possible de la version actuellement installée';
$lang['install_Continue_installation']='Continuer l\'installation';
$lang['install_Continue_update']='Continuer la mise à jour';
$lang['install_Database_configuration']='Configuration de la base de données';
$lang['install_Hostname']='Hôte';
$lang['install_Username']='Nom d\'utilisateur';
$lang['install_Password']='Mot de passe';
$lang['install_Database']='Base de données';
$lang['install_Table prefix']='Préfix des tables';
$lang['install_General_settings']='Paramètres généraux';
$lang['install_Name_of_your_club']='Nom de votre club';
$lang['install_Matricule_of_your_club']='Matricule de votre club';
$lang['install_Administrator_email']='Email administrateur';
$lang['install_Administrator_password']='Mot de passe administrateur';
$lang['install_No-reply_email']='Email No-reply';
$lang['install_Language']='Langue';
$lang['install_lang_english']='anglais';
$lang['install_lang_french']='français';
$lang['install_Install_now']='Installer maintenant';
$lang['install_Installation_success']='L\'installation de Footcms est terminée.';
$lang['install_Installation_complete_with_errors']='L\'installation de Footcms s\'est terminée avec des erreurs.';

?>
