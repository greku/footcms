<?php

$lang['setting_locale']='fr_FR';

$lang['dt_datetimetextlongday']="%A %d %B %Y &agrave; %H:%M";
$lang['dt_datetimetextlong']="%d %B %Y &agrave; %H:%M";
$lang['dt_datetimetextshortday']="%a %d %b %Y &agrave; %H:%M";
$lang['dt_datetimenumeric']="%d/%m/%y %H:%M";
$lang['dt_datetextlong']="%d %B %Y";
$lang['dt_datetextlongday']="%A %d %B %Y";
$lang['dt_datetextshort']="%d %b %y";
$lang['dt_datetextshortday']="%a %d %b %y";
$lang['dt_datenumeric']="%d/%m/%y";
$lang['dt_time']="%H:%M";
$lang['dt_form']="%d/%m/%Y";
$lang['dt_form_time']="%H:%M";
$lang['dt_form_date_humanpattern']="dd/mm/yyyy";
$lang['dt_form_time_humanpattern']="hh:mm";
$lang['dt_form_date_regexp']='/^(?P<day>\d{1,2})\/(?P<month>\d{1,2})\/(?P<year>\d\d(?:\d\d)?)$/';
$lang['dt_form_time_regexp']='/^(?P<hours>\d{1,2})[:.](?P<minutes>\d{2})$/';
$lang['date_Date_format_should_follow_pattern']="La date doit être au format %s.";
$lang['date_Time_format_should_follow_pattern']="L'heure doit être au format %s.";
$lang['date_Date_is_invalid']="La date est incorrecte";

$lang['access_forbidden']="Vous n'avez pas accès à cette page.";
$lang['access_Go_to_home_page']="Retour à la page d'accueil";
$lang['error_Version_incompatible']="Versions des fichiers et base de données différentes";
$lang['error_In_case_you_are_updating']="Si vous suivez une procédure de mise à jour, continuez.";
$lang['error_Install_controller_not_removed']="Fichier d'installation non supprimé";
$lang['error_The install controller must be removed after installation']="Le dossier d'installation doit être supprimé avant que le site ne soit utilisable.";

$lang['Home']="Accueil";
$lang['Team']="Équipe";
$lang['News']="News";
$lang['Matches']="Matchs";
$lang['valid_captcha']="Le mot recopié n'est pas correct";


$lang['captcha_Word_verification']="Vérification du mot";
$lang['captcha_Copy_word_below']="Recopier le mot ci-dessous";

$lang['news_full_story']="Lire la suite";
$lang['news_no_news']="Pas de news";
$lang['news_Latest_news']="Dernières news";
$lang['news_More_news']="Voir toutes les news";

$lang['matches_no_match']="Pas de match";
$lang['matches_date']="Date";
$lang['matches_time']="Heure";
$lang['matches_competition']="Compétition";
$lang['matches_score']="Score";
$lang['matches_teams']="Équipes";
$lang['matches_report']="Rapport";
$lang['match_Upcoming_matches']="Matchs à venir";
$lang['match_%s_vs_%s']="Match %s vs %s";
$lang['match_All_matches']="Voir tous les matchs";
$lang['match_No_upcoming_match']="Pas de match à venir";

$lang['players_goals']="Buts";
$lang['players_no_goal']="Pas de but";
$lang['teams_no_team']="Pas d'équipe";
$lang['teams_players']="Joueurs";
$lang['teams_no_player']="Pas de joueur";
$lang['teams_stats']="Statistiques";
$lang['teams_Team']="Équipe";

$lang['comments_view_comments']="Voir les commentaires";
$lang['comments_no_comments']="Pas de commentaire";
$lang['comments_add_comment']="Ajouter un commentaire";
$lang['comments_post_comment']="Poster un commentaire";
$lang['comments_yourname']="Votre nom";
$lang['comments_comment']="Commentaire";
$lang['comments_post']="Envoyer";
$lang['comments_comment_added']="Votre commentaire a bien été ajouté.";
$lang['comments_wait_validation']="Votre commentaire a été ajouté, il sera visible après validation par le webmaster.";

$lang['report_teams']="Équipes";
$lang['report_undefined_team']="Équipe indéterminée";
$lang['report_goals']="Buts";
$lang['report_no_goal']="Pas de but";
$lang['report_cards']="Cartes";
$lang['report_red']="rouge";
$lang['report_yellow']="jaune";
$lang['report_no_card']="Pas de carte";

$lang['pagination_First']="Première";
$lang['pagination_Last']="Dernière";

$lang['login_Login']="Connexion";
$lang['login_Username']="Nom d'utilisateur";
$lang['login_Password']="Mot de passe";
$lang['login_Sign_in']="Se connecter";
$lang['login_Already_logged_in']="Vous êtes déjà connecté.";
$lang['login_Connect_success']="Vous êtes maintenant connecté.";
$lang['login_timeout_error']="Votre session a expirée. Vous devez vous reconnecter.";
$lang['login_bad_user_pass_error']="Mauvais non d'utilisateur ou mot de passe";
$lang['login_Disconnect_success']="Vous avez bien été déconnecté";
$lang['login_Disconnect_not_logged']="Vous n'êtes pas connecté pour le moment";
$lang['login_Go_to_home_page']="Retour à la page d'accueil";
$lang['login_Go_to_administration']="Aller à l'administration";
$lang['login_Logout']="Déconnexion";

?>
