<?php

$lang['install_Footcms_install']='Footcms installation';
$lang['install_Change_language']='Change language';
$lang['install_Footcms_version_being_installed']='Footcms version being installed';
$lang['install_result_OK']='OK';
$lang['install_result_NOK']='NOT OK';
$lang['install_Compatibility_check']='Compatibility check';
$lang['install_Database_connection']='Database connection';
$lang['install_Database_selection']='Database selection';
$lang['install_Tables_not_in_use']='Tables not in use';
$lang['install_Table with name %s already exists']='Table with name %s already exists';
$lang['install_GD library']='GD library';
$lang['install_JPEG support']='JPEG support';
$lang['install_GIF support']='GIF support';
$lang['install_PNG support']='PNG support';
$lang['install_Upload folder']='Upload folder';
$lang['install_Connection to database failed (host=%s, username=%s, password=%s)']='Connection to database failed (host=%s, username=%s, password=%s)';
$lang['install_Database selection failed (database=%s)']='Database selection failed (database=%s)';
$lang['install_JPEG support of GD is not available']='JPEG support of GD is not available';
$lang['install_GIF support of GD is not available']='GIF support of GD is not available';
$lang['install_PNG support of GD is not available']='PNG support of GD is not available';
$lang['install_upload folder must be readable and writable']='upload folder must be readable and writable';
$lang['install_Update_from_version_%s_not_supported']='Update from version %s not supported';
$lang['install_Currently_installed_version_supported_for_update']='Currently installed version supported for update';
$lang['install_Continue_installation']='Continue installation';
$lang['install_Continue_update']='Continue update';
$lang['install_Database_configuration']='Database configuration';
$lang['install_Hostname']='Hostname';
$lang['install_Username']='Username';
$lang['install_Password']='Password';
$lang['install_Database']='Database';
$lang['install_Table prefix']='Table prefix';
$lang['install_General_settings']='General settings';
$lang['install_Name_of_your_club']='Name of your club';
$lang['install_Matricule_of_your_club']='Matricule of your club';
$lang['install_Administrator_email']='Administrator email';
$lang['install_Administrator_password']='Administrator password';
$lang['install_No-reply_email']='No-reply email';
$lang['install_Language']='Language';
$lang['install_lang_english']='english';
$lang['install_lang_french']='french';
$lang['install_Install_now']='Install now';
$lang['install_Installation_success']='Footcms installation is finished.';
$lang['install_Installation_complete_with_errors']='Footcms installation completed with errors.';

?>
