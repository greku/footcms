<?php

$lang['setting_locale']='en_US';

$lang['dt_datetimetextlongday']="%A %d %B %Y at %I:%M %P";
$lang['dt_datetimetextlong']="%d %B %Y at %I:%M %P";
$lang['dt_datetimetextshortday']="%a %d %b %Y at %I:%M %P";
$lang['dt_datetimenumeric']="%d/%m/%y %I:%M %P";
$lang['dt_datetextlong']="%d %B %Y";
$lang['dt_datetextlongday']="%A %d %B %Y";
$lang['dt_datetextshort']="%d %b %y";
$lang['dt_datetextshortday']="%a %d %b %y";
$lang['dt_datenumeric']="%d/%m/%y";
$lang['dt_time']="%I:%M %P";
$lang['dt_form']="%d/%m/%Y";
$lang['dt_form_time']="%H:%M";
$lang['dt_form_date_humanpattern']="dd/mm/yyyy";
$lang['dt_form_time_humanpattern']="hh:mm";
$lang['dt_form_date_regexp']='/^(?P<day>\d{1,2})\/(?P<month>\d{1,2})\/(?P<year>\d\d(?:\d\d)?)$/';
$lang['dt_form_time_regexp']='/^(?P<hours>\d{1,2})[:.](?P<minutes>\d{2})$/';
$lang['date_Date_format_should_follow_pattern']='The date should follow the pattern %s.';
$lang['date_Time_format_should_follow_pattern']="The time should follow the pattern %s.";
$lang['date_Date_is_invalid']='The date is not valid.';

$lang['access_forbidden']="You don't have access to this page.";
$lang['access_Go_to_home_page']="Go back to home page";
$lang['error_Version_incompatible']="Files and database versions not matching";
$lang['error_In_case_you_are_updating']="In case you'are following an upgrade procedure, please continue.";
$lang['error_Install_controller_not_removed']="Install controller not yet removed";
$lang['error_The install controller must be removed after installation']="The install folder must be removed before the site becomes useable";

$lang['Home']="Home";
$lang['Matches']="Matches";
$lang['Team']="Team";
$lang['News']="News";
$lang['valid_captcha']="The word verification is not correct";


$lang['captcha_Word_verification']="Word verification";
$lang['captcha_Copy_word_below']="Copy the word below";

$lang['news_full_story']="Full story";
$lang['news_no_news']="No news";
$lang['news_Latest_news']="Latest news";
$lang['news_More_news']="More news";

$lang['matches_no_match']="No match";
$lang['matches_date']="Date";
$lang['matches_time']="Time";
$lang['matches_competition']="Competition";
$lang['matches_score']="Score";
$lang['matches_teams']="Teams";
$lang['matches_report']="Report";
$lang['match_Upcoming_matches']="Upcoming matches";
$lang['match_%s_vs_%s']="Match %s vs %s";
$lang['match_All_matches']="View all matches";
$lang['match_No_upcoming_match']="No upcoming match";

$lang['players_goals']="Goals";
$lang['players_no_goal']="No goal";
$lang['teams_no_team']="No team";
$lang['teams_players']="Players";
$lang['teams_no_player']="No player";
$lang['teams_stats']="Statistics";
$lang['teams_Team']="Team";

$lang['comments_view_comments']="View comments";
$lang['comments_no_comments']="No comment";
$lang['comments_add_comment']="Add a comment";
$lang['comments_post_comment']="Post a comment";
$lang['comments_yourname']="Your name";
$lang['comments_comment']="Comment";
$lang['comments_post']="Post";
$lang['comments_comment_added']="Your comment has been added with success.";
$lang['comments_wait_validation']="Your comment has been added, it will be visible after validation by the webmaster.";

$lang['report_teams']="Teams";
$lang['report_undefined_team']="Team undefined";
$lang['report_goals']="Goals";
$lang['report_no_goal']="No goal";
$lang['report_cards']="Cards";
$lang['report_red']="red";
$lang['report_yellow']="yellow";
$lang['report_no_card']="No card";

$lang['pagination_First']="First";
$lang['pagination_Last']="Last";

$lang['login_Login']="Login";
$lang['login_Username']="Username";
$lang['login_Password']="Password";
$lang['login_Sign_in']="Sign in";
$lang['login_Already_logged_in']="You are already logged in.";
$lang['login_Connect_success']="You were successfully connected.";
$lang['login_timeout_error']="Your session has expired due to inactivity. Please reconnect.";
$lang['login_bad_user_pass_error']="Bad username or password";
$lang['login_Disconnect_success']="You were successfully disconnected.";
$lang['login_Disconnect_not_logged']="You are currently not logged in.";
$lang['login_Go_to_home_page']="Go back to home page";
$lang['login_Go_to_administration']="Go to administration";
$lang['login_Logout']="Logout";

?>
