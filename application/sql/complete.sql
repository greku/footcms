-- phpMyAdmin SQL Dump
-- version 3.4.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 20, 2011 at 07:50 AM
-- Server version: 5.1.53
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `footcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_blocks`
--

CREATE TABLE IF NOT EXISTS `tableprefix_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moduleId` int(11) DEFAULT NULL,
  `shortname` varchar(25) NOT NULL,
  `name` varchar(128) NOT NULL,
  `params` text NOT NULL,
  `rank` int(11) NOT NULL,
  `show` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tableprefix_blocks`
--

INSERT INTO `tableprefix_blocks` (`id`, `moduleId`, `shortname`, `name`, `params`, `rank`, `show`) VALUES
(1, NULL, 'latestnews', 'Latest news', 'a:2:{s:6:"nbnews";i:5;s:9:"shorttext";i:1;}', 1, 1),
(2, NULL, 'nextmatch', 'Upcoming matches', 'a:1:{s:7:"nbmatch";i:5;}', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_cards`
--

CREATE TABLE IF NOT EXISTS `tableprefix_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `color` enum('Yellow','Red') NOT NULL DEFAULT 'Yellow',
  `minute` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_clubs`
--

CREATE TABLE IF NOT EXISTS `tableprefix_clubs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `matricule` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matricule` (`matricule`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_comments`
--

CREATE TABLE IF NOT EXISTS `tableprefix_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(25) NOT NULL,
  `date` datetime NOT NULL,
  `comment` text NOT NULL,
  `validated` enum('YES','NO') NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_competitions`
--

CREATE TABLE IF NOT EXISTS `tableprefix_competitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeId` int(11) DEFAULT NULL,
  `seasonId` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_files`
--

CREATE TABLE IF NOT EXISTS `tableprefix_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parentname` (`parent`,`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tableprefix_files`
--

INSERT INTO `tableprefix_files` (`id`, `parent`, `name`, `type`, `date`, `path`) VALUES
(1, 0, 'sample.gif', 4, '2010-01-24 17:05:36', '/sample.gif');

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_file_types`
--

CREATE TABLE IF NOT EXISTS `tableprefix_file_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mime` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `uploadAllowed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mime` (`mime`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tableprefix_file_types`
--

INSERT INTO `tableprefix_file_types` (`id`, `mime`, `description`, `uploadAllowed`) VALUES
(1, 'folder', 'Folder', 0),
(2, 'image/jpeg', 'JPEG Image', 1),
(3, 'image/png', 'PNG Image', 1),
(4, 'image/gif', 'GIF Image', 1),
(5, 'application/pdf', 'PDF File', 1),
(6, 'image/pjpeg', 'IE JPEG image', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_goals`
--

CREATE TABLE IF NOT EXISTS `tableprefix_goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matchId` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `minute` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_groups`
--

CREATE TABLE IF NOT EXISTS `tableprefix_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tableprefix_groups`
--

INSERT INTO `tableprefix_groups` (`id`, `name`, `description`) VALUES
(1, 'Administrator', 'All permissions!'),
(2, 'Co-administrator', 'Have all permissions except user management and configuration of the CMS'),
(3, 'Content editor', 'Group for users doing common tasks (news, pictures, reports, ...)');

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_groups_permissions`
--

CREATE TABLE IF NOT EXISTS `tableprefix_groups_permissions` (
  `groupId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  PRIMARY KEY (`groupId`,`permissionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tableprefix_groups_permissions`
--

INSERT INTO `tableprefix_groups_permissions` (`groupId`, `permissionId`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(3, 3),
(3, 4),
(3, 5),
(3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_interlinks`
--

CREATE TABLE IF NOT EXISTS `tableprefix_interlinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('PHOTO_NEWS','PHOTO_MATCH','NEWS_MATCH','NEWS_NEWS','COMMENT_NEWS','COMMENT_PHOTO','PHOTO_TEAM') NOT NULL DEFAULT 'PHOTO_MATCH',
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `id1` (`id1`),
  KEY `id2` (`id2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_matches`
--

CREATE TABLE IF NOT EXISTS `tableprefix_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competitionId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `teamId1` int(11) NOT NULL,
  `teamId2` int(11) NOT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `forfeit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_menu`
--

CREATE TABLE IF NOT EXISTS `tableprefix_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(127) NOT NULL,
  `link` varchar(255) NOT NULL,
  `moduleId` int(11) DEFAULT NULL,
  `parent` int(11) NOT NULL,
  `edit` tinyint(4) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL,
  `show` tinyint(4) NOT NULL DEFAULT '0',
  `admin` tinyint(4) NOT NULL,
  `rights` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tableprefix_menu`
--

INSERT INTO `tableprefix_menu` (`id`, `text`, `link`, `moduleId`, `parent`, `edit`, `rank`, `show`, `admin`, `rights`) VALUES
(1, 'Home', '', NULL, 0, 0, 1, 1, 0, ''),
(2, 'News', 'news', NULL, 0, 0, 2, 1, 0, ''),
(3, 'Team', 'teams', NULL, 0, 0, 3, 1, 0, ''),
(4, 'Matches', 'matches', NULL, 0, 0, 4, 1, 0, ''),
(5, 'My club', 'admin/home', NULL, 0, 0, 5, 1, 1, ''),
(6, 'Reports', 'admin/reports', NULL, 5, 0, 6, 1, 1, ''),
(7, 'News', 'admin/news', NULL, 5, 0, 7, 1, 1, ''),
(8, 'Teams', 'admin/clubs/show/1', NULL, 5, 0, 8, 1, 1, ''),
(9, 'Comments', 'admin/comments', NULL, 5, 0, 9, 1, 1, ''),
(11, 'General management', 'admin/management', NULL, 0, 0, 11, 1, 1, ''),
(12, 'Clubs', 'admin/clubs', NULL, 11, 0, 12, 1, 1, ''),
(13, 'Competitions', 'admin/competitions', NULL, 11, 0, 13, 1, 1, ''),
(14, 'Players', 'admin/players', NULL, 11, 0, 14, 1, 1, ''),
(15, 'Sports halls', 'admin/places', NULL, 11, 0, 15, 1, 1, ''),
(16, 'Site configuration', 'admin/config', NULL, 0, 0, 16, 1, 1, ''),
(17, 'File explorer', 'admin/explorer', NULL, 16, 0, 17, 1, 1, ''),
(18, 'Users', 'admin/users', NULL, 16, 0, 18, 1, 1, ''),
(19, 'Logout', 'session/logout', NULL, 0, 0, 19, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_modules`
--

CREATE TABLE IF NOT EXISTS `tableprefix_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `author` varchar(31) NOT NULL,
  `version_major` varchar(4) NOT NULL,
  `version_minor` varchar(4) NOT NULL,
  `version_revision` varchar(6) NOT NULL,
  `url` varchar(255) NOT NULL,
  `settings` tinyint(4) NOT NULL,
  `enable` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_news`
--

CREATE TABLE IF NOT EXISTS `tableprefix_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `authorId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_permissions`
--

CREATE TABLE IF NOT EXISTS `tableprefix_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moduleId` int(11) DEFAULT NULL,
  `shortname` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tableprefix_permissions`
--

INSERT INTO `tableprefix_permissions` (`id`, `moduleId`, `shortname`, `name`) VALUES
(1, NULL, 'siteconfig', 'Configuration of the site'),
(2, NULL, 'users', 'Users Management'),
(3, NULL, 'viewadmin', 'View administration pages'),
(4, NULL, 'newsedit', 'Create/Edit/Delete news'),
(5, NULL, 'pictureedit', 'Add/Remove pictures'),
(6, NULL, 'teamplayeredit', 'Management of teams and players'),
(7, NULL, 'matchcompetitionedit', 'Management of matches and competitions'),
(8, NULL, 'reportedit', 'Add/Edit/Delete match reports'),
(9, NULL, 'placeedit', 'Management of places'),
(10, NULL, 'eventedit', 'Editing events'),
(11, NULL, 'uploadfile', 'Right to upload file'),
(12, NULL, 'deletefile', 'Delete file'),
(13, NULL, 'commentedit', 'Validate/Delete comments');

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_places`
--

CREATE TABLE IF NOT EXISTS `tableprefix_places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `shortname` varchar(20) NOT NULL,
  `street` varchar(255) NOT NULL,
  `number` varchar(5) NOT NULL,
  `postalcode` varchar(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_players`
--

CREATE TABLE IF NOT EXISTS `tableprefix_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(32) NOT NULL,
  `lastName` varchar(32) NOT NULL,
  `birthday` date DEFAULT NULL,
  `city` varchar(127) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_players_matches`
--

CREATE TABLE IF NOT EXISTS `tableprefix_players_matches` (
  `playerId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`playerId`,`matchId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_players_teams`
--

CREATE TABLE IF NOT EXISTS `tableprefix_players_teams` (
  `playerId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`playerId`,`teamId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_seasons`
--

CREATE TABLE IF NOT EXISTS `tableprefix_seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `year` (`year`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_sessions`
--

CREATE TABLE IF NOT EXISTS `tableprefix_sessions` (
  `id` varchar(32) NOT NULL,
  `userId` int(11) NOT NULL,
  `startTime` int(11) NOT NULL,
  `lastTime` int(11) NOT NULL,
  `lastIP` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_settings`
--

CREATE TABLE IF NOT EXISTS `tableprefix_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moduleId` int(11) DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tableprefix_settings`
--

INSERT INTO `tableprefix_settings` (`id`, `moduleId`, `name`, `value`) VALUES
(1, NULL, 'myClubId', '0'),
(2, NULL, 'seasonId', '0'),
(3, NULL, 'myClubName', ''),
(4, NULL, 'mail_no-reply', ''),
(5, NULL, 'commentValidation', '0'),
(6, NULL, 'language', 'english'),
(7, NULL, 'template', 'default'),
(8, NULL, 'adminEmail', ''),
(9, NULL, 'version_major', '1'),
(10, NULL, 'version_minor', '4'),
(11, NULL, 'version_revision', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_teams`
--

CREATE TABLE IF NOT EXISTS `tableprefix_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clubId` int(11) NOT NULL,
  `seasonId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contactName` varchar(255) NOT NULL,
  `contactPhone` varchar(32) NOT NULL,
  `contactEmail` varchar(255) NOT NULL,
  `placeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_teams_competitions`
--

CREATE TABLE IF NOT EXISTS `tableprefix_teams_competitions` (
  `teamId` int(11) NOT NULL,
  `competitionId` int(11) NOT NULL,
  `forfeit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`teamId`,`competitionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_type_competition`
--

CREATE TABLE IF NOT EXISTS `tableprefix_type_competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `shortname` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tableprefix_type_competition`
--

INSERT INTO `tableprefix_type_competition` (`id`, `name`, `shortname`) VALUES
(1, 'Championnat LFFS (Belgium)', 'BE-LFFS-CHAMP'),
(2, 'Coupe', ''),
(3, 'Tournoi', '');

-- --------------------------------------------------------

--
-- Table structure for table `tableprefix_users`
--

CREATE TABLE IF NOT EXISTS `tableprefix_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tableprefix_users`
--

INSERT INTO `tableprefix_users` (`id`, `username`, `password`, `email`, `groupId`) VALUES
(1, 'admin', '', '', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
