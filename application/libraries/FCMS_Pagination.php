<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class FCMS_Pagination extends CI_Pagination { 
    
    /* Call parent constructor with specific parameters */
    function __construct(){
        $config['num_links'] = 5;
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<p class="paginationMenu">';
        $config['full_tag_close'] = '</p>';
        $config['first_link'] = '&lsaquo; '.lang('pagination_First');
        $config['last_link'] = lang('pagination_Last').' &rsaquo;';
        parent::__construct($config);
    }
    
    function setBaseUrl($baseUrl){
        $config['base_url']=$baseUrl;
        $this->initialize($config);
    }
    
    function setPage($totalRows, $curPage, $perPage=10){
        if($totalRows==0)
            $lastPage=1;
        else
            $lastPage=ceil($totalRows/$perPage);
        if($curPage<1 || $curPage>$lastPage)
            return false;
        $config['total_rows']=$totalRows;
        $config['per_page']=$perPage;
        $config['cur_page']=$curPage;
        $this->initialize($config);
        /* return logical id of first element of the page */
        
        return ($curPage-1)*$perPage;
    }
    
	function create_links()
	{
		// If our item count or per-page total is zero there is no need to continue.
		if ($this->total_rows == 0 OR $this->per_page == 0)
		{
			return '';
		}

		// Calculate the total number of pages
		$num_pages = ceil($this->total_rows / $this->per_page);

		// Is there only one page? Hm... nothing more to do here then.
		if ($num_pages == 1)
		{
			return '';
		}

		// Determine the current page number.
		$CI =& get_instance();

		/*if ($CI->config->item('enable_query_strings') === TRUE OR $this->page_query_string === TRUE)
		{
			if ($CI->input->get($this->query_string_segment) != 0)
			{
				$this->cur_page = $CI->input->get($this->query_string_segment);

				// Prep the current page - no funny business!
				$this->cur_page = (int) $this->cur_page;
			}
		}
		else
		{
			if ($CI->uri->segment($this->uri_segment) != 0)
			{
				$this->cur_page = $CI->uri->segment($this->uri_segment);

				// Prep the current page - no funny business!
				$this->cur_page = (int) $this->cur_page;
			}
		}*/

		$this->num_links = (int)$this->num_links;

		if ($this->num_links < 1)
		{
			show_error('Your number of links must be a positive number.');
		}

		if ( ! is_numeric($this->cur_page))
		{
			$this->cur_page = 0;
		}

		// Is the page number beyond the result range?
		// If so we show the last page
		/*if ($this->cur_page > $this->total_rows)
		{
			$this->cur_page = ($num_pages - 1) * $this->per_page;
		}*/

		$uri_page_number = $this->cur_page;
		//$this->cur_page = floor(($this->cur_page/$this->per_page) + 1);

		// Calculate the start and end numbers. These determine
		// which number to start and end the digit links with
        $linkseachside=floor($this->num_links/2);
        $start = $this->cur_page;
        $end = $this->cur_page;
        for ($i=0;$i<$linkseachside;$i++) {
            if($start>1)
                $start--;
            elseif($end<$num_pages) {
                $end++;
            }
            else
                break;
            if($end<$num_pages)
                $end++;
            elseif ($start>1)
                $start--;
            else
                break;
        }
        
		// Is pagination being used over GET or POST?  If get, add a per_page query
		// string. If post, add a trailing slash to the base URL if needed
		if ($CI->config->item('enable_query_strings') === TRUE OR $this->page_query_string === TRUE)
		{
			$this->base_url = rtrim($this->base_url).'&amp;'.$this->query_string_segment.'=';
		}
		else
		{
			$this->base_url = rtrim($this->base_url, '/') .'/';
		}

		// And here we go...
		$output = '';

		// Render the "First" link
		if  ($this->first_link !== FALSE)
		{
            if($this->cur_page > 1){
                $first_url = ($this->first_url == '') ? $this->base_url : $this->first_url;
                $output .= $this->first_tag_open.'<a '.$this->anchor_class.'href="'.$first_url.'">'.$this->first_link.'</a>'.$this->first_tag_close;
            }
            else {
                $output .= $this->first_tag_open.$this->first_link.$this->first_tag_close;
            }
		}

		// Render the "previous" link
		if  ($this->prev_link !== FALSE)
		{
            if($this->cur_page>1){
                $i = $this->cur_page-1;
                $i = ($i == 0) ? '' : $this->prefix.$i.$this->suffix;
                $output .= $this->prev_tag_open.'<a '.$this->anchor_class.'href="'.$this->base_url.$i.'">'.$this->prev_link.'</a>'.$this->prev_tag_close;
            }
            else 
                $output .= $this->prev_tag_open.$this->prev_link.$this->prev_tag_close;
		}

		// Render the pages
		if ($this->display_pages !== FALSE)
		{
			// Write the digit links
			for ($loop = $start ; $loop <= $end; $loop++)
			{
				$i = ($loop);

				//if ($i >= 1)
				{
					if ($this->cur_page == $loop)
					{
						$output .= $this->cur_tag_open.$loop.$this->cur_tag_close; // Current page
					}
					else
					{
						$n = ($i == 0) ? '' : $i;

						if ($n == '' && $this->first_url != '')
						{
							$output .= $this->num_tag_open.'<a '.$this->anchor_class.'href="'.$this->first_url.'">'.$loop.'</a>'.$this->num_tag_close;
						}
						else
						{
							$n = ($n == '') ? '' : $this->prefix.$n.$this->suffix;

							$output .= $this->num_tag_open.'<a '.$this->anchor_class.'href="'.$this->base_url.$n.'">'.$loop.'</a>'.$this->num_tag_close;
						}
					}
				}
			}
		}

		// Render the "next" link
		if ($this->next_link !== FALSE)
		{
            if($this->cur_page < $num_pages)
                $output .= $this->next_tag_open.'<a '.$this->anchor_class.'href="'.$this->base_url.$this->prefix.($this->cur_page + 1 ).$this->suffix.'">'.$this->next_link.'</a>'.$this->next_tag_close;
            else 
                $output .= $this->next_tag_open.$this->next_link.$this->next_tag_close;
        }

		// Render the "Last" link
		if ($this->last_link !== FALSE)
		{
            if($this->cur_page<$num_pages){
                $output .= $this->last_tag_open.'<a '.$this->anchor_class.'href="'.$this->base_url.$this->prefix.$num_pages.$this->suffix.'">'.$this->last_link.'</a>'.$this->last_tag_close;
            }
            else {
                $output .= $this->last_tag_open.$this->last_link.$this->last_tag_close;
            }
        }

		// Kill double slashes.  Note: Sometimes we can end up with a double slash
		// in the penultimate link so we'll kill all double slashes.
		$output = preg_replace("#([^:])//+#", "\\1/", $output);

		// Add the wrapper HTML if exists
		$output = $this->full_tag_open.$output.$this->full_tag_close;

		return $output;
	}

}
?>
