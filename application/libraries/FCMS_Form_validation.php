<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class FCMS_Form_validation extends CI_Form_validation { 
    
    var $formToken;
    
    function __construct(){
        parent::__construct();
        $this->formToken = uniqid();
    }

    /*
     * Overriding run method to handle form token
     */
    function run() {
        $tokens = $this->CI->session->userdata('formTokens');
        if(!is_array($tokens)) $tokens=array();
        if(!isset($_POST['formToken'])
            || in_array($_POST['formToken'],$tokens)){
            //generate a new token
            return false;
        }
        else {
            //save token that has been used
            $tokens[] = $_POST['formToken'];
            $this->CI->session->set_userdata('formTokens', $tokens);
            return parent::run();
        }
    }

    /*
     * doesNotMatch validation function
     */
    function doesNotMatch($str, $field)
	{
		if ( ! isset($_POST[$field]))
		{
			return TRUE;
		}

		$field = $_POST[$field];

		return ($str === $field) ? FALSE : TRUE;
	}

    /*
     * Check if word from captcha is valid
     */
    function valid_captcha($str)
    {
        
        if(!isset($_POST['formToken']))
            return false;
        
        $captchas = $this->CI->session->userdata('captchas');
        if (!array_key_exists($_POST['formToken'], $captchas))
            return false;
        if (md5(strtolower($str)) != $captchas[$_POST['formToken']]){
            return false;
        }
        else{
            return true;
        }
    }

    /*
     * Check if date is valid
     */
    function valid_date($str)
    {
        if(empty($str))
            return true;
        $pattern = lang('dt_form_date_regexp');
        $humanpattern = lang('dt_form_date_humanpattern');
        if ($pattern=='dt_form_date_regexp'){
            $pattern = '/^(?P<day>\d{1,2})\/(?P<month>\d{1,2})\/(?P<year>\d\d(?:\d\d)?)$/';
            $humanpattern = 'dd/mm/yyyy';
        }
        if(!preg_match($pattern, $str, $matches)){
            $this->CI->form_validation->set_message('valid_date', sprintf(lang('date_Date_format_should_follow_pattern'), $humanpattern));
            return false;
        }
        if($matches['year']<100){
            if($matches['year']>50)
                $matches['year']+=1900;
            else
                $matches['year']+=2000;
        }
        if(checkdate($matches['month'], $matches['day'], $matches['year']))
            return $matches['year'].'-'.$matches['month'].'-'.$matches['day'];
        else {
            $this->CI->form_validation->set_message('valid_date', sprintf(lang('date_Date_is_invalid'), $humanpattern));
            return false;
        }
    }

    /*
     * Check if time is valid
     */
    function valid_time($str) {
        if(empty($str))
            return true;
        $pattern = lang('dt_form_time_regexp');
        $humanpattern = lang('dt_form_time_humanpattern');
        if ($pattern=='dt_form_time_regexp'){
            $pattern= '/^(?P<hours>\d{1,2})[:.](?P<minutes>\d{2})$/';
            $humanpattern='hh:mm';
        }
        if(!preg_match($pattern, $str, $matches)){
            $this->CI->form_validation->set_message('valid_time', sprintf(lang('date_Time_format_should_follow_pattern'), $humanpattern));
            return false;
        }
        if($matches['hours']>23 || $matches['minutes']>59){
            $this->CI->form_validation->set_message('valid_time', sprintf(lang('date_Time_is_invalid'), $humanpattern));
            return false;
        }
        else {
            return $matches['hours'].':'.$matches['minutes'];
        }
    }
}
?>
