<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
.---------------------------------------------------------------------------.
|  Software: PHP Captcha Generator                                          |
|   Version: 1.0                                                            |
|   Contact: <contact@dotrun.net>                                           |
|      Info: http://www.dotrun.net/blog/                                    |
|   Support: <contact@dotrun.net>                                           |
| ------------------------------------------------------------------------- |
|    Author: Raffaël Lapierre                                               |
| Copyright (c) 2009, dotRun. All Rights Reserved.                          |
| ------------------------------------------------------------------------- |
|   License: Distributed under the General Public License (GPL)             |
'---------------------------------------------------------------------------'

/**
 * PHP Captcha Generator class
 * @author Raffael Lapierre
 * @copyright 2009 dotRun.net
 */

class CaptchaGenerator
{
  /////////////////////////////////////////////////
  // PROPERTIES, PUBLIC
  /////////////////////////////////////////////////

   /**
    * Length of the word.
    * @var int
    */
    var $Length = 4;

   /**
    * Chars used to create the word.
    * @var string
    */
    // J'ai viré certains caractères pour éviter des confusions
    // 0,1,i,l,o
    var $Chars = '23456789abcdefghjkmnpqrstuvwxyz';

   /**
    * TTL of the captcha in s
    * Not implemented yet.
    * @var int
    */
    var $Timeout = 60;

   /**
    * Fonts
    * @var array(string)
    */
    var $Fonts = array();

  /////////////////////////////////////////////////
  // PROPERTIES, PRIVATE
  /////////////////////////////////////////////////

    var $value = '';
    var $width = 130;
    var $height = 35;
    var $angle = 35;
    var $mindots = 100;
    var $maxdots = 150;
    var $minlines = 16;
    var $maxlines = 16;

  /////////////////////////////////////////////////
  // CONSTRUCTOR
  /////////////////////////////////////////////////

    function __construct(){
        if(!isset($_SESSION)){
            session_start();
        }
    }

  /////////////////////////////////////////////////
  // SETTERS
  /////////////////////////////////////////////////

   /**
    * @access public
    * @param int $length
    * @return void
    */
    function setLength($length){
      if($length > 0){
        $this->Length = (int)$length;
      }
    }

   /**
    * @access public
    * @param string $chars
    * @return void
    */
    function setChars($chars){
      if(!empty($chars)){
        $this->Chars = (string)$chars;
      }
    }

   /**
    * @access public
    * @param int $timeout
    * @return void
    */
    function setTimeout($timeout){
      if($timeout > 0){
        $this->Timeout = (int)$timeout;
      }
    }

  /////////////////////////////////////////////////
  // METHODS
  /////////////////////////////////////////////////

   /**
    * Generate the captcha value
    * @access private
    * @param void
    * @return void
    */
    function generateCaptcha(){
        for($i=1; $i <= $this->Length; $i++){
          $n = strlen($this->Chars);
          $n = mt_rand(0,$n-1);
          $this->value .= $this->Chars[$n];
        }
    }

   /**
    * Add a font
    * @access public
    * @param string $font
    * @return void
    */
    function addFont($font){
      if(!empty($font)){
        if(is_array($font)){
          foreach($font as $f){
            array_push($this->Fonts, $f);
          }
        } else {
          array_push($this->Fonts, $font);
        }
      }
    }

   /**
    * Return a random color.
    * @access private
    * @param void
    * @return resource
    */
    function randomColor($image){
        $r = imagecolorallocate($image,rand(0,0),rand(0,0),rand(0,0));
        return $r;
    }

   /**
    * Create the captcha in a png file
    * @param void
    * @return void
    */
    function createImage($id=''){
        $this->generateCaptcha();
        $this->addFont('captcha/luggerbu.ttf'); // Default font
        $this->addFont('captcha/TOONISH.ttf'); // Default font
/*
        $this->addFont('captcha/casual.ttf'); // Default font
*/
        $this->addFont('captcha/Alanden_.ttf'); // Default font

        $img = imagecreatetruecolor($this->width,$this->height);
        $blank = imagecolorallocate($img ,255,255,255);
        $black = imagecolorallocate($img ,0,0,0);
        imagefill($img,0,0,$blank);

        $s = round($this->width/5);
        $x = 10;

        // Make some noise !
        $rdot = rand($this->mindots,$this->maxdots);
        $rline = rand($this->minlines,$this->maxlines);
        for($d=0; $d <= $rdot; $d++){
          imagesetpixel($img,rand(0,$this->width),rand(0,$this->height),$this->randomColor($img));
        }
        for($l=0; $l <= $rline; $l++){
          imageline($img,rand(0,$this->width),rand(0,$this->height),rand(0,$this->width),rand(0,$this->height),$this->randomColor($img));
        }

        // Génération des lettres
        for($i=0; $i<=strlen($this->value)-1; $i++){
            //font
            $fr = mt_rand(0,count($this->Fonts)-1);
            $font = $this->Fonts[$fr];

            //colors
            $a = (rand(1,2)==1)?rand(0,$this->angle):rand(360-$this->angle,360);
            $font =  dirname(__FILE__)."/".$font; //edit Gregory BAUDET
            imagettftext($img,16*rand(10, 10)/10,$a,$x*rand(10, 10)/10.0,(($this->height/2*rand(8, 12)/10)+8),$this->randomColor($img),$font,$this->value[$i]);
            $x+=$s;
        }
        return (array('text'=>$this->value, 'img'=>$img)); //edit Gregory BAUDET
    }

    public function checkCaptcha($value, $id='')
    {
        if(!isset($_SESSION['captchas'][$id]))
          return false;
        if($_SESSION['captchas'][$id]==md5(strtolower($value)))
          return true;
        return false;
    }
}
// That's all folks !
?>
