<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * Authentication of users and permissions
 */

class Authentication
{
    private $timeOut;
    
    private $updateInterval;
    
    private $authenticated;

    private $authError;
    
    var $CI;

    public function __construct() {
        $this->timeOut=1500;
        $this->updateInterval=180;
        $this->CI=&get_instance();
        $this->CI->load->model('Usermodel');
        //$this->CI->session->start();
        $this->authError = '';
        $this->authenticated = false;
    }

    public function setTimeOut($val){
        $this->timeOut=(int)$val;
    }

    /**
     * Check if the user is authenticated
     */
    public function isAuthenticated()
    {
        return $this->authenticated;
    }

    /*
     * Retreive the authentication error;
     */
    public function getAuthError()
    {
        return $this->authError;
    }

    
    /*
     * Check if user has permission for something
     */
    public function hasPermission($permission, $module=null) {
        $authInfo=$this->CI->session->userdata('authInfo');
        if(is_array($authInfo['permissions'])){
            foreach($authInfo['permissions'] as $perm){
                if($permission==$perm['shortname'] && $module==$perm['moduleName'])
                    return true;
            }
        }
        return false;
    }

    /*
     * Check if the user has already a good session
     */
    public function checkAuthentication() {
        $this->authenticated = false;
        $authInfo=$this->CI->session->userdata('authInfo');
        if(isset($authInfo['username']) && isset($authInfo['userId']) && 
                isset($authInfo['groupId']) && isset($authInfo['permissions'])){
            $data = $this->CI->Usermodel->getSession($authInfo['userId']);
            if(count($data)==1){
                $sess = $data[0];
                if(($sess['lastIP'] == $_SERVER['REMOTE_ADDR']) && 
                        ((int)$this->timeOut > (int)$sess['diffTime'])){
                    //authentication OK
                    if($sess['diffTime']>$this->updateInterval){
                        $this->CI->Usermodel->updateSessionTime($authInfo['userId']);
                    }
                    $this->authenticated = true;
                    return true;
                }
                else {
                    $this->CI->Usermodel->clearSession($authInfo['userId']);
                    $this->authError = 'login_timeout_error';
                    $this->authenticated = false;
                    return false;
                }
            }
        }
        return false;
    }

    /*
     * Logout the user
     */
    public function logout() {
        $this->authenticated = false;
        $this->CI->session->unset_userdata('authInfo');
        $this->CI->session->destroy();
        return true;
    }

    /*
     * Login a user with password
     */
    public function login($user, $password) {
        $data = $this->CI->Usermodel->checkUsernamePassword($user, $password);
        $this->CI->session->unset_userdata('authInfo');
        if(count($data)==1) {
            $authInfo=array();
            $infos = $data[0];
            $authInfo['username']=$infos['username'];
            $authInfo['userId']=$infos['id'];
            $authInfo['groupId']=$infos['groupId'];
            //clear any remaining session
            $this->CI->Usermodel->clearSession($authInfo['userId']);
            //create new session
            $this->CI->Usermodel->createSession($authInfo['userId'], $_SERVER['REMOTE_ADDR']);
            //load permissions
            $authInfo['permissions']=$this->CI->Usermodel->getPermissions($authInfo['groupId']);
            $this->CI->session->set_userdata('authInfo', $authInfo);
            $this->authenticated = true;
            return true;
        }
        else{
            $this->authError = 'login_bad_user_pass_error';
            $this->authenticated = false;
            return false;
        }
    }

}

?>
