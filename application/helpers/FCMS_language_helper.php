<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

if ( ! function_exists('lang'))
{
	function lang($line, $id = '')
	{
		$CI =& get_instance();
		$linet = $CI->lang->line($line);
        
        if($linet===FALSE)
            $linet=$line;
		
		return $linet;
	}
}

if ( ! function_exists('formatDate'))
{
	function formatDate($date, $format)
	{
        if (preg_match('/^(\d\d(?:\d\d)?)-(\d{1,2})-(\d{1,2})(\s\d{1,2}:\d{1,2}:\d{1,2})?$/', $date)!=1)
            return $date;
        $CI =& get_instance();
        $format = $CI->lang->line('dt_'.$format);

        if($format===FALSE)
            return $date;

        return strftime($format , strtotime($date));
	}
}

?>
