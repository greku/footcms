<h2><?php echo lang('login_Login'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
<?php if(!empty($autherror)) echo '<p>'.lang($autherror).'</p>'; ?>
<form action="<?php echo site_url('session/login'); ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="formToken" value="<?php echo $formToken; ?>" />
<table width="100%">
  <tr valign="top">
    <td><?php echo lang('login_Username'); ?> :</td>
    <td><input type="text" name="username" value="<?php echo set_value('username'); ?>" size="15" maxlength="49" /></td>
  </tr>
  <tr valign="top">
    <td><?php echo lang('login_Password'); ?> :</td>
    <td><input type="password" name="password" value=""></td>
  </tr>
  <tr>
    <td colspan="2"><input type="submit" value="<?php echo lang('login_Sign_in'); ?>"/></td>
  </tr>
</table>
</form>

