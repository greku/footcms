<h2><?php echo lang('Comments'); ?></h2>
<p>
  <a href="<?php echo site_url('comments/'.$type.'/'.$id.'/add'); ?>">
  <?php echo lang('comments_add_comment'); ?>
  </a>
</p>
<?php echo $pagination; ?>
<?php if(isset($commentAdded)&&$commentAdded==true): ?>
<div class="messageSuccess">
  <p>
    <?php if(isset($waitValidation) && $waitValidation==true)
            echo lang('comments_wait_validation');
        else
            echo lang('comments_comment_added');
    ?>
  </p>
</div>
<?php endif; ?>
<?php foreach($comments as $item): extract($item);?>
<p><b><?php echo $item['author']; ?></b><br />
  <?php echo $item['comment']; ?>
  <br />
  <em><?php echo formatDate($item['date'], 'datetimetextlong');?></em>
</p>
<?php endforeach; ?>
<?php if(empty($comments)): ?>
<p><?php echo lang('comments_no_comments');?></p>
<?php endif; ?>
<?php echo $pagination; ?>
