<?php
  echo '<?xml version="1.0" encoding="utf-8" ?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo lang('install_Footcms_install'); ?></title>
  <meta name="generator" content="FootCMS" />
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link href="<?php echo base_url(); ?>css/default/install.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="Page">
  <p style="float:right"><a href="<?php echo site_url('install/'); ?>"><?php echo lang('install_Change_language'); ?></a></p>
  <h1><?php echo lang('install_Footcms_install'); ?></h1>
        <p><?php echo lang('install_Footcms_version_being_installed'); ?> : <?php echo FCMS_VERSION_MAJOR.'.'.FCMS_VERSION_MINOR.'.'.FCMS_VERSION_REVISION; ?></p>
  <h2><?php echo lang('install_Compatibility_check'); ?></h2>
  <?php foreach ($tests as $test): ?>
    <p><?php echo lang('install_'.$test['name']);?> :
      <?php if ($test['result']==1): ?>
        <span style='color: green'><?php echo lang('install_result_OK');?></span>
      <?php elseif ($test['result']==2): ?>
        <span style='color: red'><?php echo lang('install_result_NOK');?></span>
      <?php elseif ($test['result']==3): ?>
        <span style='color: orange'><?php echo lang('install_result_NOK');?></span>
      <?php else: ?>
        <span style='color: red'>Unknown</span>
      <?php endif; ?>
    </p>
  <?php endforeach; ?>
  <?php if(!empty($warnings)): ?>
    <div class="messageWarning">
      <ul>
        <?php foreach ($warnings as $warning): ?>
          <li><?php echo lang($warning); ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  <?php endif; ?>
  <?php if(empty($errors)): ?>
    <?php if($installType=='complete'): ?>
      <p><a href="<?php echo site_url('install/complete_'.$installDetail.'/'.$lang); ?>"><?php echo lang('install_Continue_installation'); ?></a></p>
    <?php elseif($installType=='update'): ?>
      <p><a href="<?php echo site_url('install/update_'.$installDetail.'/'.$lang); ?>"><?php echo lang('install_Continue_update'); ?></a></p>
    <?php endif; ?>
  <?php else: ?>
    <div class="messageError">
      <ul>
        <?php foreach ($errors as $error): ?>
          <li><?php echo lang($error); ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  <?php endif; ?>
</div>
</body>
</html>
