<?php
  echo '<?xml version="1.0" encoding="utf-8" ?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo lang('install_Footcms_install'); ?></title>
  <meta name="generator" content="FootCMS" />
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link href="<?php echo base_url(); ?>css/default/install.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="Page">
  <h1><?php echo lang('install_Footcms_install'); ?></h1>
    <p><?php echo lang('install_Footcms_version_being_installed'); ?> : <?php echo FCMS_VERSION_MAJOR.'.'.FCMS_VERSION_MINOR.'.'.FCMS_VERSION_REVISION; ?></p>
  <h2><?php echo lang('install_Database_configuration'); ?></h2>
    <p>
      <?php echo lang('install_Hostname'); ?>: <?php echo $db_hostname; ?></br>
      <?php echo lang('install_Username'); ?>: <?php echo $db_username; ?></br>
      <?php echo lang('install_Password'); ?>: <?php echo $db_password; ?></br>
      <?php echo lang('install_Database'); ?>: <?php echo $db_database; ?></br>
      <?php echo lang('install_Table prefix'); ?>: <?php echo $db_tableprefix; ?>
    </p>
  <h2><?php echo lang('install_General_settings'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('install/complete/'.$lang); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('install_Name_of_your_club'); ?>&nbsp;:</td>
      <td><input name="clubname" value="<?php echo set_value('clubname', ''); ?>" size="25" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('install_Matricule_of_your_club'); ?>&nbsp;:</td>
      <td><input name="clubmatricule" value="<?php echo set_value('clubmatricule', ''); ?>" size="15" maxlength="10" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('install_Administrator_email'); ?>&nbsp;:</td>
      <td><input name="adminemail" value="<?php echo set_value('adminemail', ''); ?>" size="30" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('install_Administrator_password'); ?>&nbsp;:</td>
      <td><input name="adminpass" value="<?php echo set_value('adminpass', ''); ?>" size="30" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('install_Administrator_password'); ?>&nbsp;:</td>
      <td><input name="adminrepass" value="<?php echo set_value('adminrepass', ''); ?>" size="30" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('install_No-reply_email'); ?>&nbsp;:</td>
      <td><input name="mail_no-reply" value="<?php echo set_value('mail_no-reply', ''); ?>" size="30" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('install_Language'); ?>&nbsp;:</td>
      <td>
      <select name="language">
      <?php foreach ($languages as $language): ?>
        <option value="<?php echo $language ?>" <?php echo set_select('language', $language, $language==$lang); ?>>
          <?php echo lang('install_lang_'.$language);?>
        </option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('install_Install_now'); ?>" type="submit" /></td>
    </tr>
    </tbody></table>
  </form>
</div>
</body>
</html>
