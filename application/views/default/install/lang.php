<?php
  echo '<?xml version="1.0" encoding="utf-8" ?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Footcms installation</title>
  <meta name="generator" content="FootCMS" />
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link href="<?php echo base_url(); ?>css/default/install.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="Page">
    <h1>Footcms installation</h1>
    <h3 style="text-align:center"><a href="<?php echo site_url('install/check/french');?>">Français</a></h3>
    <h3 style="text-align:center"><a href="<?php echo site_url('install/check/english');?>">English</a></h3>
</div>
</body>
</html>
