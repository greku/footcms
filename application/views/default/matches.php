<div class="contentblock">
  <h2><?php echo lang('Matches'); ?></h2>
  <table>
  <thead>
    <tr>
      <th><?php echo lang('matches_date');?></th>
      <th><?php echo lang('matches_time');?></th>
      <th><?php echo lang('matches_competition');?></th>
      <th><?php echo lang('matches_teams');?></th>
      <th><?php echo lang('matches_score');?></th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
  <?php 
  $next=0;
  foreach($matches as $item): extract($item);
    if($next==0 && $futur==1): $next=1?>
    <tr class='nextmatch'>
    <?php else: ?>
    <tr>
    <?php endif; ?>
      <td><?php echo formatDate($date,'datetextshortday');?></td>
      <td><?php echo formatDate($date,'time');?></td>
      <td><?php echo $competitionName; ?></td>
      <td><?php echo $teamName1.' vs '.$teamName2; ?></td>
      <td><?php echo (is_null($score1)||is_null($score2)?'&nbsp;':$score1.' - '.$score2).($forfeit=='YES'?' FF':'');?></td>
      <?php if($report==1): ?>
      <td><a href="reports/show/<?php echo $id; ?>" ><?php echo lang('matches_report'); ?></a></td>
      <?php else: ?>
      <td>&nbsp;</td>
      <?php endif; ?>
    </tr>
  <?php endforeach;?>
  <?php if(empty($matches)): ?>
  <tr><td colspan="6"><?php echo lang('matches_no_match');?></td></tr>
  <?php endif; ?>
  </tbody>
  </table>
</div>
