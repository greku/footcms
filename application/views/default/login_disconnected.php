<h2><?php echo lang('login_Logout'); ?></h2>
<?php if(isset($notLogged) and $notLogged==true): ?>
<div class="messageInfo">
  <p>
    <?php echo lang("login_Disconnect_not_logged"); ?> 
  </p>
</div>
<?php elseif(isset($success) and $success==true): ?>
<div class="messageSuccess">
  <p>
    <?php echo lang("login_Disconnect_success"); ?> 
  </p>
</div>
<?php endif; ?>
<p>
  <a href="<?php echo site_url(); ?>">
    <?php echo lang('login_Go_to_home_page'); ?> 
  </a>
</p>
