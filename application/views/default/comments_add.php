<?php
loadJs(array('jquery', 'ahctpac'));
?>
<h2><?php echo lang('comments_post_comment'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
<form action="<?php echo site_url('comments/'.$type.'/'.$id.'/add'); ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="formToken" value="<?php echo $formToken; ?>" />
<table width="100%">
  <tr valign="top">
    <td><?php echo lang('comments_yourname'); ?> :</td>
    <td><input type="text" name="gal" value="<?php echo set_value('gal'); ?>" size="15" maxlength="49" /></td>
  </tr>
  <tr valign="top">
    <td><?php echo lang('comments_comment'); ?> :</td>
    <td><textarea name="ticori" cols="30" rows="4"><?php echo set_value('ticori'); ?></textarea></td>
  </tr>
  <tr valign="top">
    <td><?php echo lang('captcha_Word_verification'); ?> :</td>
    <td><span class='ahctpac'><img src="<?php echo site_url('ajax/captcha'); ?>" alt="image" /></span><br />
    <?php echo lang('captcha_Copy_word_below'); ?><br />
    <input type="text" name="abira" value="" /></td>
  </tr>
  <tr>
    <td colspan="2"><input type="submit" value="<?php echo lang('comments_post'); ?>"/></td>
  </tr>
</table>
</form>
