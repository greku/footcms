<h2><?php echo lang('login_Login'); ?></h2>
<?php if(isset($already) and $already==true): ?>
<div class="messageInfo">
  <p>
    <?php echo lang("login_Already_logged_in"); ?> 
  </p>
</div>
<?php elseif(isset($success) and $success==true): ?>
<div class="messageSuccess">
  <p>
    <?php echo lang("login_Connect_success"); ?> 
  </p>
</div>
<?php endif; ?>
<?php if(!empty($message)) echo '<p>'.lang($message).'</p>'; ?>
<p>
  <a href="<?php echo site_url(); ?>"><?php echo lang('login_Go_to_home_page'); ?></a>
</p>
<?php if(isset($admin) and $admin==true):?>
<p>
  <a href="<?php echo site_url('admin/home'); ?>"><?php echo lang('login_Go_to_administration'); ?></a>
</p>
<?php endif; ?>
