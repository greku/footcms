<div class="contentblock">
<h2>
  <?php echo lang('error_Install_controller_not_removed'); ?>
</h2>
<p>
  <?php echo lang('error_The install controller must be removed after installation'); ?>
</p>
</div>
