<?php
loadJs(array('jquery', 'fancybox', 'fancybox.comments'));
loadCss('fancybox');
?>
<h2><?php echo lang('news_Latest_news'); ?></h2>
<?php foreach($news as $item): extract($item);?>
<p>
  <?php echo formatDate($date, 'datetextlong'); ?>: 
  <?php if(is_null($matchId)): ?>
  <a href="<?php echo site_url('news/show/'.$id); ?>">
  <?php else: ?>
  <a href="<?php echo site_url('reports/show/'.$matchId); ?>">
  <?php endif; ?>
  <?php echo $title; ?>
  </a>
  <?php if($nbcomments>0): ?>
   - <a href="<?php echo site_url('comments/news/'.$id); ?>"  class="fancy_comments"><?php echo lang('comments_view_comments'); ?> (<?php echo $nbcomments ?>)</a>
  <?php endif; ?>
</p>
<?php endforeach; ?>
<?php if(empty($news)): ?>
<?php echo lang('news_no_news');?>
<?php else: ?>
<a href="<?php echo site_url('news'); ?>"><?php echo lang('news_More_news');?></a>
<?php endif; ?>
