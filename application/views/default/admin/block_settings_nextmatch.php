<div class="contentblock">
  <h2><?php echo lang('blocks_Settings').' '.$block['name']; ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/blocks/settings/'.$block['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('blocks_Number_of_matches'); ?>&nbsp;:</td>
      <td><input name="nbmatch" value="<?php echo set_value('nbmatch', $settings['nbmatch']); ?>" size="25" maxlength="3" type="text" /></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit" /></td>
    </tr>
    </tbody></table>
  </form>
</div>

