<div class="contentblock">
  <?php if($group['id']==''):?>
  <h2><?php echo lang('users_Create_group'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('users_Edit_group'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/users/editgroup/'.$group['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('users_Name'); ?>&nbsp;:</td>
      <td><input name="name" value="<?php echo set_value('name', $group['name']); ?>" size="25" maxlength="20" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('users_Description'); ?>&nbsp;:</td>
      <td><textarea name="description" cols="40" rows="3"><?php echo set_value('description', $group['description']); ?></textarea></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('users_Permissions'); ?>&nbsp;:</td>
      <td>
      <?php foreach ($allPermissions as $permission): ?>
        <input type="checkbox" name="permissions[]" value="<?php echo $permission['id'];?>" <?php echo set_checkbox('permissions[]', $permission['id'], in_array($permission['id'], $permissions)); ?>/>
        <?php echo (lang('group_description_'.$permission['name'])=='group_description_'.$permission['name']?$permission['name']:lang('group_description_'.$permission['name'])); ?><br />
      <?php endforeach; ?>
      </td>
    </tr>
    <?php if(isset($users)): ?>
    <tr valign="top">
      <td><?php echo lang('users_Users_in_the_group'); ?>&nbsp;:</td>
      <td><?php echo implode($users,', '); ?></td>
    </tr>
    <?php endif; ?>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

