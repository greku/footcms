<div class="contentblock">
  <?php if($place['id']==''):?>
  <h2><?php echo lang('places_Add_sport_hall'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('places_Edit_sport_hall'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/places/edit/'.$place['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('places_Name'); ?>&nbsp;:</td>
      <td><input name="name" value="<?php echo set_value('name', $place['name']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('places_Short_name'); ?>&nbsp;:</td>
      <td><input name="shortname" value="<?php echo set_value('shortname', $place['shortname']); ?>" size="25" maxlength="20" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('places_Street'); ?>&nbsp;:</td>
      <td><input name="street" value="<?php echo set_value('street', $place['street']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('places_Number'); ?>&nbsp;:</td>
      <td><input name="number" value="<?php echo set_value('number', $place['number']); ?>" size="25" maxlength="5" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('places_Postal_code'); ?>&nbsp;:</td>
      <td><input name="postalcode" value="<?php echo set_value('postalcode', $place['postalcode']); ?>" size="25" maxlength="10" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('places_City'); ?>&nbsp;:</td>
      <td><input name="city" value="<?php echo set_value('city', $place['city']); ?>" size="25" maxlength="50" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('places_Phone'); ?>&nbsp;:</td>
      <td><input name="phone" value="<?php echo set_value('phone', $place['phone']); ?>" size="25" maxlength="32" type="text"></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

