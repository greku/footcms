<div class="contentblock">
<h2><?php echo lang('comments_Comments'); ?></h2>
<?php echo $pagination; ?>
<table>
  <thead>
    <tr>
      <td><?php echo lang('comments_Date');?></td>
      <td><?php echo lang('comments_Author');?></td>
      <td><?php echo lang('comments_Comment');?></td>
      <td><?php echo lang('comments_Validate');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($comments as $comment): ?>
  <tr>
  <td width="16%"><?php echo formatDate($comment['date'], 'datetimenumeric'); ?></td>
  <td><?php echo $comment['author']; ?></td>
  <td><?php echo $comment['comment']; ?></td>
  <td>
	<?php if($comment['validated']=='NO'): ?>
    <a href="<?php echo site_url('admin/comments/validate/'.$comment['id']); ?>"><?php echo lang('comments_Validate'); ?></a></td>
	<?php else: ?>
	&nbsp;
	<?php endif; ?>
  <td>
    <a href="<?php echo site_url('admin/comments/delete/'.$comment['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($comments)): ?>
    <td colspan="5"><?php echo lang('comments_No_comment'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
<?php echo $pagination; ?>
</div>

