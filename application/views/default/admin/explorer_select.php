<?php
loadJs(array('jquery', 'explorer_select'));
loadCss('fileselector');
?>
<div id="fileselector">
  <h1><?php echo lang('explorer_Select_a_file'); ?></h1>
  <h2><?php echo lang('explorer_Folder'); ?>: <span class="folderPath"><?php echo $folder['path']; ?></span></h2>
  <ul>
    <?php foreach ($files as $file):
        $name = $file['name'];
        if ($file['type']==1) {
            $imgsrc = $images_path.'folder.png';
            $imgalt = $file['name'];
            $url = site_url('admin/browser/'.$browserType.'/'.$file['id']);
        }
        else {
            if (file_exists(str_replace('system/', '', BASEPATH).'upload'.substr($file['path'], 0, strrpos($file['path'], '/')).'/thumbs/'.$file['name'])) {
                $imgsrc = site_url('upload'.substr($file['path'], 0, strrpos($file['path'], '/')).'/thumbs/'.$file['name']);
            }
            else {
                if($file['mime']=='images/png' || $file['mime']=='images/jpg' || $file['mime']=='images/gif')
                    $imgsrc = $images_path.'file-image.png';
                else if($file['mime']=='application/pdf')
                    $imgsrc = $images_path.'file-pdf.png';
                else
                    $imgsrc = $images_path.'file.png';
            }
            $imgalt = $file['name'].' '.formatDate($file['date'], 'datetimenumericc');
            $url = site_url('admin/explorer/file/'.$file['id']);
        }
    ?>
    <li>
      <?php if ($file['type']==1): ?>
        <div class="folder">
          <a href="<?php echo $url; ?>">
            <img src="<?php echo $imgsrc; ?>" alt="<?php echo $imgalt; ?>" /><br />
            <span><?php echo $name; ?></span>
          </a>
        </div>
      <?php else: ?>
        <div class="file">
          <img src="<?php echo $imgsrc; ?>" alt="<?php echo $imgalt; ?>" /><br />
          <span class="fileId-<?php echo $file['id']; ?>"><?php echo $name; ?></span>
        </div>
    <?php endif; ?>
    </li>
    <?php endforeach;?>
    <?php if(empty($files)): ?>
        <p><?php echo lang('explorer_Folder_empty'); ?></p>
    <?php endif; ?>
  </ul>
  <p>
    <input name="selectedFilePath" type="hidden" value="" />
    <input name="selectedFileId" type="hidden" value="" />
    <input id="buttonOK" type="button" value="<?php echo lang('explorer_Select'); ?>" />
    <input id="buttonCancel" type="button" value="<?php echo lang('explorer_Cancel'); ?>" />
  </p>
</div>

