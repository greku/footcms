<div class="contentblock">
<h2><?php echo lang('news_News'); ?></h2>
<a href="<?php echo site_url('admin/news/edit'); ?>"><?php echo lang('news_Add_news');?></a>
<?php echo $pagination; ?>
<table>
  <thead>
    <tr>
      <td><?php echo lang('news_Date');?></td>
      <td><?php echo lang('news_Title');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($news as $n): ?>
  <tr>
  <td><?php echo formatDate($n['date'], 'datenumeric'); ?></td>
  <td><?php echo $n['title']; ?></td>
  <td><a href="<?php echo site_url('admin/news/edit/'.$n['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <a href="<?php echo site_url('admin/news/delete/'.$n['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($news)): ?>
    <td colspan="4"><?php echo lang('news_No_news'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
<?php echo $pagination; ?>
</div>

