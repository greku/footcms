<div class="contentblock">
  <h2><?php echo lang('blocks_Settings').' '.$block['name']; ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/blocks/settings/'.$block['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('blocks_Number_of_news'); ?>&nbsp;:</td>
      <td><input name="nbnews" value="<?php echo set_value('nbnews', $settings['nbnews']); ?>" size="25" maxlength="3" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('blocks_Show_first_lines'); ?>&nbsp;:</td>
      <td><input name="shorttext" type="checkbox" value="1" <?php echo set_checkbox('shorttext', 1, $settings['shorttext']==1); ?> /></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit" /></td>
    </tr>
    </tbody></table>
  </form>
</div>

