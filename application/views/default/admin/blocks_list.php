<div class="contentblock">
<h2><?php echo lang('config_Home_page_blocks'); ?></h2>
<table>
  <thead>
    <tr>
      <td><?php echo lang('blocks_Rank');?></td>
      <td><?php echo lang('blocks_Name');?></td>
      <td><?php echo lang('blocks_Settings'); ?></td>
      <td><?php echo lang('blocks_Enable/Disable'); ?></td>
      <td><?php echo lang('blocks_Move_up'); ?></td>
      <td><?php echo lang('blocks_Move_down'); ?></td>
    </tr>
  </thead>
  <tbody>
  <?php $i=0; foreach ($blocks as $block): $i++;?>
  <tr <?php echo ($block['show']==0?'style="color:#ccc"':'') ?>>
  <td><?php echo $i; ?></td>
  <td><?php echo $block['name']; ?></td>
  <td>
    <?php if($block['params']!=''): ?>
    <a href="<?php echo site_url('admin/blocks/settings/'.$block['id']); ?>"><?php echo lang('blocks_Settings'); ?></a>
    <?php endif; ?>
  </td>
  <td>
    <?php if($block['show']==1): ?>
    <a href="<?php echo site_url('admin/blocks/disable/'.$block['id']); ?>"><?php echo lang('blocks_Disable'); ?></a>
    <?php elseif($block['show']==0): ?>
    <a href="<?php echo site_url('admin/blocks/enable/'.$block['id']); ?>"><?php echo lang('blocks_Enable'); ?></a>
    <?php endif; ?>
  </td>
  <td>
    <?php if($i>1): ?>
    <a href="<?php echo site_url('admin/blocks/move/up/'.$block['id']); ?>"><?php echo lang('blocks_Up'); ?></a>
    <?php endif; ?>
  </td>
  <td>
    <?php if($i<count($blocks)): ?>
    <a href="<?php echo site_url('admin/blocks/move/down/'.$block['id']); ?>"><?php echo lang('blocks_Down'); ?></a>
    <?php endif; ?>
  </td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
</div>
