<div class="contentblock">
  <?php if($competition['id']==''):?>
  <h2><?php echo lang('competitions_Add_competition'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('competitions_Edit_competition'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/competitions/edit/'.$competition['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('competitions_Name'); ?>&nbsp;:</td>
      <td><input name="name" value="<?php echo set_value('name', $competition['name']); ?>" size="30" maxlength="32" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('competitions_Type'); ?>&nbsp;:</td>
      <td>
      <?php if((int)$competition['id']>0): ?>
        <?php echo $competition['typeName']; ?>
      <?php else: ?>
      <select name="type">
      <?php foreach ($competitionTypes as $type): ?>
        <option value="<?php echo $type['id']; ?>" <?php echo set_select('type', $type['id'], $type['id']==$competition['typeId']);?>>
          <?php echo $type['name'];?>
        </option>
      <?php endforeach; ?>
      </select>
      <?php endif; ?>
      </td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

