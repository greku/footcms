<div class="contentblock">
  <?php if(!empty($title)): ?>
  <h2><?php echo lang($title); ?></h2>
  <?php endif; ?>
  <div class="<?php echo 'message'.ucfirst($type); ?>">
    <?php foreach ($messages as $message): ?>
    <?php if(isset($message['href'])): ?>
    <p>
      <a href="<?php echo site_url($message['href']);?>"><?php echo lang($message['text']);?></a>
    </p>
    <?php else: ?>
    <p>
      <?php echo lang($message['text']); ?>
    </p>
    <?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>
