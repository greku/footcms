<div class="contentblock">
<h2><?php echo $club['name'].' ('.$club['matricule'].')';?></h2>
<h3><?php echo lang('teams_Teams'); ?></h3>
<a href="<?php echo site_url('admin/teams/edit/0/'.$club['id']); ?>"><?php echo lang('teams_Create_team');?></a>

<?php foreach ($teams as $team): ?>
  <h4><a href="<?php echo site_url('admin/teams/show/'.$team['id'].'/'.$club['id']); ?>"><?php echo $team['name']; ?></a></h4>
    <p>
      <?php if(!empty($team['competitions'])): ?>
        <?php echo lang('teams_Competitions').': '.$team['competitions']; ?><br />
      <?php else: ?>
        <?php echo lang('teams_Competitions').': '.lang('teams_No_competitions'); ?><br />
      <?php endif; ?>
      <?php if(!empty($team['contactName'])): ?>
        <?php echo lang('teams_Contact_person').': '.$team['contactName']; ?><br />
      <?php endif; ?>
      <?php if(!empty($team['contactEmail'])): ?>
        <?php echo lang('teams_Contact_email').': '.$team['contactEmail']; ?><br />
      <?php endif; ?>
      <?php if(!empty($team['contactPhone'])): ?>
        <?php echo lang('teams_Contact_phone').': '.$team['contactPhone']; ?><br />
      <?php endif; ?>
      <?php if(!is_null($team['placeName'])): ?>
        <?php echo lang('teams_Sport_hall').': '.$team['placeName']; ?><br />
      <?php endif; ?>
    </p>
<?php endforeach; ?>
<?php if(empty($teams)): ?>
    <p><?php echo lang('clubs_No_team');?></p>
<?php endif; ?>
</div>

