<div class="contentblock">
  <h2><?php echo lang('explorer_Create_folder'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/explorer/newfolder/'.$folder['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('explorer_Folder_name'); ?>&nbsp;:</td>
      <td>
      <input type="text" name="foldername" value="<?php echo set_value('foldername'); ?>" maxlength="255" />
      </td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

