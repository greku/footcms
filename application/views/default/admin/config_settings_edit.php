<div class="contentblock">
  <h2><?php echo lang('config_Settings'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/config/general'); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('config_Name_of_your_club'); ?>&nbsp;:</td>
      <td><input name="clubname" value="<?php echo set_value('clubname', $settings['myClubName']); ?>" size="25" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('config_Administrator_email'); ?>&nbsp;:</td>
      <td><input name="adminemail" value="<?php echo set_value('adminemail', $settings['adminEmail']); ?>" size="30" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('config_No-reply_email'); ?>&nbsp;:</td>
      <td><input name="mail_no-reply" value="<?php echo set_value('mail_no-reply', $settings['mail_no-reply']); ?>" size="30" maxlength="255" type="text" /></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('config_Language'); ?>&nbsp;:</td>
      <td>
      <select name="language">
      <?php foreach ($languages as $language): ?>
        <option value="<?php echo $language ?>" <?php echo set_select('language', $language, $language==$settings['language']); ?>>
          <?php echo $language;?>
        </option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('config_Template'); ?>&nbsp;:</td>
      <td>
      <select name="template">
      <?php foreach ($templates as $template): ?>
        <option value="<?php echo $template ?>" <?php echo set_select('template', $template, $template==$settings['template']); ?>>
          <?php echo $template;?>
        </option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('config_Comments_are_invisible_until_validated'); ?>&nbsp;:</td>
      <td><input name="commentvalidation" type="checkbox" value="1" <?php echo set_checkbox('commentvalidation', 1, $settings['commentValidation']==1); ?> /></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit" /></td>
    </tr>
    </tbody></table>
  </form>
</div>

