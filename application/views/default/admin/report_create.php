<div class="contentblock">
<h2><?php echo lang('reports_Create_report'); ?></h2>
<div class="messageInfo">
    <p><?php echo lang('reports_Choose_the_match'); ?></p>
</div>
<table>
  <thead>
    <tr>
      <td><?php echo lang('matches_Date');?></td>
      <td><?php echo lang('matches_Teams');?></td>
      <td><?php echo lang('matches_Score');?></td>
      <td>&nbsp;</td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($matches as $match): ?>
  <tr>
  <td><?php echo formatDate($match['date'], 'datetimetextlong'); ?></td>
  <td><?php echo $match['teamName1'].' vs '.$match['teamName2']; ?></td><td><?php echo $match['score1'].' - '.$match['score2'].($match['forfeit']==1?' FF':''); ?></td>
  <td>
    <a href="<?php echo site_url('admin/reports/edit/'.$match['id']); ?>">
      <?php echo lang('reports_Create_report'); ?>
    </a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($matches)): ?>
  <tr>
  <td colspan="4"><?php echo lang('reports_No_match_without_reports'); ?>
  </td>
  </tr>
  <?php endif; ?>
  </tbody>
</table>
</div>

