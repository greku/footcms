<div class="contentblock">
<h2><?php echo lang('competitions_Competitions'); ?></h2>
<a href="<?php echo site_url('admin/competitions/edit'); ?>"><?php echo lang('competitions_Add_competition');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('competitions_Name');?></td>
      <td><?php echo lang('competitions_Type');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($competitions as $competition): ?>
  <tr>
  <td><a href="<?php echo site_url('admin/competitions/show/'.$competition['id']); ?>"><?php echo $competition['name']; ?></a></td>
  <td><?php echo $competition['typeName']; ?></td>
  <td><a href="<?php echo site_url('admin/competitions/edit/'.$competition['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <a href="<?php echo site_url('admin/competitions/delete/'.$competition['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($competitions)): ?>
    <td colspan="4"><?php echo lang('competitions_No_competition'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
</div>
