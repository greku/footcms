<div class="contentblock">
<h2><?php echo lang('users_Users_and_groups'); ?></h2>
<h3><?php echo lang('users_List_of_users'); ?></h3>
<a href="<?php echo site_url('admin/users/edituser'); ?>"><?php echo lang('users_Create_user');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('users_Username');?></td>
      <td><?php echo lang('users_Email');?></td>
      <td><?php echo lang('users_Group');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($users as $user): ?>
  <tr>
  <td><?php echo $user['username']; ?></td>
  <td><?php echo $user['email']; ?></td>
  <td><?php echo (lang('group_name_'.$user['groupName'])=='group_name_'.$user['groupName']?$user['groupName']:lang('group_name_'.$user['groupName'])); ?></td>
  <td><a href="<?php echo site_url('admin/users/edituser/'.$user['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <?php if($user['id']!=1): ?>
    <a href="<?php echo site_url('admin/users/deleteuser/'.$user['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
    <?php endif; ?>
  </td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<h3><?php echo lang('users_List_of_groups'); ?></h3>
<a href="<?php echo site_url('admin/users/editgroup'); ?>"><?php echo lang('users_Create_group');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('users_Name');?></td>
      <td><?php echo lang('users_Description');?></td>
      <td><?php echo lang('users_Users_in_the_group');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($groups as $group): ?>
  <tr>
  <td><?php echo (lang('group_name_'.$group['name'])=='group_name_'.$group['name']?$group['name']:lang('group_name_'.$group['name'])); ?></td>
  <td><?php echo (lang('group_description_'.$group['description'])=='group_description_'.$group['description']?$group['description']:lang('group_description_'.$group['description'])); ?></td>
  <td><?php echo $group['nbusers']; ?></td>
  <td><a href="<?php echo site_url('admin/users/editgroup/'.$group['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td><a href="<?php echo site_url('admin/users/deletegroup/'.$group['id']); ?>"><?php echo lang('admin_Delete'); ?></a></td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
</div>

