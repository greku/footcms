<div class="contentblock">
  <?php if($club['id']==''):?>
  <h2><?php echo lang('clubs_Add_club'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('clubs_Edit_club'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/clubs/edit/'.$club['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('clubs_Name'); ?>&nbsp;:</td>
      <td><input name="name" value="<?php echo set_value('name', $club['name']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('clubs_Matricule'); ?>&nbsp;:</td>
      <td><input name="matricule" value="<?php echo set_value('matricule', $club['matricule']); ?>" size="25" maxlength="20" type="text"></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

