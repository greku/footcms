<div class="contentblock">
<h2>
  <?php echo lang($title); ?>
</h2>
  <form method="post" action="<?php echo site_url($formAction); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
    <input name="confirmation" value="yes" type="hidden" />
    <div class="messageWarning">
    <p>
	  <?php echo $message; ?>
	</p>
    <input value="<?php echo lang(isset($confirmButton)?$confirmButton:'admin_Confirm'); ?>" type="submit" /><br />
    <p>
    <a href="<?php echo site_url($formCancel);?>">Cancel this operation</a>
    </p>
	</div>
  </form>

</div>
