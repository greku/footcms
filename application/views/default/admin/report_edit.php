<?php
loadJs(array('jquery', 'ckeditor/ckeditor', 'ckeditor/adapters/jquery', 'news_editor'));
?>
<div class="contentblock">
  <?php if(!isset($report['newsId']) or (int)$report['newsId']==0):?>
  <h2><?php echo lang('reports_Create_report'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('reports_Edit_report'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/reports/edit/'.$report['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('reports_Title'); ?>&nbsp;:</td>
      <td><?php echo $report['title']; ?></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('reports_Content'); ?>&nbsp;:</td>
      <td><textarea name="content" cols="70" rows="10"><?php echo set_value('content', $report['content']); ?></textarea></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

