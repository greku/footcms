<div class="contentblock">
  <h2><?php echo lang('teams_Add_existing_player'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/teams/addplayer/'.$teamId.'/'.$clubId); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('teams_Player'); ?>&nbsp;:</td>
      <td>
      <select name="player">
      <?php foreach ($players as $player): ?>
        <option value="<?php echo $player['id']; ?>" <?php echo ($player['id']==set_value('player')?'selected="selected"':'');?>><?php echo $player['lastName'].' '.$player['firstName'];?></option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('players_Shirt_number'); ?>&nbsp;:</td>
      <td><input name="number" value="<?php echo set_value('number'); ?>" size="10" maxlength="20" type="text"></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

