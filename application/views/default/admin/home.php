<div class="contentblock">
  <h2><?php echo lang('home_My_club'); ?></h2>
  <h3><?php echo lang('home_Quick_links'); ?></h3>
  <ul class="quicklinks">
    <li>
      <a href="<?php echo site_url('admin/news/edit'); ?>"><?php echo lang('home_Write_a_news');?></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/reports/create'); ?>"><?php echo lang('home_Write_a_match_report');?></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/comments/'); ?>"><?php echo lang('home_View_comments');?></a>
    </li>
  </ul>
  <h3><?php echo lang('home_Teams'); ?></h3>
  <table>
  <thead>
    <tr>
      <td><?php echo lang('teams_Name'); ?></td>
      <td><?php echo lang('teams_Players'); ?></td>
      <td><?php echo lang('teams_Competitions'); ?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($teams as $team): ?>
  <tr>
  <td><a href="<?php echo site_url('admin/teams/show/'.$team['id'].'/'.$myClubId); ?>"><?php echo $team['name']; ?></a></td>
  <td>
    <?php
        $players=array();
        foreach ($team['players'] as $player) {
            $players[]='<a href="'.site_url('admin/players/show/'.$player['id'].'/'.$team['id'].'/'.$myClubId).'">'
                .$player['lastname'].' '.$player['firstname'].(is_null($player['number'])?'':' ('.$player['number'].')')
                .'</a>';
        }
        echo implode($players, '<br />');
    ?>
  </td>
  <td>
    <?php
        $competitions=array();
        foreach ($team['competitions'] as $competition) {
            $competitions[]='<a href="'.site_url('admin/competitions/show/'.$competition['id']).'">'.$competition['name'].'</a>';
        }
        echo implode($competitions, '<br />');
    ?>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($teams)): ?>
    <td colspan="3"><a href="<?php echo site_url('admin/teams/edit/0/'.$myClubId); ?>"><?php echo lang('home_Create_team'); ?></a></td>
  <?php endif; ?>
  </tbody>
</table>
</div>

