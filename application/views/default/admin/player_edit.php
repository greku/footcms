<div class="contentblock">
  <?php if($player['id']==''):?>
    <h2><?php echo lang('players_Add_player'); ?></h2>
  <?php else: ?>
    <h2><?php echo lang('players_Edit_player'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/players/edit/'.(int)$player['id']).(!is_null($teamId)?'/'.(int)$teamId.'/'.(int)$clubId:''); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('players_Last_name'); ?>&nbsp;:</td>
      <td><input name="lastname" value="<?php echo set_value('lastname', $player['lastname']); ?>" size="25" maxlength="32" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('players_First_name'); ?>&nbsp;:</td>
      <td><input name="firstname" value="<?php echo set_value('firstname', $player['firstname']); ?>" size="25" maxlength="32" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('players_Email'); ?>&nbsp;:</td>
      <td><input name="email" value="<?php echo set_value('email', $player['email']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('players_Date_of_birth'); ?>&nbsp;:</td>
      <td><input name="birthday" value="<?php echo formatDate(set_value('birthday', $player['birthday']), 'form'); ?>" size="25" maxlength="12" type="text">&nbsp;(<?php echo lang('dt_form_date_humanpattern'); ?>)</td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('players_City'); ?>&nbsp;:</td>
      <td><input name="city" value="<?php echo set_value('city', $player['city']); ?>" size="25" maxlength="127" type="text"></td>
    </tr>
    <?php if(!is_null($teamId)): ?>
    <tr valign="top">
      <td><?php echo lang('players_Shirt_number'); ?>&nbsp;:</td>
      <td><input name="number" value="<?php echo set_value('number', $player['number']); ?>" size="10" maxlength="20" type="text"></td>
    </tr>
    <?php endif; ?>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

