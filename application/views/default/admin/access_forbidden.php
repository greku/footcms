<div class="contentblock">
<h2>
  <?php echo lang('access_forbidden'); ?>
</h2>
<p>
  <a href="<?php echo site_url('admin/home'); ?>"><?php echo lang('access_Go_to_home_page'); ?></a>
</p>
</div>
