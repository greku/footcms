<div class="contentblock">
  <h2><?php echo lang('config_Go_to_next_season'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/config/nextseason'); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
    <div class="messageWarning">
      <?php echo lang('config_Warning_Next_season'); ?><br />
      <input name="confirm" type="checkbox" value="1" <?php echo set_checkbox('confirm', 1, false); ?> />
       <?php echo lang('config_Next_season_confirm');?><br />
      <input name="copyteams" type="checkbox" value="1" <?php echo set_checkbox('copyteams', 1, true); ?> />
       <?php echo lang('config_Next_season_copy_teams');?><br />
      <input value="<?php echo lang('admin_Submit'); ?>" type="submit" />
    </div>
  </form>
</div>

