<div class="contentblock">
<h2><?php echo sprintf(lang('explorer_Folder_%s'), $folder['path']); ?></h2>
<div class="fileExplorer">
  <ul>
    <?php foreach ($files as $file):
        $name = $file['name'];
        if ($file['type']==1) {
            $imgsrc = $images_path.'folder.png';
            $imgalt = $file['name'];
            $url = site_url('admin/explorer/folder/'.$file['id']);
        }
        else {
            if (file_exists(str_replace('system/', '', BASEPATH).'upload'.substr($file['path'], 0, strrpos($file['path'], '/')).'/thumbs/'.$file['name'])) {
                $imgsrc = site_url('upload'.substr($file['path'], 0, strrpos($file['path'], '/')).'/thumbs/'.$file['name']);
            }
            else {
                if($file['mime']=='images/png' || $file['mime']=='images/jpg' || $file['mime']=='images/gif')
                    $imgsrc = $images_path.'file-image.png';
                else if($file['mime']=='application/pdf')
                    $imgsrc = $images_path.'file-pdf.png';
                else
                    $imgsrc = $images_path.'file.png';
            }
            $imgalt = $file['name'].' '.formatDate($file['date'], 'datetimenumericc');
            $url = site_url('admin/explorer/file/'.$file['id']);
        }
    ?>
    <li>
      <div>
        <a href="<?php echo $url; ?>">
          <img src="<?php echo $imgsrc; ?>" alt="<?php echo $imgalt; ?>" /><br />
          <?php echo $name; ?>
        </a>
        <?php if($file['name']!='..'): ?>
        <a href="<?php echo site_url('admin/explorer/delete/'.$file['id']);?>">
          <img src="<?php echo $images_path.'icon-delete.png';?>" alt="<?php echo lang('explorer_Delete');?>" />
        </a>
        <?php endif; ?>
      </div>
    </li>
    <?php endforeach;?>
    <?php if(empty($files)): ?>
        <p><?php echo lang('explorer_Folder_empty'); ?></p>
    <?php endif; ?>
  </ul>
</div>
<ul>
  <li><a href="<?php echo site_url('admin/explorer/upload/'.$folder['id']);?>">Upload file or image</a></li>
  <li><a href="<?php echo site_url('admin/explorer/newfolder/'.$folder['id']);?>">Create new folder</a></li>
</ul>
</div>

