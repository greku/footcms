<div class="contentblock">
<h2><?php echo $team['name']?></h2>
<a href="<?php echo site_url('admin/teams/edit/'.$team['id'].'/'.$team['clubId']); ?>"><?php echo lang('teams_Edit_team');?></a> -
<a href="<?php echo site_url('admin/teams/delete/'.$team['id'].'/'.$team['clubId']); ?>"><?php echo lang('teams_Delete_team');?></a>
<p>
  <?php if(!empty($team['contactName'])): ?>
    <?php echo lang('teams_Contact_person').': '.$team['contactName']; ?><br />
  <?php endif; ?>
  <?php if(!empty($team['contactEmail'])): ?>
    <?php echo lang('teams_Contact_email').': '.$team['contactEmail']; ?><br />
  <?php endif; ?>
  <?php if(!empty($team['contactPhone'])): ?>
    <?php echo lang('teams_Contact_phone').': '.$team['contactPhone']; ?><br />
  <?php endif; ?>
  <?php if(!is_null($team['placeName'])): ?>
    <?php echo lang('teams_Sport_hall').': '.$team['placeName']; ?><br />
  <?php endif; ?>
  <?php echo lang('teams_Number_of_players').': '.count($players); ?>
</p>
<h3><?php echo lang('teams_Players'); ?></h3>
<a href="<?php echo site_url('admin/teams/addplayer/'.$team['id'].'/'.$team['clubId']); ?>"><?php echo lang('teams_Add_existing_player');?></a> -
<a href="<?php echo site_url('admin/players/edit/0/'.$team['id'].'/'.$team['clubId']); ?>"><?php echo lang('teams_Create_new_player');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('players_Name');?></td>
      <td><?php echo lang('players_Shirt_number');?></td>
      <td><?php echo lang('players_Email');?></td>
      <td><?php echo lang('players_Date_of_birth');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('teams_Remove_player');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($players as $player): ?>
  <tr>
  <td><a href="<?php echo site_url('admin/players/show/'.$player['id'].'/'.$team['id'].'/'.$team['clubId']); ?>"><?php echo $player['lastname'].' '.$player['firstname']; ?></a></td>
  <td><?php echo (is_null($player['number'])?'':$player['number']); ?></td>
  <td><?php echo $player['email']; ?></td>
  <td><?php echo formatDate($player['birthday'], 'datenumeric'); ?></td>
  <td>
    <a href="<?php echo site_url('admin/players/edit/'.$player['id'].'/'.$team['id'].'/'.$team['clubId']); ?>">
      <?php echo lang('admin_Edit'); ?>
    </a>
  </td>
  <td>
    <a href="<?php echo site_url('admin/teams/removeplayer/'.$player['id'].'/'.$team['id'].'/'.$team['clubId']); ?>">
      <?php echo lang('teams_Remove_player'); ?>
    </a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($players)): ?>
  <tr>
  <td colspan="6"><?php echo lang('teams_No_player'); ?>
  </td>
  </tr>
  <?php endif; ?>
  </tbody>
</table>

</div>

