<?php
loadJs(array('jquery', 'match_edit'));
?>
<div class="contentblock">
  <h2><?php echo lang('matches_Edit_match'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <div id="debug"></div>
  <form method="post" action="<?php echo site_url('admin/matches/edit/'.$match['id'].'/'.$competition['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('matches_Teams'); ?>&nbsp;:</td>
      <td><?php echo $match['teamName1'].' vs '.$match['teamName2']; ?></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Date'); ?>&nbsp;:</td>
      <td><input name="date" value="<?php echo formatDate(set_value('date', $match['date']), 'form'); ?>" size="16" maxlength="12" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Time'); ?>&nbsp;:</td>
      <td><input name="time" value="<?php echo formatDate(set_value('time', $match['date']), 'form_time'); ?>" size="16" maxlength="12" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Score'); ?>&nbsp;:</td>
      <td>
        <input name="score1" id="score1" value="<?php echo set_value('score1', $match['score1']); ?>" size="5" maxlength="3" type="text"> -
        <input name="score2" id="score2" value="<?php echo set_value('score2', $match['score2']); ?>" size="5" maxlength="3" type="text">
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Forfeit'); ?>&nbsp;:</td>
      <td><input type="checkbox" name="forfeit" id="forfeit" value="1" <?php echo set_checkbox('forfeit', 1, $match['forfeit']==1); ?>/>
      </td>
    </tr>
    </tbody></table>

    <h3><?php echo lang('matches_Participating_players'); ?></h3>
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td width="50%">
        <h4><?php echo $match['teamName1']; ?></h4>
        <div id="players1">
          <?php foreach ($playersTeam1 as $player): ?>
            <input name="players1[]" value="<?php echo $player['id']; ?>" type="checkbox"
              <?php echo set_checkbox('players1[]', $player['id'], in_array($player['id'], $players1)); ?>/>
            <span id="nameplayers1<?php echo $player['id']; ?>"><?php echo $player['lastname'].' '.$player['firstname']; ?></span>
            (Num. : <input name="playernum1<?php echo $player['id'];?>" value="<?php echo set_value('playernum1'.$player['id'], $player['number']); ?>" size="3" type="text" />)<br>
          <?php endforeach; ?>
          <?php if(empty($playersTeam1)): ?>
            <?php echo lang('matches_Players_undefined'); ?>
          <?php endif; ?>
        </div>
      </td>
      <td>
        <h4><?php echo $match['teamName2']; ?></h4>
        <div id="players2">
          <?php foreach ($playersTeam2 as $player): ?>
            <input name="players2[]" value="<?php echo $player['id']; ?>" type="checkbox"
              <?php echo set_checkbox('players2[]', $player['id'], in_array($player['id'], $players2)); ?>/>
            <span id="nameplayers2<?php echo $player['id']; ?>"><?php echo $player['lastname'].' '.$player['firstname']; ?></span>
            (Num. : <input name="playernum2<?php echo $player['id'];?>" value="<?php echo set_value('playernum2'.$player['id'], $player['number']); ?>" size="3" type="text" />)<br>
          <?php endforeach; ?>
          <?php if(empty($playersTeam2)): ?>
            <?php echo lang('matches_Players_undefined'); ?>
          <?php endif; ?>
        </div>
      </td>
    </tr>
    </tbody></table>

    <h3><?php echo lang('matches_Goals'); ?></h3>
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td width="50%">
        <h4><?php echo $match['teamName1']; ?></h4>
        <span id="gspan1type"><select id="goaler1type"></select> min. : <input type="text" name="" size="5" value="" /><br /></span>
        <?php if(!empty($playersTeam1)): ?>
        <div id="goalersList1">
          <?php
          for($i=0; $i<$match['score1']; $i++):
            if(isset($goals1[$i])){
              $gplayerId=$goals1[$i]['playerId'];
              $gminute=$goals1[$i]['minute'];
            }
            else{
              $gplayerId=0;
              $gminute='';
            }
            ?>
            <span id="gspan1<?php echo $i; ?>">
              <select id="goaler1<?php echo $i; ?>" name="goaler1<?php echo $i; ?>">
                <option value="0">&nbsp;</option>
              <?php foreach($playersTeam1 as $player): ?>
                <option value="<?php echo $player['id']; ?>" <?php echo set_select('goaler1'.$i, $gplayerId, $gplayerId==$player['id']); ?>>
                  <?php echo $player['lastname'].' '.$player['firstname']; ?>
                </option>
              <?php endforeach; ?>
              </select>
              min. : <input type="text" name="goalmin1<?php echo $i; ?>" size="5" value="<?php echo $gminute;?>"/><br />
            </span>
          <?php
          endfor;
          ?>
        </div>
        <?php else: ?>
          <?php echo lang('matches_Players_undefined'); ?>
        <?php endif; ?>
      </td>
      <td>
        <h4><?php echo $match['teamName2']; ?></h4>
        <span id="gspan2type"><select id="goaler2type"></select> min. : <input type="text" name="" size="5" value="" /><br /></span>
          <?php if(!empty($playersTeam2)): ?>
          <div id="goalersList2">
          <?php
          for($i=0; $i<$match['score2']; $i++):
            if(isset($goals2[$i])){
              $gplayerId=$goals2[$i]['playerId'];
              $gminute=$goals2[$i]['minute'];
            }
            else{
              $gplayerId=0;
              $gminute='';
            }
            ?>
            <span id="gspan2<?php echo $i; ?>">
              <select id="goaler2<?php echo $i; ?>" name="goaler2<?php echo $i; ?>">
                <option value="0">&nbsp;</option>
              <?php foreach($playersTeam2 as $player): ?>
                <option value="<?php echo $player['id']; ?>" <?php echo set_select('goaler2'.$i, $gplayerId, $gplayerId==$player['id']); ?>>
                  <?php echo $player['lastname'].' '.$player['firstname']; ?>
                </option>
              <?php endforeach; ?>
              </select>
              min. : <input type="text" name="goalmin2<?php echo $i; ?>" size="5" value="<?php echo $gminute;?>"/><br />
            </span>
          <?php
          endfor;
          ?>
        </div>
        <?php else: ?>
          <?php echo lang('matches_Players_undefined'); ?>
        <?php endif; ?>
      </td>
    </tr>
    </tbody></table>

    <h3><?php echo lang('matches_Cards'); ?></h3>
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td width="50%">
        <h4><?php echo $match['teamName1']; ?></h4>
        <span id="cspan1type">
          <select id="card1type" name="player"></select>
          <?php echo lang('matches_color');?> :
          <select id="" name="color">
            <option value="yellow"><?php echo lang('matches_yellow'); ?></option>
            <option value="red"><?php echo lang('matches_red'); ?></option>
          </select>
          min. : <input type="text" name="" size="5" value="" /><br />
        </span>
        <?php if(!empty($playersTeam1)): ?>
        <div id="cardsList1">
          <?php
          $i=0;
          foreach ($cards1 as $card):
              $cplayerId=$card['playerId'];
              $cminute=$card['minute'];
              $ccolor=$card['color'];
            ?>
            <span id="cspan1<?php echo $i; ?>">
              <select id="cardplayer1<?php echo $i; ?>" name="cardplayer1<?php echo $i; ?>">
                <option value="0">&nbsp;</option>
              <?php foreach($playersTeam1 as $player): ?>
                <option value="<?php echo $player['id']; ?>" <?php echo set_select('card1'.$i, $cplayerId, $cplayerId==$player['id']); ?>>
                  <?php echo $player['lastname'].' '.$player['firstname']; ?>
                </option>
              <?php endforeach; ?>
              </select>
              <?php echo lang('matches_color');?> :
              <select id="cardcolor1<?php echo $i; ?>" name="cardcolor1<?php echo $i; ?>">
                <option value="yellow" <?php echo ($ccolor=='yellow'?'selected="selected"':''); ?>><?php echo lang('matches_yellow'); ?></option>
                <option value="red" <?php echo ($ccolor=='red'?'selected="selected"':''); ?>><?php echo lang('matches_red'); ?></option>
              </select>
              min. : <input type="text" name="cardmin1<?php echo $i; ?>" size="5" value="<?php echo $cminute;?>"/><br />
            </span>
          <?php
            $i++;
          endforeach;
          ?>
        </div>
        <a id="addCard1">Add card</a>
        <?php else: ?>
          <?php echo lang('matches_Players_undefined'); ?>
        <?php endif; ?>
      </td>
      <td width="50%">
        <h4><?php echo $match['teamName2']; ?></h4>
        <span id="cspan2type">
          <select id="card2type" name="player"></select>
          <?php echo lang('matches_color');?> :
          <select id="" name="color">
            <option value="yellow"><?php echo lang('matches_yellow'); ?></option>
            <option value="red"><?php echo lang('matches_red'); ?></option>
          </select>
          min. : <input type="text" name="" size="5" value="" /><br />
        </span>
        <?php if(!empty($playersTeam2)): ?>
        <div id="cardsList2">
          <?php
          $i=0;
          foreach ($cards2 as $card):
              $cplayerId=$card['playerId'];
              $cminute=$card['minute'];
              $ccolor=$card['color'];
            ?>
            <span id="cspan2<?php echo $i; ?>">
              <select id="cardplayer2<?php echo $i; ?>" name="cardplayer2<?php echo $i; ?>">
                <option value="0">&nbsp;</option>
              <?php foreach($playersTeam2 as $player): ?>
                <option value="<?php echo $player['id']; ?>" <?php echo set_select('card2'.$i, $cplayerId, $cplayerId==$player['id']); ?>>
                  <?php echo $player['lastname'].' '.$player['firstname']; ?>
                </option>
              <?php endforeach; ?>
              </select>
              <?php echo lang('matches_color');?> :
              <select id="cardcolor2<?php echo $i; ?>" name="cardcolor2<?php echo $i; ?>">
                <option value="yellow" <?php echo ($ccolor=='yellow'?'selected="selected"':''); ?>><?php echo lang('matches_yellow'); ?></option>
                <option value="red" <?php echo ($ccolor=='red'?'selected="selected"':''); ?>><?php echo lang('matches_red'); ?></option>
              </select>
              min. : <input type="text" name="cardmin2<?php echo $i; ?>" size="5" value="<?php echo $cminute;?>"/><br />
            </span>
          <?php
            $i++;
          endforeach;
          ?>
        </div>
        <a id="addCard2">Add card</a>
        <?php else: ?>
          <?php echo lang('matches_Players_undefined'); ?>
        <?php endif; ?>
      </td>
    </tr>
    </tbody></table>

    <table width="100%">
    <tbody>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

