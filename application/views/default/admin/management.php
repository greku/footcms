<div class="contentblock">
<h2><?php echo lang('manag_General_management'); ?></h2>
  <ul class="quicklinks">
    <li>
      <a href="<?php echo site_url('admin/clubs'); ?>"><?php echo lang('clubs_Clubs');?></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/competitions'); ?>"><?php echo lang('competitions_Competitions');?></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/players'); ?>"><?php echo lang('players_Players');?></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/places'); ?>"><?php echo lang('places_Sport_halls');?></a>
    </li>
  </ul>
</div>
