<div class="contentblock">
<h2><?php echo lang('places_Sport_halls'); ?></h2>
<a href="<?php echo site_url('admin/places/edit'); ?>"><?php echo lang('places_Add_sport_hall');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('places_Name');?></td>
      <td><?php echo lang('places_Address');?></td>
      <td><?php echo lang('places_Phone');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($places as $place): ?>
  <tr>
  <td><?php echo $place['name']." (".$place['shortname'].")"; ?></td>
  <td>
    <?php echo $place['number']." ".$place['street']; ?><br />
    <?php echo $place['postalcode']." ".$place['city']; ?>
  </td>
  <td><?php echo $place['phone']; ?></td>
  <td><a href="<?php echo site_url('admin/places/edit/'.$place['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <a href="<?php echo site_url('admin/places/delete/'.$place['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($places)): ?>
    <td colspan="5"><?php echo lang('places_No_sport_hall'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
</div>

