<div class="contentblock">
<h2><?php echo $competition['name']?></h2>
<a href="<?php echo site_url('admin/matches/p/'.$competition['id']); ?>"><?php echo lang('competitions_View_all_matches');?></a>
<?php if(!empty($matchesWithoutScore)): ?>
<h3><?php echo lang('competitions_Matches_without_score'); ?></h3>
<table>
  <thead>
    <tr>
      <td><?php echo lang('matches_Date');?></td>
      <td><?php echo lang('matches_Teams');?></td>
      <td><?php echo lang('matches_Score');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($matchesWithoutScore as $match): ?>
  <tr>
  <td><?php echo formatDate($match['date'], 'datetimenumeric'); ?></td>
  <td><?php echo $match['teamName1'].' vs '.$match['teamName2']; ?></td>
  <td>? - ?</td>
  <td>
    <a href="<?php echo site_url('admin/matches/edit/'.$match['id'].'/'.$competition['id']); ?>">
      <?php echo lang('admin_Edit'); ?>
    </a>
  </td>
  <td>
    <a href="<?php echo site_url('admin/matches/delete/'.$match['id'].'/'.$competition['id']); ?>">
      <?php echo lang('admin_Delete'); ?>
    </a>
  </td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<h3><?php echo lang('competitions_Upcoming_matches'); ?></h3>
<a href="<?php echo site_url('admin/matches/create/'.$competition['id']); ?>"><?php echo lang('matches_Add_match');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('matches_Date');?></td>
      <td><?php echo lang('matches_Teams');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($upcomingMatches as $match): ?>
  <tr>
  <td><?php echo formatDate($match['date'], 'datetimenumeric'); ?></td>
  <td><?php echo $match['teamName1'].' vs '.$match['teamName2']; ?></td>
  <td>
    <a href="<?php echo site_url('admin/matches/edit/'.$match['id'].'/'.$competition['id']); ?>">
      <?php echo lang('admin_Edit'); ?>
    </a>
  </td>
  <td>
    <a href="<?php echo site_url('admin/matches/delete/'.$match['id'].'/'.$competition['id']); ?>">
      <?php echo lang('admin_Delete'); ?>
    </a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($upcomingMatches)): ?>
  <tr>
  <td colspan="4"><?php echo lang('competitions_No_upcoming_match'); ?>
  </td>
  </tr>
  <?php endif; ?>
  </tbody>
</table>
<h3><?php echo lang('competitions_Teams'); ?></h3>
<a href="<?php echo site_url('admin/competitions/addteam/'.$competition['id']); ?>"><?php echo lang('competitions_Add_team');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('teams_Name');?></td>
      <td><?php echo lang('competitions_Remove_team');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($teams as $team): ?>
  <tr>
  <td><?php echo $team['name']; ?></td>
  <td>
    <a href="<?php echo site_url('admin/competitions/removeteam/'.$competition['id'].'/'.$team['id']); ?>">
      <?php echo lang('competitions_Remove_team'); ?>
    </a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($teams)): ?>
  <tr>
  <td colspan="2"><?php echo lang('competitions_No_team'); ?>
  </td>
  </tr>
  <?php endif; ?>
  </tbody>
</table>
</div>
