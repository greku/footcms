<div class="contentblock">
<h2><?php echo lang('players_Players'); ?></h2>
<a href="<?php echo site_url('admin/players/edit'); ?>"><?php echo lang('players_Add_player');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('players_Name');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($players as $player): ?>
  <tr>
  <td><a href="<?php echo site_url('admin/players/show/'.$player['id']); ?>"><?php echo $player['lastname'].' '.$player['firstname']; ?></a></td>
  <td><a href="<?php echo site_url('admin/players/edit/'.$player['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <a href="<?php echo site_url('admin/players/delete/'.$player['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($players)): ?>
    <td colspan="3"><?php echo lang('players_No_player'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
</div>

