<div class="contentblock">
  <?php if($user['id']==''):?>
  <h2><?php echo lang('users_Create_user'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('users_Edit_user'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/users/edituser/'.$user['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('users_Username'); ?>&nbsp;:</td>
      <td><input name="username" value="<?php echo set_value('username', $user['username']); ?>" size="25" maxlength="14" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('users_Email'); ?>&nbsp;:</td>
      <td><input name="email" value="<?php echo set_value('email', $user['email']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('users_Group'); ?>&nbsp;:</td>
      <td>
      <?php if((int)$user['id']!=1): ?>
      <select name="groupId">
      <?php foreach ($groups as $group): ?>
        <option value="<?php echo $group['id']; ?>" <?php echo ($group['id']==set_value('groupId', $user['groupId'])?'selected="selected"':'');?>><?php echo (lang('group_name_'.$group['name'])=='group_name_'.$group['name']?$group['name']:lang('group_name_'.$group['name']));?></option>
      <?php endforeach; ?>
      </select>
      <?php else: ?>
      <input name="groupId" value="1" type="hidden" />
      <?php echo lang('group_name_Administrator'); ?>
      <?php endif; ?>
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('users_Password'); ?>&nbsp;:</td>
      <td><input name="password" value="" size="25" maxlength="255" type="password"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('users_Re-enter_password'); ?>&nbsp;:</td>
      <td><input name="repassword" value="" size="25" maxlength="255" type="password"></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

