<div class="contentblock">
  <h2><?php echo lang('matches_Add_match'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/matches/create/'.$competition['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('matches_First_team'); ?>&nbsp;:</td>
      <td>
      <select name="team1">
      <?php foreach ($teams as $team): ?>
        <option value="<?php echo $team['id']; ?>" <?php echo ($team['id']==set_value('team1')?'selected="selected"':'');?>>
          <?php echo $team['name'];?>
        </option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Second_team'); ?>&nbsp;:</td>
      <td>
      <select name="team2">
      <?php foreach ($teams as $team): ?>
        <option value="<?php echo $team['id']; ?>" <?php echo ($team['id']==set_value('team2')?'selected="selected"':'');?>>
          <?php echo $team['name'];?>
        </option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Date'); ?>&nbsp;:</td>
      <td><input name="date" value="<?php echo formatDate(set_value('date'), 'form'); ?>" size="16" maxlength="12" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('matches_Time'); ?>&nbsp;:</td>
      <td><input name="time" value="<?php echo set_value('time'); ?>" size="16" maxlength="12" type="text"></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

