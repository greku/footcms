<div class="contentblock">
  <h2><?php echo lang('competitions_Add_team'); ?></h2>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/competitions/addteam/'.$competition['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <?php if(!empty($teams)): ?>
    <tr valign="top">
      <td><?php echo lang('competitions_Team'); ?>&nbsp;:</td>
      <td>
      <select name="team">
      <?php foreach ($teams as $team): ?>
        <option value="<?php echo $team['id']; ?>" <?php echo ($team['id']==set_value('team')?'selected="selected"':'');?>><?php echo $team['name'];?></option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    <?php else: ?>
    <tr>
      <td><?php echo lang('competitions_No_teams_available_that_is_not_in_competition'); ?></td>
    </tr>
    <?php endif; ?>
    </tbody></table>
  </form>
</div>

