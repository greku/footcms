<div class="contentblock">
  <h2><?php echo lang('admin_Configuration'); ?></h2>
  <ul>
    <li><a href="<?php echo site_url('admin/config/menu'); ?>"><?php echo lang('config_Site_menu'); ?></li></a>
    <li><a href="<?php echo site_url('admin/blocks'); ?>"><?php echo lang('config_Home_page_blocks'); ?></li></a>
    <li><a href="<?php echo site_url('admin/users'); ?>"><?php echo lang('config_Users_and_groups'); ?></li></a>
    <li><a href="<?php echo site_url('admin/config/general'); ?>"><?php echo lang('config_General_settings'); ?></li></a>
    <li><a href="<?php echo site_url('admin/modules'); ?>"> <?php echo lang('config_Modules'); ?></li></a>
    <li><a href="<?php echo site_url('admin/config/nextseason'); ?>"> <?php echo lang('config_Go_to_next_season'); ?></li></a>
  </ul>
</div>
