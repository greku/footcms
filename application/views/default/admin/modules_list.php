<div class="contentblock">
<h2><?php echo lang('modules_Modules'); ?></h2>
<table>
  <thead>
    <tr>
      <td><?php echo lang('modules_Name');?></td>
      <td><?php echo lang('modules_Description');?></td>
      <td><?php echo lang('modules_Version');?></td>
      <td><?php echo lang('modules_Author');?></td>
      <td><?php echo lang('modules_Status');?></td>
      <td><?php echo lang('modules_Configure');?></td>
      <td><?php echo lang('modules_Enable/Disable');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($modules as $module): ?>
  <tr <?php echo ($module['enable']==0?'style="color:#777"':'');?>>
  <td><?php echo $module['name']; ?></td>
  <td><?php echo $module['description']; ?></td>
  <td>
    <?php echo $module['version_major'].'.'.$module['version_minor'].($module['version_revision']!=''?'.'.$module['version_revision']:''); ?>
  </td>
  <td><?php echo $module['author']; ?></td>
  <td>
    <?php if($module['status']=='installed'): ?>
      <?php echo lang('modules_Installed'); ?><br />
      <a href="<?php echo site_url($module['name'].'/uninstall'); ?>"><?php echo lang('modules_Uninstall');?></a>
    <?php elseif($module['status']=='corrupted'): ?>
      <span style="color:red;">
        <?php echo lang('modules_Corrupted'); ?>
        (<?php echo $module['error']; ?>)
      </span>
    <?php elseif($module['status']=='notinstalled'): ?>
      <?php echo lang('modules_Not_installed'); ?><br />
      <?php if($module['compatible']=='1'): ?>
        <a href="<?php echo site_url($module['name'].'/install'); ?>"><?php echo lang('modules_Install');?></a>
      <?php else:?>
        <?php echo lang('modules_Error_not_compatible'); ?>
      <?php endif;?>
    <?php elseif($module['status']=='outdated'): ?>
      <?php echo lang('modules_New_version_detected'); ?><br />
      <?php if($module['compatible']=='1'): ?>
        <a href="<?php echo site_url($module['name'].'/update'); ?>"><?php echo lang('modules_Update');?></a>
      <?php else:?>
        <?php echo lang('modules_Error_not_compatible'); ?>
      <?php endif;?>
    <?php endif; ?>
  </td>
  <td>
    <?php if($module['settings']==1 && $module['status']=='installed'): ?>
        <a href="<?php echo site_url($module['name'].'/config'); ?>"><?php echo lang('modules_Configure'); ?></a>
    <?php else: ?>
        &nbsp;
    <?php endif; ?>
  </td>
  <td>
    <?php if($module['status']!='notinstalled'): ?>
      <?php if($module['enable']==1): ?>
        <a href="<?php echo site_url('admin/modules/disable/'.$module['id']); ?>"><?php echo lang('modules_Disable'); ?></a></td>
      <?php else: ?>
        <a href="<?php echo site_url('admin/modules/enable/'.$module['id']); ?>"><?php echo lang('modules_Enable'); ?></a></td>
      <?php endif; ?>
    <?php endif;?>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($modules)): ?>
    <td colspan="7"><?php echo lang('modules_No_module'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
</div>

