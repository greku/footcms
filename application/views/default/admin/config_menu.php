<div class="contentblock">
<h2><?php echo lang('config_Site_menu'); ?></h2>
<table>
  <thead>
    <tr>
      <td><?php echo lang('menu_Rank');?></td>
      <td><?php echo lang('menu_Item');?></td>
      <td><?php echo lang('menu_Move_up'); ?></td>
      <td><?php echo lang('menu_Move_down'); ?></td>
    </tr>
  </thead>
  <tbody>
  <?php $i=0; foreach ($menuItems as $item): $i++;?>
  <tr>
  <td><?php echo $i; ?></td>
  <td><?php echo $item['text']; ?></td>
  <td>
    <?php if($i>1): ?>
    <a href="<?php echo site_url('admin/config/menu/moveup/'.$item['id']); ?>"><?php echo lang('menu_Up'); ?></a>
    <?php endif; ?>
  </td>
  <td>
    <?php if($i<count($menuItems)): ?>
    <a href="<?php echo site_url('admin/config/menu/movedown/'.$item['id']); ?>"><?php echo lang('menu_Down'); ?></a>
    <?php endif; ?>
  </td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
</div>
