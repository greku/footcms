<div class="contentblock">
<h2><?php echo $competition['name']; ?></h2>
<h3><?php echo lang('matches_Matches'); ?></h3>
<a href="<?php echo site_url('admin/matches/create/'.$competition['id']); ?>"><?php echo lang('matches_Add_match');?></a>
<?php echo $pagination; ?>
<table>
  <thead>
    <tr>
      <td><?php echo lang('matches_Date');?></td>
      <td><?php echo lang('matches_Teams');?></td>
      <td><?php echo lang('matches_Score');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($matches as $match): ?>
  <tr>
  <td><?php echo formatDate($match['date'], 'datetimenumeric'); ?></td>
  <td><?php echo $match['teamName1'].' vs '.$match['teamName2']; ?></td>
  <?php if(is_null($match['score1'])): ?>
    <td>&nbsp;</td>
  <?php else: ?>
    <td><?php echo $match['score1'].' - '.$match['score2'].($match['forfeit']==1?' FF':''); ?></td>
  <?php endif; ?>
  <td>
    <a href="<?php echo site_url('admin/matches/edit/'.$match['id'].'/'.$competition['id'].'/l'); ?>">
      <?php echo lang('admin_Edit'); ?>
    </a>
  </td>
  <td>
    <a href="<?php echo site_url('admin/matches/delete/'.$match['id'].'/'.$competition['id'].'/l'); ?>">
      <?php echo lang('admin_Delete'); ?>
    </a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($matches)): ?>
  <tr>
  <td colspan="5"><?php echo lang('competitions_No_matches'); ?>
  </td>
  </tr>
  <?php endif; ?>
  </tbody>
</table>
<?php echo $pagination; ?>
</div>

