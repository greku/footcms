<div class="contentblock">
<h2><?php echo $player['lastname'].' '.$player['firstname']; ?></h2>
<p>
  <?php echo lang('players_Date_of_birth').': '.formatDate($player['birthday'], 'datetextlong'); ?><br />
  <?php echo lang('players_Email').': '.$player['email']; ?><br />
  <?php echo lang('players_City').': '.$player['city']; ?><br />
</p>
<h3><?php echo lang('players_Teams'); ?></h3>
<table>
  <thead>
    <tr>
      <td><?php echo lang('players_Season'); ?></td>
      <td><?php echo lang('teams_Name');?></td>
      <td><?php echo lang('players_Shirt_number');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($teams as $team): ?>
  <tr>
  <td><?php echo $team['year']; ?> - <?php echo $team['year']+1; ?></td>
  <td><?php echo $team['name']; ?></td>
  <td><?php echo (is_null($team['number'])?'':$team['number']); ?></td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($teams)): ?>
  <tr>
  <td colspan="3"><?php echo lang('players_No_team'); ?>
  </td>
  </tr>
  <?php endif; ?>
  </tbody>
</table>
<h3><?php echo lang('players_Current_season_stats'); ?></h3>
<p>
  <?php echo lang('players_Number_of_matches_played').': '.$matches; ?><br />
  <?php echo lang('players_Number_of_goals').': '.$goals; ?><br />
  <?php echo lang('players_Number_of_cards').': '.$cards; ?><br />
</p>
</div>

