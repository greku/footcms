<?php
loadJs(array('jquery', 'ckeditor/ckeditor', 'ckeditor/adapters/jquery', 'news_editor'));
?>
<div class="contentblock">
  <?php if($news['id']==''):?>
  <h2><?php echo lang('news_Add_news'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('news_Edit_news'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/news/edit/'.$news['id']); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('news_Title'); ?>&nbsp;:</td>
      <td><input name="title" value="<?php echo set_value('title', $news['title']); ?>" size="50" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('news_Content'); ?>&nbsp;:</td>
      <td><textarea name="content" cols="70" rows="10"><?php echo set_value('content', $news['content']); ?></textarea></td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

