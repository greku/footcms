<div class="contentblock">
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <h2><?php echo lang('explorer_Upload'); ?></h2>
  <h3><?php echo lang('explorer_Picture_upload'); ?></h3>
  <form enctype="multipart/form-data" action="<?php echo site_url('admin/explorer/upload/'.$folder['id']); ?>" method="post">
  <input name="formToken" value="<?php echo $formToken; ?>" type="hidden" />
  <table width="100%">
    <input name="type" value="sendpicture" type="hidden" />
    <input name="MAX_FILE_SIZE" value="2097152" type="hidden" />
    <tbody>
      <tr valign="top">
        <td><?php echo lang('explorer_File'); ?>&nbsp;:</td>
        <td><input name="filename" value="" size="25" type="file" /></td>
      </tr>
      <tr valign="top">
        <td><?php echo lang('explorer_Resizing');?>&nbsp;:</td>
        <td><select name="resizing">
          <option value="3" selected="selected"><?php echo lang('explorer_Resize_keep_ratio'); ?></option>
          <option value="2"><?php echo lang('explorer_Free_resize_no_ratio'); ?></option>
          <option value="1"><?php echo lang('explorer_No_resize');?></option>
          </select>
        </td>
      </tr>
      <tr valign="top">
        <td><?php echo lang('explorer_Width'); ?> (px)&nbsp;:</td>
        <td><input name="width" value="600" size="10" type="text"></td>
      </tr>
      <tr valign="top">
        <td><?php echo lang('explorer_Height'); ?> (px)&nbsp;:</td>
        <td><input name="height" value="400" size="10" type="text" /></td>
      </tr>
      <tr>
        <td colspan="2"><input value="<?php echo lang('explorer_Upload');?>" type="submit" /></td>
      </tr>
    </tbody>
  </table>
  </form>
  <h3><?php echo lang('explorer_File_upload'); ?></h3>
  <form enctype="multipart/form-data" action="<?php echo site_url('admin/explorer/upload/'.$folder['id']); ?>" method="post">
  <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
  <p>
    <input name="type" value="sendfile" type="hidden" />
    <input name="MAX_FILE_SIZE" value="2097152" type="hidden" />
    File&nbsp;:&nbsp;<input name="filename[]" value="" size="25" type="file" />
    <input value="<?php echo lang('explorer_Upload');?>" type="submit" />
  </p>
  </form>
</div>

