<div class="contentblock">
<h2><?php echo lang('reports_Match_reports'); ?></h2>
<a href="<?php echo site_url('admin/reports/create'); ?>"><?php echo lang('reports_Create_report');?></a>
<?php echo $pagination; ?>
<table>
  <thead>
    <tr>
      <td><?php echo lang('reports_Date');?></td>
      <td><?php echo lang('reports_Match');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($reports as $report): ?>
  <tr>
  <td><?php echo formatDate($report['date'], 'datenumeric'); ?></td>
  <td><?php echo $report['title']; ?></td>
  <td><a href="<?php echo site_url('admin/reports/edit/'.$report['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <a href="<?php echo site_url('admin/reports/delete/'.$report['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($reports)): ?>
    <td colspan="4"><?php echo lang('reports_No_report'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
<?php echo $pagination; ?>
</div>

