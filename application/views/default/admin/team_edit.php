<?php
loadJs(array('jquery', 'fancybox', 'browsepicture'));
loadCss('fancybox');
?>
<div class="contentblock">
  <?php if($team['id']=='0'):?>
  <h2><?php echo lang('teams_Create_team'); ?></h2>
  <?php else: ?>
  <h2><?php echo lang('teams_Edit_team'); ?></h2>
  <?php endif; ?>
  <?php $val_errors=validation_errors(); echo ($val_errors!=''?'<div class="messageValidation">'.$val_errors.'</div>':''); ?>
  <form method="post" action="<?php echo site_url('admin/teams/edit/'.$team['id'].'/'.$clubId); ?>">
    <input name="formToken" value="<?php echo $formToken; ?>" type="hidden">
    <table width="100%">
    <tbody>
    <tr valign="top">
      <td><?php echo lang('teams_Name'); ?>&nbsp;:</td>
      <td><input name="name" value="<?php echo set_value('name', $team['name']); ?>" size="30" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('teams_Contact_name'); ?>&nbsp;:</td>
      <td><input name="contactName" value="<?php echo set_value('contactName', $team['contactName']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('teams_Contact_phone'); ?>&nbsp;:</td>
      <td><input name="contactPhone" value="<?php echo set_value('contactPhone', $team['contactPhone']); ?>" size="25" maxlength="32" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('teams_Contact_email'); ?>&nbsp;:</td>
      <td><input name="contactEmail" value="<?php echo set_value('contactEmail', $team['contactEmail']); ?>" size="25" maxlength="255" type="text"></td>
    </tr>
    <tr valign="top">
      <td><?php echo lang('teams_Place'); ?>&nbsp;:</td>
      <td>
      <select name="placeId">
        <option value="0" <?php echo (is_null(set_value('placeId', $team['placeId']))?'selected="selected"':'');?>>
          <?php echo lang('teams_No_place'); ?>
        </option>
      <?php foreach ($places as $place): ?>
        <option value="<?php echo $place['id']; ?>" <?php echo ($place['id']==set_value('placeId', $team['placeId'])?'selected="selected"':'');?>>
          <?php echo $place['name'];?>
        </option>
      <?php endforeach; ?>
      </select>
      </td>
    </tr>
    <tr>
      <td>
        <?php echo lang('teams_Team_picture'); ?>&nbsp;:
      </td>
      <td>
        <span class="fileBrowser">
            <span class="noSelection"><?php echo lang('teams_No_picture_selected');?></span>
            <span class="selPath"><?php echo set_value('filePath', $picture['path']); ?></span>
            <input class="fileId" type="hidden" name="fileId" value="<?php echo (int)set_value('fileId', $picture['id']); ?>" />
            <input class="filePath" type="hidden" name="filePath" value="<?php echo set_value('filePath', $picture['path']); ?>" />
            <a class="browsepicture" href="<?php echo site_url('admin/browser/picture');?>"><?php echo lang('teams_Select_picture');?></a>
            <a class="nofile" href="#"><?php echo lang('teams_No_picture');?></a>
        </span>
      </td>
    </tr>
    <tr>
      <td colspan="2"><input value="<?php echo lang('admin_Submit'); ?>" type="submit"></td>
    </tr>
    </tbody></table>
  </form>
</div>

