<div class="contentblock">
<h2><?php echo lang('clubs_Clubs'); ?></h2>
<a href="<?php echo site_url('admin/clubs/edit'); ?>"><?php echo lang('clubs_Add_club');?></a>
<table>
  <thead>
    <tr>
      <td><?php echo lang('clubs_Name');?></td>
      <td><?php echo lang('clubs_Matricule');?></td>
      <td><?php echo lang('admin_Edit');?></td>
      <td><?php echo lang('admin_Delete');?></td>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($clubs as $club): ?>
  <tr>
  <td><a href="<?php echo site_url('admin/clubs/show/'.$club['id']); ?>"><?php echo $club['name']; ?></a></td>
  <td><?php echo $club['matricule']; ?></td>
  <td><a href="<?php echo site_url('admin/clubs/edit/'.$club['id']); ?>"><?php echo lang('admin_Edit'); ?></a></td>
  <td>
    <?php if($club['id']!=$myClubId): ?>
        <a href="<?php echo site_url('admin/clubs/delete/'.$club['id']); ?>"><?php echo lang('admin_Delete'); ?></a>
    <?php else: ?>
        &nbsp;
    <?php endif; ?>
  </td>
  </tr>
  <?php endforeach; ?>
  <?php if(empty($clubs)): ?>
    <td colspan="4"><?php echo lang('clubs_No_club'); ?></td>
  <?php endif; ?>
  </tbody>
</table>
</div>

