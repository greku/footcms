<?php foreach($teams as $team): extract($team);?>
<div class="contentblock" style="clear:both">
<h2><?php echo $name;?></h2>
<?php if(!is_null($photopath)):?>
<img style="float:right;" alt="<?php echo lang('teams_Team').' '.$name; ?>" src="<?php echo $photopath;?>" />
<?php endif;?>
<div id="players">
  <h3><?php echo lang('teams_players');?></h3>
  <ul>
    <?php if(count($players) == 0): ?>
    <li><?php echo lang('teams_no_player');?></li>
    <?php else: ?>
    <?php foreach ($players as $item): ?>
    <li><?php echo $item['firstname'].' '.$item['lastname']; ?></li>
    <?php endforeach; endif;?>
  </ul>
</div>
<div id="team_stats">
  <h3><?php echo lang('teams_stats');?></h3>
  <table>
    <thead>
      <tr>
        <th colspan="2"><?php echo lang('players_goals');?></th>
      </tr>
    </thead>
    <tbody>
    <?php if(count($goalers) == 0): ?>
      <tr><td colspan="2"><?php echo lang('players_no_goal');?></td></tr>
    <?php else: ?>
    <?php foreach ($goalers as $item): ?>
      <tr><td><?php echo $item['firstname'].' '.$item['lastname']; ?></td><td><?php echo $item['nbgoals']; ?></td></tr>
    <?php endforeach; endif;?>
    </tbody>
  </table>
</div>
<div style="clear:both;"></div>
</div>
<?php endforeach; ?>
<?php if(empty($teams)): ?>
<div class="contentblock">
  <?php echo lang('teams_no_team');?>
</div>
<?php endif; ?>
