<?php
loadJs(array('jquery', 'fancybox', 'fancybox.comments'));
loadCss('fancybox');
?>
<div class="contentblock">
<h2><?php echo $title; ?></h2>
<div class="leftspace"> 
<p style="color:gray;"><em><?php echo $author; ?>, <?php echo formatDate($date, 'datetimetextlong');?></em></p>
<?php echo $content; ?>
<?php if($nbcomments>0): ?>
<a href="<?php echo site_url('comments/news/'.$id); ?>" class="fancy_comments"><?php echo lang('comments_view_comments'); ?> (<?php echo $nbcomments ?>)</a>
<?php else: echo lang('comments_no_comments'); endif; ?>
- <a href="<?php echo site_url('comments/news/'.$id.'/add/'); ?>" class="fancy_comments"><?php echo lang('comments_add_comment'); ?></a>
</div>
</div>
