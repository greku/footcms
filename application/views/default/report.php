<?php
loadJs(array('jquery', 'fancybox', 'fancybox.comments'));
loadCss('fancybox');
?> 
<div class="contentblock">
<h2><?php echo $news['title']; ?></h2>
<div class="leftspace"> 
<p style="color:gray;"><em><?php echo $news['author']; ?>, <?php echo formatDate($news['date'], 'datetimetextlong');?></em></p>
<?php echo $news['content']; ?>
<?php if($news['nbcomments']>0): ?>
<a href="<?php echo site_url('comments/news/'.$news['id']); ?>" class="fancy_comments"><?php echo lang('comments_view_comments'); ?> (<?php echo $news['nbcomments']; ?>)</a>
<?php else: echo lang('comments_no_comments'); endif; ?>
- <a href="<?php echo site_url('comments/news/'.$news['id'].'/add/'); ?>" class="fancy_comments"><?php echo lang('comments_add_comment'); ?></a>
</div>

<h3><?php echo lang('report_teams');?></h3>
<table>
<thead>
  <tr>
    <th><?php echo $team1['name']; ?></td>
    <th><?php echo $team2['name']; ?></td>
  </tr>
</thead>
<tbody>
<tr>
  <td>
  <?php 
    foreach ($team1['players'] as $player):
      echo $player['lastName'].' '.$player['firstName'];
      if(!is_null($player['number'])) 
        echo ' ('.$player['number'].')'; 
      echo '<br />';
    endforeach;
    if(count($team1['players'])==0)
      echo lang('report_undefined_team');
  ?> 
  </td>
  <td>
  <?php 
    foreach ($team2['players'] as $player):
      echo $player['lastName'].' '.$player['firstName'];
      if(!is_null($player['number'])) 
        echo ' ('.$player['number'].')'; 
      echo '<br />';
    endforeach;
    if(count($team2['players'])==0)
      echo lang('report_undefined_team');
  ?> 
  </td>
</tr>
</tbody>
</table>
<h3><?php echo lang('report_goals');?></h3>
<table>
<thead>
  <tr>
    <th><?php echo $team1['name']; ?></td>
    <th><?php echo $team2['name']; ?></td>
  </tr>
</thead>
<tbody>
<tr>
  <td>
  <?php 
    foreach ($team1['goals'] as $goal):
      echo $goal['lastName'].' '.$goal['firstName'].'<br />';
    endforeach;
    if(count($team1['players'])==0)
      echo lang('report_undefined_team');
    else if(count($team1['goals'])==0)
      echo lang('report_no_goal');
  ?> 
  </td>
  <td>
  <?php 
    foreach ($team2['goals'] as $goal):
      echo $goal['lastName'].' '.$goal['firstName'].'<br />';
    endforeach;
    if(count($team2['players'])==0)
      echo lang('report_undefined_team');
    else if(count($team2['goals'])==0)
      echo lang('report_no_goal');
  ?> 
  </td>
</tr>
</tbody>
</table>
<h3><?php echo lang('report_cards');?></h3>
<table>
<thead>
  <tr>
    <th><?php echo $team1['name']; ?></td>
    <th><?php echo $team2['name']; ?></td>
  </tr>
</thead>
<tbody>
<tr>
  <td>
  <?php 
    foreach ($team1['cards'] as $card):
      echo $card['lastName'].' '.$card['firstName'];
      echo ' ('.lang('report_'.$card['color']).')';
      echo '<br />';
    endforeach;
    if(count($team1['players'])==0)
      echo lang('report_undefined_team');
    else if(count($team1['cards'])==0)
      echo lang('report_no_card');
  ?> 
  </td>
  <td>
  <?php 
    foreach ($team2['cards'] as $card):
      echo $card['lastName'].' '.$card['firstName'];
      echo ' ('.lang('report_'.$card['color']).')';
      echo '<br />';
    endforeach;
    if(count($team2['players'])==0)
      echo lang('report_undefined_team');
    else if(count($team2['cards'])==0)
      echo lang('report_no_card');
  ?> 
  </td>
</tr>
</tbody>
</table>
</div>
