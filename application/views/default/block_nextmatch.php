<h2><?php echo lang('match_Upcoming_matches'); ?></h2>
<?php foreach($matches as $match): extract($match);?>
<p>
  <?php echo ucfirst(formatDate($date, 'datetimetextshortday')); ?>: 
  <?php echo sprintf(lang('match_%s_vs_%s'), $teamName1, $teamName2); ?>
  (<?php echo $competitionName; ?>)
</p>
<?php endforeach; ?>
<?php if(empty($matches)): ?>
<?php echo lang('match_No_upcoming_match');?>
<?php else: ?>
<a href="<?php echo site_url('matches'); ?>"><?php echo lang('match_All_matches');?></a>
<?php endif; ?>
