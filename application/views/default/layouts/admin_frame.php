<?php
  echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo $header_title; ?></title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <meta name="generator" content="FootCMS" />
  <link href="<?php echo $css_folder; ?>admin.css" rel="stylesheet" type="text/css" />
  <?php foreach($header_css as $path): ?>
  <link href="<?php echo $path; ?>" rel="stylesheet" type="text/css" />
  <?php endforeach; ?>
  <?php foreach($header_js as $path): ?>
  <script src="<?php echo $path; ?>" type="text/javascript"></script>
  <?php endforeach; ?>
  <?php echo $header_more; ?>
</head>

<body class="frame">
<div id="PageFrame">
    <div id="ContentFrame">
      <?php echo $page_content ?>
    </div>
</div>
<div id="FooterFrame">
  <?php echo $footer_content ?>
</div>
</body>
</html>
