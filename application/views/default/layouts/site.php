<?php 
  echo '<?xml version="1.0" encoding="utf-8" ?>'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo $header_title; ?></title>
  <meta name="generator" content="FootCMS" />
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link href="<?php echo $css_folder; ?>site.css" rel="stylesheet" type="text/css" />
  <?php foreach($header_css as $path): ?>
  <link href="<?php echo $path; ?>" rel="stylesheet" type="text/css" />
  <?php endforeach; ?>
  <?php foreach($header_js as $path): ?>
  <script src="<?php echo $path; ?>" type="text/javascript"></script>
  <?php endforeach; ?>
  <?php echo $header_more; ?>
</head>

<body>
<div id="Page">
  <div id="Top">
    <div id="TopPlayers">
      <div id="Banner">
        <?php echo $club_name; ?>
      </div>
    </div>
  </div>
  <div id="Middle">
    <div id="TopMenu">
      <?php foreach ($navigation_menu as $item): ?>
        <div class="menu_element"><a href="<?php echo site_url($item['link']); ?>"><?php echo lang($item['name']); ?></a></div>
      <?php endforeach; ?>
    </div>
    <div id="Content">
      <?php echo $page_content ?>
    </div>
  </div>
  <div id="Bottom">
    <div id="Footer">
      <?php echo $footer_content ?>
    </div>
  </div>
</div>
</body>
</html>
