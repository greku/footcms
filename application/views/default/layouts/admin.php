<?php echo '<?xml version="1.0" encoding="utf8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo $header_title; ?></title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <meta name="generator" content="FootCMS" />
  <link href="<?php echo $css_folder; ?>admin.css" rel="stylesheet" type="text/css" />
  <?php foreach($header_css as $path): ?>
  <link href="<?php echo $path; ?>" rel="stylesheet" type="text/css" />
  <?php endforeach; ?>
  <?php foreach($header_js as $path): ?>
  <script src="<?php echo $path; ?>" type="text/javascript"></script>
  <?php endforeach; ?>
  <?php echo $header_more; ?>
</head>

<body>
<div id="Page">
  <div id="Top">
     <div id="Banner">
       <h1>Administration Panel FOOTCMS</h1>
     </div>
  </div>
  <div id="Middle">
    <div id="Menu">
      <?php foreach ($navigation_menu as $link): ?>
        <h3><a href="<?php echo site_url($link['link']); ?>"><?php echo lang($link['name']); ?></a></h3>
        <?php if(isset($link['sub_menu']) && count($link['sub_menu'])>0): ?>
          <ul>
            <?php foreach ($link['sub_menu'] as $sublink): ?>
              <li><a href="<?php echo site_url($sublink['link']); ?>"><?php echo lang($sublink['name']); ?></a></li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
    <div id="Content">
      <div class="contentblock">
       <?php
          $elements = array();
          foreach ($breadcrumb as $item){
              if(isset($item['link']) && ($item['link']!=''))
                  $elements[]='<a href="'.site_url($item['link']).'">'.$item['name'].'</a>';
              else
                  $elements[]=$item['name'];
          }
          echo implode($elements, ' &gt; ');
       ?>
      </div>
      <?php echo $page_content; ?>
      <div id="Footer">
        <?php echo $footer_content; ?>
      </div>
    </div>
  </div>
</div>
</body>
</html>
