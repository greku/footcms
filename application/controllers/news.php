<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class News extends FCMS_Controller {

	function __construct(){
		parent::__construct();
	}
	
    function index(){
        $this->p();
    }
    
	function p($page=1){
        $newsPerPage=7;
        $this->load->library('pagination');
        /* count rows */
        $this->load->model('Newsmodel');
        $n=$this->Newsmodel->count();
        /* url for pagination */
        $this->pagination->setBaseUrl(site_url('news').'/p/');
        $firstOfPage=$this->pagination->setPage($n, (int)$page, $newsPerPage);
        if($firstOfPage===false){
            show_404();
            exit();
        }
        $data['pagination']=$this->pagination->create_links();
        $data['news'] = $this->Newsmodel->getList($firstOfPage, $newsPerPage);
        
        $this->setContent($this->load->view('news',$data, true));
        $this->render();
	}
    
    function show($id=0){
        $this->load->model('Newsmodel');
        $data=$this->Newsmodel->get((int)$id);
        if($data===false){
            show_404();
            exit();
        }
        else {
            $this->setContent($this->load->view('news_detail',$data, true));
        }
        $this->render();
    }
}

?>
