<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Teams extends FCMS_Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $this->load->model('Teammodel');
        $this->load->model('Playermodel');
        $data['teams']=$this->Teammodel->getTeams($this->config->fcms('myClubId'), $this->config->fcms('seasonId'));

        foreach ($data['teams'] as &$team){
            /* Get players for each teams */
            $team['players']=$this->Playermodel->getPlayers($team['id']);
            /* Get goalers */
            $team['goalers']=$this->Playermodel->getGoalersFromTeam($team['id'], $this->config->fcms('seasonId'));
        }
        $this->setContent($this->load->view('teams',$data, true));
        $this->render();
    }

}

?>
