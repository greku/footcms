<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Comments extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('site_frame');
    }

    function news($id=0, $action='show', $paget='p', $pagen=1){
        /* check news exists */
        $this->load->model('Newsmodel');
        $news=$this->Newsmodel->getBasic($id);
        if($news===false) {
            show_404();
            exit();
        }
        $this->load->model('Commentmodel');
        $data['type']='news';
        $data['id']=(int)$id;
        if($action=="add"){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('gal', 'lang:comments_yourname', 'trim|required|min_length[2]|max_length[25]|xss_clean');
            $this->form_validation->set_rules('ticori', 'lang:comments_comment', 'required|xss_clean');
            $this->form_validation->set_rules('abira', 'lang:captcha_Word_verification', 'required|valid_captcha');
            if($this->form_validation->run()==false || preg_match('/href=/i', $this->input->post('comment'))>0) {
                $data['formToken']=$this->form_validation->formToken;
                $this->setContent($this->load->view('comments_add',$data, true));
            }
            else {
                if($this->config->fcms('commentValidation')==1)
                    $validated='NO';
                else
                    $validated='YES';
                $this->Commentmodel->insert($id, 'news',
                    $this->input->post('gal'),
                    $this->input->post('ticori'),
                    $validated);
                $commentsPerPage=5;
                $this->load->library('pagination');
                $n=$this->Commentmodel->count('news', (int)$id);
                /* url for pagination */
                $this->pagination->setBaseUrl(site_url('comments/news/'.$id.'/show/p/'));
                $firstOfPage=$this->pagination->setPage($n, (int)$pagen, $commentsPerPage);
                if($firstOfPage===false){
                    show_404();
                    exit();
                }
                $data['pagination']=$this->pagination->create_links();
                $data['comments']=$this->Commentmodel->listComments('news', (int)$id, $firstOfPage, $commentsPerPage);
                $data['commentAdded']=true;
                if($this->config->fcms('commentValidation')==1)
                    $data['waitValidation']=true;
                $this->setContent($this->load->view('comments',$data, true));
            }
            $this->render();
        }
        elseif ($action=="show"){
            $commentsPerPage=5;
            $this->load->library('pagination');
            $n=$this->Commentmodel->count('news', (int)$id);
            /* url for pagination */
            $this->pagination->setBaseUrl(site_url('comments/news/'.$id.'/show/p/'));
            $firstOfPage=$this->pagination->setPage($n, (int)$pagen, $commentsPerPage);
            if($firstOfPage===false){
                show_404();
                exit();
            }
            $data['pagination']=$this->pagination->create_links();
            $data['comments']=$this->Commentmodel->listComments('news', (int)$id, $firstOfPage, $commentsPerPage);
            $this->setContent($this->load->view('comments',$data, true));
            $this->render();
        }
        else {
            show_404();
            exit();
        }
    }

}

?>
