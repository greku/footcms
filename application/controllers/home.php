<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Home extends FCMS_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
        $this->load->model('Blockmodel');
        $blocks = $this->Blockmodel->getBlocks(1);
        $blockOutputs=array();
        foreach ($blocks as $block){
            $r=null;
            if(is_null($block['moduleId'])){
                $r=$this->renderBlock($block['shortname'], unserialize($block['params']));
            }
            else {
                $this->load->module($block['moduleName']);
                $controller  =$block['moduleName'].'_blocks';
                $r=$this->$block['moduleName']->controller->$controller->$block['shortname']($block['params']);
                $this->set_instance($this);
            }
            if(!is_null($r))
                $blockOutputs[]=$r;
        }
        $out=$this->load->view('home', array('blocks'=>$blockOutputs), true);
		$this->setContent($out);
        $this->render();
	}
    
    private function renderBlock($blockname, $params){
        global $FOOTCMS;
        $out='';
        if($blockname=='latestnews'){
            /*
              description:
                show a list of news (with/without shorttext)
              params:
                nbnews   def 5
                shorttext:     1 or 0 def 0
            */
            $this->load->model('Newsmodel');
            $data=array();
            $noShortText=$params['shorttext']==0?true:false;
            $data['news']=$this->Newsmodel->getList(0, (int)$params['nbnews'], $noShortText);
            if($noShortText)
                $out=$this->load->view('block_news_titles', $data, true);
            else
                $out=$this->load->view('block_news_shorttext', $data, true);
        }
        //change it to last match ?
        /*elseif ($blockname=='fullnews'){
            
            description:
                show the last news (or match report) entirely
            params :
                includereport : 1 or 0 def 0
            
            $this->load->model('Newsmodel');
            $data=array();
            $data['news']=$this->Newsmodel->get('last');
            $out=$this->load->view('block_onenews', $data, true);
        }*/        
        else if ($blockname=='nextmatch'){
            /*
            description:
                show a list of next matches in future
            params:
                nbmatch: def 3
            */
            $this->load->model('Matchmodel');
            $data=array();
            $data['matches']=$this->Matchmodel->getNext($params['nbmatch']);
            $out=$this->load->view('block_nextmatch', $data, true);
        }
        else {
            $out.='Unknown block';
        }
        return $out;
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */

?>
