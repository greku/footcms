<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Install extends CI_Controller {

    function __construct(){
        parent::__construct();
    }

    function language(){
        $this->load->helper('url');
        $this->load->helper('language');
        $this->lang->load('install');
        header('Content-Type: text/html; charset=utf-8');
        $this->load->view('install/lang', array());
    }

    function check($lang='english'){
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('language');
        if($lang!='french') {
            $lang = 'english';
        }
        $this->config->set_item('language', $lang);
        $this->lang->load('install');
        header('Content-Type: text/html; charset=utf-8');
        $data['tests']=array();
        $errors=array();
        $warnings=array();
        //database connection
        $test['name']='Database_connection';
        $this->load->database();
        $ret = $this->db->initialize();
        if ($ret==true)
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]=sprintf(lang("install_Connection to database failed (host=%s, username=%s, password=%s)"), $this->db->hostname, $this->db->username, $this->db->password);
        }
        $data['tests'][]=$test;
        //database name
        $test['name']='Database_selection';
        $ret =  $this->db->db_select();
        if ($ret==true)
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]=sprintf(lang("install_Database selection failed (database=%s)"), $this->db->database);
            //var_dump($this->db->database);
        }
        $data['tests'][]=$test;
        // GD
        $test['name']='GD library';
        if (extension_loaded('gd'))
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]="install_GD library extension is not loaded";
        }
        $data['tests'][]=$test;
        $gdinfo = gd_info();
        // JPEG support
        $test['name']='JPEG support';
        if (isset($gdinfo['JPEG Support']) && $gdinfo['JPEG Support']===true)
            $test['result']=1;
        elseif (isset($gdinfo['JPG Support']) && $gdinfo['JPG Support']===true)
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]="install_JPEG support of GD is not available";
        }
        $data['tests'][]=$test;
        // GIF support
        $test['name']='GIF support';
        if (isset($gdinfo['GIF Read Support']) && $gdinfo['GIF Read Support']===true
            && isset($gdinfo['GIF Create Support']) && $gdinfo['GIF Create Support']===true)
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]="install_GIF support of GD is not available";
        }
        $data['tests'][]=$test;
        // PNG support
        $test['name']='PNG support';
        if (isset($gdinfo['PNG Support']) && $gdinfo['PNG Support']===true)
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]="install_PNG support of GD is not available";
        }
        $data['tests'][]=$test;
        // Upload foler read/writable
        $test['name']='Upload folder';
        if (is_writable(str_replace('system/','', BASEPATH).'upload'))
            $test['result']=1;
        else {
            $test['result']=2;
            $errors[]="install_upload folder must be readable and writable";
        }
        $data['tests'][]=$test;
        //free type support in GD???

        $tables = $this->db->list_tables();
        if (in_array($this->db->dbprefix.'settings', $tables)) {
            // check if footcms is already installed
            $request="SELECT `settings`.`id`, `settings`.`name`, `settings`.`value`"."\n"
                ."FROM `".$this->db->dbprefix."settings` AS `settings`"."\n"
                ."WHERE `name`='version_major'"."\n"
                ."OR `name`='version_minor'"."\n"
                ."OR `name`='version_revision'"."\n"
                ."ORDER BY `name` ASC"."\n";
            $query = $this->db->query($request);
            $version = $query->result_array();
        }
        if (!empty($version) && $version!==false ) {
            $test['name']='Currently_installed_version_supported_for_update';
            $test['result']=1;
            $installType='error';
            $installDetail='';
            //footcms is already installed
            if ($version[0]['value']==1
                && $version[1]['value']==4
                && $version[2]['value']==1){
                $installType='update';
                $installDetail='1_4_1_to_1_4_2';
            }
            else {
                $errors[]=sprintf(lang("install_Update_from_version_%s_not_supported"),
                    $version[0]['value'].'.'.$version[1]['value'].'.'.$version[2]['value']);
                $test['result']=2;
            }
            $data['tests'][]=$test;
        }
        else {
            $installType='complete';
            $installDetail='1_4_2';
            // footcms not yet in database
            $test['name']='Tables_not_in_use';
            $test['result']=1;
            $fcms_tables = array($this->db->dbprefix.'blocks',
                $this->db->dbprefix.'cards',
                $this->db->dbprefix.'clubs',
                $this->db->dbprefix.'comments',
                $this->db->dbprefix.'competitions',
                $this->db->dbprefix.'files',
                $this->db->dbprefix.'file_types',
                $this->db->dbprefix.'goals',
                $this->db->dbprefix.'groups',
                $this->db->dbprefix.'groups_permissions',
                $this->db->dbprefix.'interlinks',
                $this->db->dbprefix.'matches',
                $this->db->dbprefix.'menu',
                $this->db->dbprefix.'modules',
                $this->db->dbprefix.'m_contactus_messages',
                $this->db->dbprefix.'news',
                $this->db->dbprefix.'permissions',
                $this->db->dbprefix.'places',
                $this->db->dbprefix.'players',
                $this->db->dbprefix.'players_matches',
                $this->db->dbprefix.'players_teams',
                $this->db->dbprefix.'seasons',
                $this->db->dbprefix.'sessions',
                $this->db->dbprefix.'settings',
                $this->db->dbprefix.'teams',
                $this->db->dbprefix.'teams_competitions',
                $this->db->dbprefix.'type_competition',
                $this->db->dbprefix.'users'
                );
            foreach ($tables as $table) {
                if (in_array($table, $fcms_tables)){
                    $test['result']=2;
                    $errors[]=sprintf(lang("install_Table with name %s already exists"), $table);
                }
            }
            $data['tests'][]=$test;
        }
        $data['errors']=$errors;
        $data['warnings']=$warnings;
        $data['lang']=$lang;
        $data['installType']=$installType;
        $data['installDetail']=$installDetail;
        if (empty($errors)) {
            $this->session->set_userdata('validSetup', time());
            $this->session->set_userdata('installType', $installType.'_'.$installDetail);
        }
        $this->load->view('install/check', $data);
    }

    function complete_1_4_2($lang='english') {
        $this->load->library('session');
        $this->load->helper('url');
        $timecheck = $this->session->userdata('validSetup');
        $installType = $this->session->userdata('installType');
        if ($timecheck===false || (time()-$timecheck)>1800 || $installType!='complete_1_4_2')
            header('Location:'.site_url('install/check/'.$lang));
        $this->load->helper('language');
        if($lang!='french') {
            $lang = 'english';
        }
        $this->config->set_item('language', $lang);
        $this->lang->load('install');
        $this->load->database();
        $this->load->library('form_validation');
    // test not already installed...
        $this->form_validation->set_rules('clubname', 'lang:install_Name_of_your_club', 'trim|required|min_length[3]|max_length[255]|xss_clean');
        $this->form_validation->set_rules('clubmatricule', 'lang:install_Matricule_of_your_club', 'trim|max_length[10]|xss_clean');
        $this->form_validation->set_rules('adminemail', 'lang:install_Administrator_email', 'trim|required|min_length[3]|max_length[255]|valid_email');
        $this->form_validation->set_rules('adminpass', 'lang:install_Administrator_password', 'trim|required|min_length[6]|max_length[255]');
        $this->form_validation->set_rules('adminrepass', 'lang:install_Administrator_password', 'trim|required|min_length[6]|max_length[255]|matches[adminpass]');
        $this->form_validation->set_rules('mail_no-reply', 'lang:install_No-reply_email', 'trim|required|min_length[3]|max_length[255]|valid_email');
        $this->form_validation->set_rules('language', 'lang:config_Language', 'trim|required|callback__validLanguage');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['db_hostname']=$this->db->hostname;
            $data['db_username']=$this->db->username;
            $data['db_password']=$this->db->password;
            $data['db_database']=$this->db->database;
            $data['db_tableprefix']=$this->db->dbprefix;
            $data['languages']=array('english', 'french');
            $data['lang']=$lang;
            $this->load->view('install/complete', $data);
        }
        else {
            $errors=array();
            $this->load->model('Clubmodel');
            $this->load->model('Settingmodel');
            $this->load->model('Usermodel');
            $sqlcontent=file_get_contents(str_replace('system/','', BASEPATH).'application/sql/complete.sql');
            $sqlcontent=preg_replace('/tableprefix_/', $this->db->dbprefix, $sqlcontent);
            $query='';
            foreach(preg_split("/(\r?\n)/", $sqlcontent) as $line){
                $query .= $line."\n";
                if (substr(rtrim($query), -1)==';'){
                    $ret=$this->db->query($query);
                    if($ret===false)
                        $errors[]=lang("install_Query failed")." : ".$query;
                    $query='';
                }
            }
            $clubId=$this->Clubmodel->createClub($this->input->post('clubname'),
                                        $this->input->post('clubmatricule'));
            $this->Settingmodel->updateSetting('myClubId', $clubId);
            $this->Settingmodel->updateSetting('myClubName', $this->input->post('clubname'));
            $this->Settingmodel->updateSetting('adminEmail', $this->input->post('adminemail'));
            $this->Settingmodel->updateSetting('mail_no-reply', $this->input->post('mail_no-reply'));
            $this->Settingmodel->updateSetting('language', $this->input->post('language'));
            $this->Usermodel->updateUser(1, 'admin', $this->input->post('adminemail'), 1, $this->input->post('adminpass'));
            date_default_timezone_set('Europe/Berlin');
            $seasonId = $this->Settingmodel->createNewSeason(strftime("%Y"));
            $this->Settingmodel->updateSetting('seasonId', $seasonId);
            $this->session->set_userdata('validSetup', false);
            $data['errors']=$errors;
            $this->load->view('install/done', $data);
        }
    }

    function update_1_4_1_to_1_4_2($lang='english'){
        $this->load->library('session');
        $this->load->helper('url');
        $timecheck = $this->session->userdata('validSetup');
        $installType = $this->session->userdata('installType');
        if ($timecheck===false || (time()-$timecheck)>1800 || $installType!='update_1_4_1_to_1_4_2')
            header('Location:'.site_url('install/check/'.$lang));
        $this->load->helper('language');
        if($lang!='french') {
            $lang = 'english';
        }
        $this->config->set_item('language', $lang);
        $this->lang->load('install');
        $this->load->database();
        $this->load->model('Settingmodel');
        $this->Settingmodel->updateSetting('version_major', FCMS_VERSION_MAJOR);
        $this->Settingmodel->updateSetting('version_minor', FCMS_VERSION_MINOR);
        $this->Settingmodel->updateSetting('version_revision', FCMS_VERSION_REVISION);
        $this->session->set_userdata('validSetup', false);
        $data['errors']=array();
        $this->load->view('install/done', $data);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */

?>
