<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Ajax extends FCMS_Controller {

	function __construct(){
		parent::__construct();
        $this->setZone('ajax');
	}
    
    function Captcha($id=null){
        if(!is_null($id)){
            $this->load->library('CaptchaGenerator');
            $captcha = new CaptchaGenerator();
            $ret=$this->captchagenerator->createImage($id);
            
            // Add captcha in a PHP Session
            $captchas = $this->session->userdata('captchas');
            if(!is_array($captchas)) $captchas=array();
            $captchas[$id] = md5($ret['text']);
            $this->session->set_userdata('captchas', $captchas);
            header("Content-type: image/png");
            header("Cache-Control: no-cache, must-revalidate");
            imagepng($ret['img']);
        }
    }
    
}

?>
