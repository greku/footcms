<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Teams extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function show($teamId=null, $clubId=null){
        $this->load->model('Teammodel');
        $this->load->model('Playermodel');
        $team=$this->Teammodel->getTeam($teamId);
        if(empty($team) || (int)$team['clubId']!=(int)$clubId) {
            show_404();
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($team['clubName'], 'admin/clubs/show/'.$clubId);
        $this->appendBreadcrumb($team['name']);
        $data['team']=$team;
        $data['players']=$this->Playermodel->getPlayers($teamId);
        $out=$this->load->view('admin/team_detail', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($id=null,$clubId=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Teammodel');
        $this->load->model('Clubmodel');
        $this->load->model('Placemodel');
        if(is_null($id) || (int)$id==0){
            $team=$this->Teammodel->getTeamEmpty($id);
            $team['id']='0';
        }
        else {
            $team=$this->Teammodel->getTeam($id);
            if(empty($team) || (int)$team['clubId']!=(int)$clubId) {
                show_404();
            }
        }
        $club=$this->Clubmodel->getClub($clubId);
        if(empty($club)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($club['name'], 'admin/clubs/show/'.$clubId);
        if($team['id']==0){
            $this->appendBreadcrumb(lang('teams_Create_team'));
        }
        else {
            $this->appendBreadcrumb($team['name'], 'admin/teams/show/'.$id.'/'.$clubId);
            $this->appendBreadcrumb(lang('admin_Edit'));
        }
        $data['clubId']=$clubId;
        $data['places']=$this->Placemodel->getPlaces();
        $data['picture']=$this->Teammodel->getTeamPicture($id);
        if(empty($data['picture']))
            $data['picture']=array('id'=>0, 'name'=>'', 'path'=>'');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:teams_Name', 'trim|required|min_length[3]|max_length[255]|xss_clean');
        $this->form_validation->set_rules('contactName', 'lang:teams_Contact_name', 'trim');
        $this->form_validation->set_rules('contactPhone', 'lang:teams_Contact_phone', 'trim');
        $this->form_validation->set_rules('contactEmail', 'lang:teams_Contact_email', 'trim|valid_email');
        $this->form_validation->set_rules('placeId', 'lang:teams_Place', 'trim|required|callback__placeValid');
        $this->form_validation->set_rules('fileId', 'lang:teams_Picture', 'trim|required|callback__pictureValid');
        $this->form_validation->set_rules('filePath');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['team']=$team;
            $out=$this->load->view('admin/team_edit', $data, true);
        }
        else {
            if ((int)$team['id']==0) {
                $id=$this->Teammodel->createTeam($this->input->post('name'),
                    $this->input->post('contactName'),
                    $this->input->post('contactPhone'),
                    $this->input->post('contactEmail'),
                    $this->input->post('placeId'),
                    $clubId,
                    $this->config->fcms('seasonId'));
                $data['title']='teams_Create_team';
                $data['messages'][0]['text']='teams_Team_successfully_created';
            }
            else {
                $this->Teammodel->updateTeam($id, $this->input->post('name'),
                    $this->input->post('contactName'),
                    $this->input->post('contactPhone'),
                    $this->input->post('contactEmail'),
                    $this->input->post('placeId'));
                $data['title']='teams_Edit_team';
                $data['messages'][0]['text']='teams_Team_successfully_edited';
            }
            if ($data['picture']['id']!=$this->input->post('fileId')) {
                if ((int)$this->input->post('fileId')==0)
                    $this->Teammodel->removePicture($id);
                else if ((int)$this->input->post('fileId')!=0){
                    if((int)$data['picture']['id']==0)
                        $this->Teammodel->addPicture($id, $this->input->post('fileId'));
                    else
                        $this->Teammodel->updatePicture($id, $this->input->post('fileId'));
                }
            }
            $data['type']='success';
            if ((int)$team['id']==0) {
                $data['messages'][1]['href']='admin/teams/show/'.(int)$id.'/'.(int)$clubId;
                $data['messages'][1]['text']='teams_Add_players_to_the_new_team';
                $data['messages'][2]['text']='teams_Return_to_club';
                $data['messages'][2]['href']='admin/clubs/show/'.$clubId;
            }
            else {
                $data['messages'][1]['href']='admin/teams/show/'.(int)$id.'/'.(int)$clubId;
                $data['messages'][1]['text']='teams_Return_to_team';
            }
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _placeValid($id){
        $place = $this->Placemodel->getPlace($id);
        if((int)$id==0 || empty($place)){
            return null;
        }
        else {
            return $place['id'];
        }
    }

    function _pictureValid($id){
        $this->load->model('Filemodel');
        $file = $this->Filemodel->getFile($id);
        $typeAllowed=array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif');
        if(empty($file) || !in_array($file['mime'], $typeAllowed)){
            return null;
        }
        else {
            return $file['id'];
        }
    }

    function addplayer($teamId=null, $clubId=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Teammodel');
        $team=$this->Teammodel->getTeam($teamId);
        if(empty($team) || (int)$team['clubId']!=(int)$clubId) {
            show_404();
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($team['clubName'], 'admin/clubs/show/'.$clubId);
        $this->appendBreadcrumb($team['name'], 'admin/teams/show/'.$teamId.'/'.$clubId);
        $this->appendBreadcrumb(lang('teams_Add_existing_player'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('player', 'lang:teams_Player', 'trim|required|callback__playerNotInTeam['.$teamId.']');
        $this->form_validation->set_rules('number', 'lang:players_Shirt_number', 'trim|is_natural');
        if($this->form_validation->run()==false) {
            $data['teamId']=$teamId;
            $data['clubId']=$clubId;
            $data['players']=$this->Teammodel->getPlayersNotInTeam($teamId);
            $data['formToken']=$this->form_validation->formToken;
            $out=$this->load->view('admin/team_add_player', $data, true);
        }
        else {
            $this->Teammodel->addPlayer($teamId,
                $this->input->post('player'),
                ($this->input->post('number')==''?null:$this->input->post('number')));
            $data['title']='teams_Add_existing_player';
            $data['messages'][0]['text']='teams_Player_successfully_added';
            $data['type']='success';
            $data['messages'][1]['href']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
            $data['messages'][1]['text']='players_Return_to_team';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function removeplayer($playerId=null, $teamId=null, $clubId=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Teammodel');
        $this->load->model('Playermodel');
        $team=$this->Teammodel->getTeam($teamId);
        if(empty($team) || (int)$team['clubId']!=(int)$clubId) {
            show_404();
        }
        $players=$this->Playermodel->getPlayers($teamId);
        $inteam=false;
        foreach ($players as $player) {
            if ($player['id']==$playerId) {
                $inteam=true;
                break;
            }
        }
        if ($inteam===false)
            show_404();
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($team['clubName'], 'admin/clubs/show/'.$clubId);
        $this->appendBreadcrumb($team['name'], 'admin/teams/show/'.$teamId.'/'.$clubId);
        $this->appendBreadcrumb(lang('teams_Remove_player'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/teams/removeplayer/'.(int)$playerId.'/'.(int)$teamId.'/'.(int)$clubId;
            $data['formCancel']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
            $data['confirmButton']='admin_Remove';
            $data['title']='teams_Remove_player';
            $data['message']=sprintf(lang('teams_Remove_player_confirmation'), $player['lastname'].' '.$player['firstname'], $team['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Teammodel->removePlayer($teamId, $playerId);
            $data['title']='teams_Remove_player';
            $data['messages'][0]['text']='teams_Player_successfully_removed';
            $data['type']='success';
            $data['messages'][1]['text']='teams_Return_to_team';
            $data['messages'][1]['href']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function delete($teamId=null,$clubId=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Teammodel');
        $this->load->model('Playermodel');
        $this->load->model('Matchmodel');
        $team=$this->Teammodel->getTeam($teamId);
        if(empty($team) || (int)$team['clubId']!=(int)$clubId) {
            show_404();
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($team['clubName'], 'admin/clubs/show/'.$clubId);
        $this->appendBreadcrumb($team['name'], 'admin/teams/show/'.$teamId.'/'.$clubId);
        $this->appendBreadcrumb(lang('admin_Delete'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        $players=$this->Playermodel->getPlayers($teamId);
        $matches=$this->Matchmodel->getMatches(null, 0, 1, $teamId);
        if(!empty($matches)) {
            $data['title']='teams_Delete_team';
            $data['messages'][0]['text']='teams_Team_played_matches_and_cant_be_removed';
            $data['type']='error';
            $data['messages'][1]['text']='teams_Return_to_team';
            $data['messages'][1]['href']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
            $out=$this->load->view('admin/message', $data, true);
        }
        else if(!empty($players)) {
            $data['title']='teams_Delete_team';
            $data['messages'][0]['text']='teams_Team_contains_players_and_cant_be_removed';
            $data['type']='error';
            $data['messages'][1]['text']='teams_Return_to_team';
            $data['messages'][1]['href']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['title']='teams_Delete_team';
            $data['formAction']='admin/teams/delete/'.(int)$teamId.'/'.(int)$clubId;
            $data['formCancel']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
            $data['confirmButton']='admin_Delete';
            $data['message']=sprintf(lang('teams_Delete_team_confirmation'), $team['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Teammodel->deleteTeam($teamId);
            $data['title']='teams_Delete_team';
            $data['type']='success';
            $data['messages'][0]['text']='teams_Team_successfully_deleted';
            $data['messages'][1]['text']='teams_Return_to_club';
            $data['messages'][1]['href']='admin/clubs/show/'.(int)$clubId;
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _playerNotInTeam($id, $teamId){
        $players = $this->Teammodel->getPlayersNotInTeam($teamId);
        foreach ($players as $player) {
            if ($player['id']==$id) {
                return $id;
            }
        }
        return false;
    }
}

?>
