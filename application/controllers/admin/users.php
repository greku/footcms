<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Users extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
        $this->setPermissions('users');
    }

    function index(){
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('users_Users_and_groups'));
        $this->load->model('Usermodel');
        $data['users']=$this->Usermodel->getUsers();
        $data['groups']=$this->Usermodel->getGroups();
        $out=$this->load->view('admin/users_groups', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edituser($id=null){
        $this->load->model('Usermodel');
        if(is_null($id)){
            $user=array('id'=>'', 'username'=>'', 'email'=>'', 'groupId'=>'');
            $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
            $this->appendBreadcrumb(lang('users_Users_and_groups'), 'admin/users');
            $this->appendBreadcrumb(lang('users_Create_user'));
        }
        else {
            $user=$this->Usermodel->getUser($id);
            if(empty($user)) {
                show_404();
            }
            $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
            $this->appendBreadcrumb(lang('users_Users_and_groups'), 'admin/users');
            $this->appendBreadcrumb(lang('users_Edit_user'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'lang:users_Username', 'trim|required|strtolower|min_length[3]|max_length[14]|callback__usernameValid['.$id.']|xss_clean');
        $this->form_validation->set_rules('email', 'lang:users_Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('groupId', 'lang:users_Group', 'callback__groupValid['.$id.']');
        if(is_null($id)){
            $this->form_validation->set_rules('password', 'lang:users_Password', 'trim|required|min_length[6]|matches[repassword]');
            $this->form_validation->set_rules('repassword', 'lang:users_Re-enter_password', 'trim');
        }
        elseif($this->input->post('password')!=''){
            $this->form_validation->set_rules('password', 'lang:users_Password', 'trim|required|min_length[6]|matches[repassword]');
            $this->form_validation->set_rules('repassword', 'lang:users_Re-enter_password', 'trim');
        }
        else{
            $this->form_validation->set_rules('password', 'lang:users_Password', 'trim');
            $this->form_validation->set_rules('repassword', 'lang:users_Re-enter_password', 'trim');
        }
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['user']=$user;
            $data['groups']=$this->Usermodel->getGroups();
            $out=$this->load->view('admin/user_edit', $data, true);
        }
        else {
            if(is_null($id)) {
                $this->Usermodel->createUser($this->input->post('username'), $this->input->post('email'), $this->input->post('groupId'), $this->input->post('password'));
                $data['title']='users_Create_user';
                $data['messages'][0]['text']='users_User_successfully_created';
            }
            else {
                $this->Usermodel->updateUser($id, $this->input->post('username'),
                    $this->input->post('email'),
                    ($id!=1?$this->input->post('groupId'):1),
                    ($this->input->post('password')==''?null:$this->input->post('password')));
                $data['title']='users_Edit_user';
                $data['messages'][0]['text']='users_User_successfully_updated';
            }
            $data['type']='success';
            $data['messages'][1]['text']='users_Return_to_users_and_groups';
            $data['messages'][1]['href']='admin/users';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function editGroup($id=null){
        $this->load->model('Usermodel');
        if(is_null($id)){
            $group=array('id'=>'', 'name'=>'', 'description'=>'');
            $permissions=array();
            $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
            $this->appendBreadcrumb(lang('users_Users_and_groups'), 'admin/users');
            $this->appendBreadcrumb(lang('users_Create_group'));
        }
        else {
            $group=$this->Usermodel->getGroup($id);
            $temp=$this->Usermodel->getPermissions($id);
            $permissions=array();
            foreach ($temp as $item)
                $permissions[]=$item['id'];
            if(empty($group)) {
                show_404();
            }
            $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
            $this->appendBreadcrumb(lang('users_Users_and_groups'), 'admin/users');
            $this->appendBreadcrumb(lang('users_Edit_group'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:users_Name', 'trim|required|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('description', 'lang:users_Description', 'trim|required|max_length[254]|xss_clean');
        $this->form_validation->set_rules('permissions[]', 'lang:users_Permissions', '');
        if($this->form_validation->run()==false || !$this->_permissionsValid(isset($_POST['permissions'])?$_POST['permissions']:null)) {
            $data['formToken']=$this->form_validation->formToken;
            $data['group']=$group;
            $data['permissions']=$permissions;
            $data['allPermissions']=$this->Usermodel->getAllPermissions();
            if(!is_null($id)){
                $data['users']=array();
                $temp=$this->Usermodel->getUsers($id);
                foreach ($temp as $item){
                    $data['users'][]=$item['username'];
                }
            }
            $out=$this->load->view('admin/group_edit', $data, true);
        }
        else {
            if(is_null($id)) {
                $id = $this->Usermodel->createGroup($this->input->post('name'), $this->input->post('description'));
                if(isset($_POST['permissions']))
                    $this->Usermodel->addGroupPermissions($id, $this->input->post('permissions'));
                $data['title']='users_Create_group';
                $data['messages'][0]['text']='users_Group_successfully_created';
            }
            else {
                $this->Usermodel->updateGroup($id,
                    $this->input->post('name'),
                    $this->input->post('description'));
                $this->Usermodel->clearGroupPermissions($id);
                if(isset($_POST['permissions']))
                    $this->Usermodel->addGroupPermissions($id, $this->input->post('permissions'));
                $data['title']='users_Edit_group';
                $data['messages'][0]['text']='users_Group_successfully_updated';
            }
            $data['type']='success';
            $data['messages'][1]['text']='users_Return_to_users_and_groups';
            $data['messages'][1]['href']='admin/users';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function deleteGroup($id=null){
        $this->load->model('Usermodel');
        if(is_null($id)){
            show_404();
        }
        else {
            $group=$this->Usermodel->getGroup($id);
            if(empty($group) || $group['id']==1) {
                show_404();
            }
            $users=$this->Usermodel->getUsers($id);
            $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
            $this->appendBreadcrumb(lang('users_Users_and_groups'), 'admin/users');
            $this->appendBreadcrumb(lang('users_Delete_group'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if(!empty($users)){
            $userslist=array();
            foreach ($users as $user){
                $userslist[]=$user['username'];
            }
            $data['title']='users_Delete_group';
            $data['messages'][0]['text']=sprintf(lang('users_Group_contains_users_and_cant_be_removed'), $group['name'], implode($userslist, ', '));
            $data['type']='error';
            $data['messages'][1]['text']='users_Return_to_users_and_groups';
            $data['messages'][1]['href']='admin/users';
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/users/deletegroup/'.(int)$id;
            $data['formCancel']='admin/users';
            $data['confirmButton']='admin_Delete';
            $data['title']='users_Delete_group';
            $data['message']=sprintf(lang('users_Delete_group_confirmation'), $group['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Usermodel->clearGroupPermissions($id);
            $this->Usermodel->deleteGroup($id);
            $data['title']='users_Delete_group';
            $data['messages'][0]['text']='users_Group_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='users_Return_to_users_and_groups';
            $data['messages'][1]['href']='admin/users';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _usernameValid($username,$id){
        $this->form_validation->set_message('_usernameValid', lang('users_Username_not_available'));
        $id2=$this->Usermodel->checkUsername($username);
        if(is_null($id2))
            return true;
        elseif(!is_null($id2) && $id==$id2)
            return true;
        return false;
    }

    function deleteUser($id=null){
        $this->load->model('Usermodel');
        if(is_null($id)){
            show_404();
        }
        else {
            $user=$this->Usermodel->getUser($id);
            if(empty($user) || $user['id']==1) {
                show_404();
            }
            $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
            $this->appendBreadcrumb(lang('users_Users_and_groups'), 'admin/users');
            $this->appendBreadcrumb(lang('users_Delete_user'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/users/deleteUser/'.(int)$id;
            $data['formCancel']='admin/users';
            $data['confirmButton']='admin_Delete';
            $data['title']='users_Delete_user';
            $data['message']=sprintf(lang('users_Delete_user_confirmation'), $user['username']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Usermodel->deleteUser($id);
            $data['title']='users_Delete_user';
            $data['messages'][0]['text']='users_User_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='users_Return_to_users_and_groups';
            $data['messages'][1]['href']='admin/users';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _groupValid($groupId, $id){
        $group = $this->Usermodel->getGroup($groupId);
        if(!empty($group)){
            if($groupId==1 && $id!=1){
                return false;
            }
            return true;
        }
        return false;
    }

    function _permissionsValid($permissions){
        if(!is_null($permissions)){
            $allPermissions = $this->Usermodel->getAllPermissions();
            $perm = array();
            foreach ($allPermissions as $item) {
                $perm[]=$item['id'];
            }
            foreach ($permissions as $permission) {
                if(!in_array($permission, $perm))
                    return false;
            }
        }
        return true;
    }
}

?>
