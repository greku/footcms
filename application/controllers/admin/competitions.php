<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Competitions extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->appendBreadcrumb(lang('competitions_Competitions'));
        $this->load->model('Competitionmodel');
        $data['competitions'] = $this->Competitionmodel->getCompetitions($this->config->fcms('seasonId'));
        $out=$this->load->view('admin/competitions_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function show($competitionId=null){
        $this->load->model('Matchmodel');
        $this->load->model('Competitionmodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name']);
        $data['competition']=$competition;
        $data['teams']=$this->Competitionmodel->getTeams($competitionId);
        $data['matchesWithoutScore']=$this->Matchmodel->getMatchesWithoutScore($competitionId);
        $data['upcomingMatches']=$this->Matchmodel->getUpcomingMatches($competitionId);
        $out=$this->load->view('admin/competition_detail', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($competitionId=null){
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        if(is_null($competitionId))
            $competition=$this->Competitionmodel->getCompetitionEmpty();
        else {
            $competition=$this->Competitionmodel->getCompetition($competitionId);
            if(empty($competition)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('admin_Edit'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:competitions_Name', 'trim|required|min_length[3]|max_length[32]|xss_clean');
        if (is_null($competitionId)) {
            $this->form_validation->set_rules('type', 'lang:competitions_Type', 'trim|is_natural|callback__validType');
        }
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['competition']=$competition;
            $data['competitionTypes']=$this->Competitionmodel->getCompetitionTypes();
            $out=$this->load->view('admin/competition_edit', $data, true);
        }
        else {
            if (is_null($competitionId)) {
                $competitionId=$this->Competitionmodel->createCompetition($this->input->post('name'),
                    $this->input->post('type'),
                    $this->config->fcms('seasonId'));
                $data['title']='competitions_Add_competition';
                $data['messages'][0]['text']='competitions_Competition_successfully_added';
            }
            else {
                $this->Competitionmodel->updateCompetition($competitionId, $this->input->post('name'));
                $data['title']='competitions_Edit_competition';
                $data['messages'][0]['text']='competitions_Competition_successfully_edited';
            }
            $data['type']='success';
            $data['messages'][1]['text']='competitions_Edit_matches_and_teams_of_the_competition';
            $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            $data['messages'][2]['text']='competitions_Return_to_competitions';
            $data['messages'][2]['href']='admin/competitions';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function delete($competitionId=null){
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        if(is_null($competitionId)){
            show_404();
        }
        else {
            $competition=$this->Competitionmodel->getCompetition($competitionId);
            if(empty($competition)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('admin_Delete'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        $teams=$this->Competitionmodel->getTeams($competitionId);
        if(!empty($teams)) {
            $data['title']='competitions_Delete_competition';
            $data['messages'][0]['text']='competitions_Competition_contains_teams_and_cant_be_removed';
            $data['type']='error';
            $data['messages'][1]['text']='competitions_Return_to_competitions';
            $data['messages'][1]['href']='admin/competitions';
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/competitions/delete/'.(int)$competitionId;
            $data['formCancel']='admin/competitions';
            $data['confirmButton']='admin_Delete';
            $data['title']='competitions_Delete_competition';
            $data['message']=sprintf(lang('competitions_Delete_competition_confirmation'), $competition['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Competitionmodel->deleteCompetition($competitionId);
            $data['title']='competitions_Delete_competition';
            $data['messages'][0]['text']='competitions_Competition_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='competitions_Return_to_competitions';
            $data['messages'][1]['href']='admin/competitions';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function addteam($competitionId=null){
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('competitions_Add_team'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('team', 'lang:competitions_Team', 'trim|required|callback__teamNotInCompetition['.$competitionId.']');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['competition']=$competition;
            $data['teams']=$this->Competitionmodel->getTeamsNotInCompetition($competitionId, $this->config->fcms('seasonId'));
            $out=$this->load->view('admin/competition_addteam', $data, true);
        }
        else {
            $this->Competitionmodel->addTeam($competitionId,
                $this->input->post('team'),
                $this->input->post('team2'));
            $data['title']='competitions_Add_team';
            $data['messages'][0]['text']='competitions_Team_successfully_added';
            $data['type']='success';
            $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            $data['messages'][1]['text']='competitions_Return_to_competition';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function removeteam($competitionId=null,$teamId=null){
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $teams = $this->Competitionmodel->getTeams($competitionId);
        $found = false;
        foreach ($teams as $team) {
            if ($team['id']==$teamId) {
                $found = true;
                break;
            }
        }
        if ($found!==true) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('competitions_Remove_team'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        $this->load->model('Matchmodel');
        $matches=$this->Matchmodel->getMatches($competitionId, 0, 1, $teamId);
        if(!empty($matches)) {
            $data['title']='competitions_Remove_team_from_competition';
            $data['messages'][0]['text']='competitions_Team_played_matches_and_cant_be_removed';
            $data['type']='error';
            $data['messages'][1]['text']='competitions_Return_to_competition';
            $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['title']='competitions_Remove_team_from_competition';
            $data['formAction']='admin/competitions/removeteam/'.(int)$competitionId.'/'.(int)$teamId;
            $data['formCancel']='admin/competitions/show/'.(int)$competitionId;
            $data['confirmButton']='admin_Remove';
            $data['message']=sprintf(lang('competitions_Remove_team_confirmation'), $team['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Competitionmodel->removeTeam($competitionId, $teamId);
            $data['title']='competitions_Remove_team_from_competition';
            $data['type']='success';
            $data['messages'][0]['text']='competitions_Team_successfully_removed';
            $data['messages'][1]['text']='competitions_Return_to_competition';
            $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _teamNotInCompetition($id, $competitionId){
        $this->load->model('Competitionmodel');
        $this->load->model('Teammodel');
        $team=$this->Teammodel->getTeam($id);
        if (empty($team))
            return false;
        $teams=$this->Competitionmodel->getTeamsNotInCompetition($competitionId, $this->config->fcms('seasonId'));
        foreach ($teams as $team) {
            if ($team['id']==$id)
                return true;
        }
        return false;
    }

    function _validType($id){
        $this->load->model('Competitionmodel');
        $types=$this->Competitionmodel->getCompetitionTypes();
        foreach ($types as $type) {
            if ($type['id']==$id)
                return true;
        }
        return false;
    }
}

?>
