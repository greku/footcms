<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Reports extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->p();
    }

    function p($page=1) {
        $this->appendBreadcrumb(lang('reports_Match_reports'));
        $this->load->model('Reportmodel');
        $reportsPerPage=15;
        $this->load->library('pagination');
        /* count rows */
        $n=$this->Reportmodel->count($this->config->fcms('seasonId'));
        /* url for pagination */
        $this->pagination->setBaseUrl(site_url('admin/reports').'/p/');
        $firstOfPage=$this->pagination->setPage($n, (int)$page, $reportsPerPage);
        if($firstOfPage===false){
            show_404();
            exit();
        }
        $data['pagination']=$this->pagination->create_links();
        $data['reports']=$this->Reportmodel->getReports($firstOfPage, $reportsPerPage, $this->config->fcms('seasonId'));
        $out=$this->load->view('admin/reports_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function create(){
        $this->appendBreadcrumb(lang('reports_Match_reports'), 'admin/reports');
        $this->appendBreadcrumb(lang('reports_Create_report'));
        $this->setPermissions('reportedit');
        $this->load->model('Matchmodel');
        $data['matches']=$this->Matchmodel->getMatchesWithoutReport(null, $this->config->fcms('myClubId'), $this->config->fcms('seasonId'));
        $out=$this->load->view('admin/report_create', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($id=null){
        $this->setPermissions('reportedit');
        $this->load->model('Reportmodel');
        $report=$this->Reportmodel->getReport($id);
        if (empty($report)) {
            $this->load->model('Matchmodel');
            $match=$this->Matchmodel->getMatch($id);
            if (empty($match)) {
                show_404();
            }
            $report['id']=$id;
            $report['title']=sprintf(lang("reports_match_x_vs_y_date"), $match['teamName1'], $match['teamName2'], formatDate($match['date'], 'datenumeric'));
            $report['content']='';
            $report['newsId']='';
        }
        $this->appendBreadcrumb(lang('reports_Match_reports'), 'admin/reports');
        $this->appendBreadcrumb(lang('reports_Edit_report'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('content', 'lang:reports_Content', 'trim');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['report']=$report;
            $out=$this->load->view('admin/report_edit', $data, true);
        }
        else {
            if ($report['newsId']=='') {
                $authInfo = $this->session->userdata('authInfo');
                $this->Reportmodel->createReport($id, $report['title'],
                    $this->input->post('content'), $authInfo['userId']);
                $data['title']='reports_Create_report';
                $data['messages'][0]['text']='reports_Report_successfully_added';
            }
            else {
                $this->load->model('Newsmodel');
                $this->Newsmodel->updateNews($report['newsId'], $report['title'],
                    $this->input->post('content'));
                $data['title']='reports_Edit_report';
                $data['messages'][0]['text']='reports_Report_successfully_edited';
            }
            $data['type']='success';
            $data['messages'][1]['text']='reports_Return_to_reports';
            $data['messages'][1]['href']='admin/reports';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function delete($id=null){
        $this->setPermissions('reportedit');
        $this->load->model('Reportmodel');
        $report=$this->Reportmodel->getReport($id);
        if (empty($report)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('reports_Match_reports'), 'admin/reports');
        $this->appendBreadcrumb(lang('reports_Delete_report'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/reports/delete/'.(int)$id;
            $data['formCancel']='admin/reports';
            $data['confirmButton']='admin_Delete';
            $data['title']='reports_Delete_report';
            $data['message']=sprintf(lang('reports_Delete_report_confirmation'), $report['title']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->load->model('Commentmodel');
            $comments = $this->Commentmodel->listComments('news', $report['newsId'], null, null, 0);
            if(!empty($comments)){
                $ids=array();
                foreach ($comments as $comment) {
                    $ids[]=$comment['id'];
                }
                $this->Commentmodel->delete($ids);
            }
            $this->Reportmodel->deleteReport($id, $report['newsId']);
            $data['title']='reports_Delete_report';
            $data['messages'][0]['text']='reports_Report_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='reports_Return_to_reports';
            $data['messages'][1]['href']='admin/reports';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }
}

?>
