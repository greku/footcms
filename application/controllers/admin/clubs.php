<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Clubs extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->appendBreadcrumb(lang('clubs_Clubs'));
        $this->load->model('Clubmodel');
        $data['clubs']=$this->Clubmodel->getClubs();
        $data['myClubId']=$this->config->fcms('myClubId');
        $out=$this->load->view('admin/clubs_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function show($id=null){
        $this->load->model('Clubmodel');
        if(is_null($id))
            $id = $this->config->fcms('myClubId');
        $data['club']=$this->Clubmodel->getClub($id);
        if(empty($data['club'])) {
            show_404();
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($data['club']['name']);
        $this->load->model('Teammodel');
        $data['teams']=$this->Teammodel->getTeamsInfo($id, $this->config->fcms('seasonId'));
        $out=$this->load->view('admin/club_detail', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($id=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Clubmodel');
        if(is_null($id))
            $club=$this->Clubmodel->getClubEmpty($id);
        else {
            $club=$this->Clubmodel->getClub($id);
            if(empty($club)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($club['name'], 'admin/clubs/show/'.$club['id']);
        $this->appendBreadcrumb(lang('admin_Edit'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:clubs_Name', 'trim|required|min_length[3]|max_length[255]|xss_clean');
        $this->form_validation->set_rules('matricule', 'lang:clubs_Matricule', 'trim|max_length[10]|xss_clean|callback__freeMatricule['.(int)$id.']');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['club']=$club;
            $out=$this->load->view('admin/club_edit', $data, true);
        }
        else {
            if (is_null($id)) {
                $this->Clubmodel->createClub($this->input->post('name'),
                    $this->input->post('matricule'));
                $data['title']='clubs_Add_club';
                $data['messages'][0]['text']='clubs_Club_successfully_added';
            }
            else {
                $this->Clubmodel->updateClub($id, $this->input->post('name'),
                    $this->input->post('matricule'));
                if($id==$this->config->fcms('myClubId')){
                    $this->load->model('Settingmodel');
                    $this->Settingmodel->updateSetting('myClubName', $this->input->post('name'));
                }
                $data['title']='clubs_Edit_club';
                $data['messages'][0]['text']='clubs_Club_successfully_edited';
            }
            $data['type']='success';
            $data['messages'][1]['text']='clubs_Return_to_clubs';
            $data['messages'][1]['href']='admin/clubs';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }


    function delete($id=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Clubmodel');
        $this->load->model('Teammodel');
        if(is_null($id) || (int)$id==$this->config->fcms('myClubId')){
            show_404();
        }
        else {
            $club=$this->Clubmodel->getClub($id);
            if(empty($club)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
        $this->appendBreadcrumb($club['name'], 'admin/clubs/show/'.$club['id']);
        $this->appendBreadcrumb(lang('admin_Remove'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        $teams=$this->Teammodel->getTeams($id);
        if(!empty($teams)) {
            $data['title']='clubs_Remove_club';
            $data['messages'][0]['text']='clubs_Club_contains_teams_and_cant_be_removed';
            $data['type']='error';
            $data['messages'][1]['text']='clubs_Return_to_clubs';
            $data['messages'][1]['href']='admin/clubs';
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/clubs/delete/'.(int)$id;
            $data['formCancel']='admin/clubs';
            $data['confirmButton']='admin_Delete';
            $data['title']='clubs_Remove_club';
            $data['message']=sprintf(lang('clubs_Remove_club_confirmation'), $club['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Clubmodel->deleteClub($id);
            $data['title']='clubs_Remove_club';
            $data['messages'][0]['text']='clubs_Club_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='clubs_Return_to_clubs';
            $data['messages'][1]['href']='admin/clubs';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _freeMatricule($matricule, $clubId){
        $this->form_validation->set_message('_freeMatricule', lang('clubs_Matricule_is_already_used_by_another_club'));
        $club = $this->Clubmodel->getClubFromMatricule($matricule);
        if (empty($club))
            return true;
        elseif ($clubId==$club['id'])
            return true;
        else
            return false;
    }

}

?>
