<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Places extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->appendBreadcrumb(lang('places_Sport_halls'));
        $this->load->model('Placemodel');
        $data['places']=$this->Placemodel->getPlaces();
        $out=$this->load->view('admin/places_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($id=null){
        $this->setPermissions('placeedit');
        $this->load->model('Placemodel');
        if(is_null($id)){
            $place=$this->Placemodel->getPlaceEmpty($id);
            $this->appendBreadcrumb(lang('places_Sport_halls'), 'admin/places');
            $this->appendBreadcrumb(lang('places_Add_sport_hall'));
        }
        else {
            $place=$this->Placemodel->getPlace($id);
            if(empty($place)) {
                show_404();
            }
            $this->appendBreadcrumb(lang('places_Sport_halls'), 'admin/places');
            $this->appendBreadcrumb(lang('places_Edit_sport_hall'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:places_Name', 'trim|required|min_length[3]|max_length[255]|xss_clean');
        $this->form_validation->set_rules('shortname', 'lang:places_Short_name', 'trim|required|min_length[3]|max_length[20]|xss_clean');
        $this->form_validation->set_rules('street', 'lang:places_Street', 'trim|max_length[255]|xss_clean');
        $this->form_validation->set_rules('number', 'lang:places_Number', 'trim|max_length[5]|xss_clean');
        $this->form_validation->set_rules('postalcode', 'lang:places_Postal_code', 'trim|max_length[10]|xss_clean');
        $this->form_validation->set_rules('city', 'lang:places_City', 'trim|max_length[50]|xss_clean');
        $this->form_validation->set_rules('phone', 'lang:places_Phone', 'trim|max_length[32]|xss_clean');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['place']=$place;
            $out=$this->load->view('admin/place_edit', $data, true);
        }
        else {
            if (is_null($id)) {
                $this->Placemodel->createPlace($this->input->post('name'),
                    $this->input->post('shortname'), $this->input->post('street'),
                    $this->input->post('number'), $this->input->post('postalcode'),
                    $this->input->post('city'), $this->input->post('phone'));
                $data['title']='places_Add_sport_hall';
                $data['messages'][0]['text']='places_Sport_hall_successfully_added';
            }
            else {
                $this->Placemodel->updatePlace($id, $this->input->post('name'),
                    $this->input->post('shortname'), $this->input->post('street'),
                    $this->input->post('number'), $this->input->post('postalcode'),
                    $this->input->post('city'), $this->input->post('phone'));
                $data['title']='places_Edit_sport_hall';
                $data['messages'][0]['text']='places_Sport_hall_successfully_edited';
            }
            $data['type']='success';
            $data['messages'][1]['text']='places_Return_to_sport_halls';
            $data['messages'][1]['href']='admin/places';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }


    function delete($id=null){
        $this->setPermissions('placeedit');
        $this->load->model('Placemodel');
        if(is_null($id)){
            show_404();
        }
        else {
            $place=$this->Placemodel->getPlace($id);
            if(empty($place)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('places_Sport_halls'), 'admin/places');
        $this->appendBreadcrumb(lang('places_Remove_sport_hall'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/places/delete/'.(int)$id;
            $data['formCancel']='admin/places';
            $data['confirmButton']='admin_Delete';
            $data['title']='places_Remove_sport_hall';
            $data['message']=sprintf(lang('places_Remove_sport_hall_confirmation'), $place['name']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Placemodel->deletePlace($id);
            $data['title']='places_Remove_sport_hall';
            $data['messages'][0]['text']='places_Sport_hall_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='places_Return_to_places';
            $data['messages'][1]['href']='admin/places';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

}

?>
