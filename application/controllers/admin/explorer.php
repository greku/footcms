<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Explorer extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->folder();
    }

    function folder($folderId=null){
        $this->load->model('Filemodel');
        $folder = $this->Filemodel->getFolder($folderId);
        if (empty($folder)){
            $folder['id']=0;
            $folder['name']='root';
            $folder['path']="/";
            $this->appendBreadcrumb(lang('explorer_File_explorer'));
        }
        else {
            $this->appendBreadcrumb(lang('explorer_File_explorer'), 'admin/explorer');
            $this->appendBreadcrumb($folder['path']);
        }
        $data['folder']=$folder;
        $data['files'] = $this->Filemodel->getChildren($folder['id']);
        if($folder['id']!=0)
            array_unshift($data['files'],
                array('id'=>$folder['parent'], 'path'=>'/',
                'name'=>'..', 'date'=>'',
                'type'=>1)
                );
        $out=$this->load->view('admin/explorer_files', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function file($fileId=null){
        $this->load->model('Filemodel');
        $file=$this->Filemodel->getFile($fileId);
        if(empty($file)) {
            show_404();
        }
        if($file['mime']=='application/pdf' ||
            $file['mime']=='text/plain' ||
            $file['mime']=='image/jpeg' ||
            $file['mime']=='image/gif' ||
            $file['mime']=='image/png'||
            $file['mime']=='image/pjpeg'){
            header('Content-type: '.$file['mime']);
            header('Content-Disposition: inline; filename="'.$file['name'].'"');
            readfile(str_replace('system/','', BASEPATH).'upload'.$file['path']);
        }
        else {
            header('Content-Disposition: attachment; filename="'.$file['name'].'"');
            readfile(str_replace('system/','', BASEPATH).'upload'.$file['path']);
        }
    }

    function upload($folderId=null){
        $this->setPermissions('uploadfile');
        $this->load->model('Filemodel');
        if(is_null($folderId)||$folderId==0){
            $folderId=0;
            $folder['id']=0;
            $folder['path']='/';
            $folder['name']='';
        }
        else {
            $folder = $this->Filemodel->getFolder($folderId);
            if(empty($folder)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('explorer_File_explorer'), 'admin/explorer');
        $this->appendBreadcrumb($folder['path'], 'admin/explorer/folder/'.$folder['id']);
        $this->appendBreadcrumb(lang('explorer_Upload'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', '', 'required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['folder']=$folder;
            $out=$this->load->view('admin/explorer_upload', $data, true);
        }
        else {
            $nbfiles=0;
            if ($this->input->post('type')=='sendpicture'){
                if(!isset($_FILES["filename"])){
                    $errors[]="Unknown error";
                }
                else if ($_FILES["filename"]["error"] == UPLOAD_ERR_NO_FILE) {
                    $errors[]=lang("explorer_No_file_uploaded");
                }
                else if($_FILES["filename"]["error"] == UPLOAD_ERR_OK) {
                    if(isset($_POST['resizing']))
                        $resizing=(int)$_POST['resizing'];
                    else
                        $resizing='3';
                    if(isset($_POST['width']))
                        $width=(int)$_POST['width'];
                    else
                        $width='600';
                    if(isset($_POST['height']))
                        $height=(int)$_POST['height'];
                    else
                        $height='400';
                    $upfile = $_FILES["filename"]["name"];
                    $tmp_name=$_FILES["filename"]["tmp_name"];
                    $filetype=$_FILES["filename"]["type"];
                    //remove blanks and symbols from filename
                    $filename2 = strtr($upfile,"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ",
                     "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn_" );
                    //check file type allowed
                    $types = $this->Filemodel->getTypesAllowed();
                    $filetypeId=0;
                    foreach ($types as $type) {
                        if ($type['mime']==$filetype){
                            $filetypeId=$type['id'];
                            break;
                        }
                    }
                    if($filetypeId!=0){
                        //check the file doesn't already exist
                        $upload_path = str_replace('system/','', BASEPATH).'upload';
                        $destination =  $upload_path.$folder['path'].$filename2;
                        $destinationthumb = $upload_path.$folder['path'].'thumbs/'.$filename2;
                        if(file_exists($destination)){
                            $ext = substr($filename2, strrpos($filename2, '.'));
                            $name = substr($filename2, 0, strrpos($filename2, '.'));
                            for ($i=2;$i<100;$i++){
                                $destination =  $upload_path.$folder['path'].$name.'_'.$i.$ext;
                                $destinationthumb =  $upload_path.$folder['path'].'thumbs/'.$name.'_'.$i.$ext;
                                if (!file_exists($destination)){
                                    $filename2=$name.'_'.$i.$ext;
                                    break;
                                }
                            }
                            if ($i==100) {
                                $errors[]=lang("explorer_Filename_already_exists");
                            }
                        }
                        if (empty($errors)){
                            // resize
                            if($filetype=='image/jpeg' || $filetype=='image/pjpeg')
                                $im = imagecreatefromjpeg($tmp_name);
                            else if($filetype=='image/png'){
                                $im = imagecreatefrompng($tmp_name);
                                imagealphablending($im, false);
                                imagesavealpha($im,true);
                            }
                            else{
                                $errors[]= lang("Picture format not supported");
                            }
                            if (empty($errors)){
                                $transparency = $filetype=='image/jpg'?false:true;
                                if($resizing==3){
                                    $sizex = imagesx($im);
                                    $sizey = imagesy($im);
                                    if(($sizex > $width) || ($sizey > $height))
                                        $im2=$this->resizeImage($im, $transparency, $width, $height);
                                    else
                                        $im2 = $im;
                                }
                                else if($resizing==2){
                                    $sizex = imagesx($im);
                                    $sizey = imagesy($im);
                                    if(($sizex != $width) || ($sizey != $height))
                                        $im2=$this->resizeImage($im, $transparency, $width, $height, false);
                                    else
                                        $im2 = $im;
                                }
                                else{
                                    $im2 = $im;
                                }
                                //save the picture
                                if($filetype=='image/jpeg' || $filetype=='image/pjpeg')
                                    imagejpeg($im2,$destination);
                                else if($filetype=='image/png')
                                    imagepng($im2,$destination);
                                chmod($destination, 0664);
                                $this->Filemodel->addFile($filename2, $filetypeId, $folder['id'], $folder['path'].$filename2);
                                $nbfiles++;
                                //create thumb
                                $sizex = imagesx($im2);
                                $sizey = imagesy($im2);
                                $imthumb=$this->resizeImage($im2, $transparency, 75, 75);
                                imagedestroy($im2);
                                //create thumb folder
                                if(!file_exists($upload_path.$folder['path'].'thumbs'))
                                    if(@mkdir($upload_path.$folder['path'].'thumbs', 0775))
                                        chmod($upload_path.$folder['path'].'thumbs', 0775);
                                //save the thumb
                                if($filetype=='image/jpeg' || $filetype=='image/pjpeg')
                                    @imagejpeg($imthumb,$destinationthumb);
                                else if($filetype=='image/png')
                                    @imagepng($imthumb,$destinationthumb, 1);
                            }
                        }
                    }
                    else { echo $filetype;
                        $errors[]=lang("explorer_File_type_not_allowed");
                    }
                }
                else if($_FILES["filename"]["error"]==UPLOAD_ERR_INI_SIZE || $_FILES["filename"]["error"]==UPLOAD_ERR_FORM_SIZE)
                    $errors[]=getLang("File \"%filename%\" is too big",array("%filename%"=>$_FILES["filename"]["name"]));
                else
                    $errors[]=getLang("Error with file \"%filename%\"",array("%filename%"=>$_FILES["filename"]["name"]));

            }
            else {
                if(!isset($_FILES["filename"])){
                    $errors[]="Unknown error";
                }
                else if ($_FILES["filename"]["error"][0] == UPLOAD_ERR_NO_FILE) {
                    $errors[]=lang("explorer_No_file_uploaded");
                }
                else{
                    foreach($_FILES['filename']['name'] as $key=>$upfile){
                        if($_FILES["filename"]["error"][$key] == UPLOAD_ERR_OK) {
                            //remove blanks and symbols from filename
                            $filename2 = strtr($upfile,"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ",
                             "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn_" );
                            //check file type allowed
                            $types = $this->Filemodel->getTypesAllowed();
                            $filetype=0;
                            foreach ($types as $type) {
                                if ($type['mime']==$_FILES['filename']['type'][$key]){
                                    $filetype=$type['id'];
                                    break;
                                }
                            }
                            if($filetype==0) {
                                $errors[]=lang("explorer_File_type_not_allowed");
                                break;
                            }
                            //check the file doesn't already exist
                            $upload_path = str_replace('system/','', BASEPATH).'upload';
                            $destination =  $upload_path.$folder['path'].$filename2;
                            if(file_exists($destination)){
                                $ext = substr($filename2, strrpos($filename2, '.'));
                                $name = substr($filename2, 0, strrpos($filename2, '.'));
                                for ($i=2;$i<100;$i++){
                                    $destination =  $upload_path.$folder['path'].$name.'_'.$i.$ext;
                                    if (!file_exists($destination)){
                                        $filename2=$name.'_'.$i.$ext;
                                        break;
                                    }
                                }
                                if ($i==100) {
                                    $errors[]=lang("explorer_Filename_already_exists");
                                    break;
                                }
                            }
                            if(!move_uploaded_file($_FILES['filename']['tmp_name'][$key], $destination)){
                                $errors[]= lang("explorer_Error_while_moving_file_on_server");
                                break;
                            }
                            chmod($destination, 0664);
                            $this->Filemodel->addFile($filename2, $filetype, $folder['id'], $folder['path'].$filename2);
                            $nbfiles++;
                        }
                        else if($_FILES["filename"]["error"][$key] == UPLOAD_ERR_INI_SIZE || $_FILES["filename"]["error"][$key] == UPLOAD_ERR_FORM_SIZE)
                            $errors[]=sprintf(lang("explorer_File_x_is_too_big"),$upfile);
                        else
                            $errors[]=sprintf(lang("explorer_Error_with_file_x"),$upfile);
                    }
                }
            }
            $data['title']='explorer_Upload';
            if (!empty($errors)) {
                $data['messages'][0]['text']=implode($errors, '<br />');
                $data['type']='error';
            }
            else {
                $data['messages'][0]['text']=sprintf(lang('explorer_n_files_successfully_uploaded'), $nbfiles);
                $data['type']='success';
            }
            $data['messages'][1]['text']='explorer_Return_to_folder';
            $data['messages'][1]['href']='admin/explorer/folder/'.$folder['id'];
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function delete($id=null){
        $this->setPermissions('deletefile');
        $this->load->model('Filemodel');
        $file=$this->Filemodel->getFile($id);
        if(empty($file)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('explorer_File_explorer'), 'admin/explorer');//TODO add last folder or all to breadcrumb
        if($file['type']==1)
            $this->appendBreadcrumb(lang('explorer_Delete_folder'));
        else
            $this->appendBreadcrumb(lang('explorer_Delete_file'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if ($file['type']==1 && count($this->Filemodel->getChildren($file['id']))>0){
            $data['title']='explorer_Delete_folder';
            $data['messages'][0]['text']='explorer_Error_Folder_contains_files';
            $data['type']='error';
            $data['messages'][1]['text']='explorer_See_files_in_folder';
            $data['messages'][1]['href']='admin/explorer/folder/'.(int)$file['id'];
            $data['messages'][2]['text']='explorer_Return_to_folder';
            $data['messages'][2]['href']='admin/explorer/folder/'.(int)$file['parent'];
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/explorer/delete/'.(int)$id;
            $data['formCancel']='admin/explorer/folder/'.$file['parent'];
            $data['confirmButton']='admin_Delete';
            if($file['type']==1){
                $data['title']='explorer_Delete_folder';
                $data['message']=sprintf(lang('explorer_Delete_folder_confirmation'), $file['name']);
            }
            else {
                $data['title']='explorer_Delete_file';
                $data['message']=sprintf(lang('explorer_Delete_file_confirmation'), $file['name']);
            }
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            if($file['type']==1){
                $data['title']='explorer_Delete_folder';
                $data['messages'][0]['text']='explorer_Folder_successfully_deleted';
            }
            else {
                $data['title']='explorer_Delete_file';
                $data['messages'][0]['text']='explorer_File_successfully_deleted';
            }
            $this->Filemodel->deleteFile($file['id']);
            $data['type']='success';
            $data['messages'][1]['text']='explorer_Return_to_folder';
            $data['messages'][1]['href']='admin/explorer/folder/'.(int)$file['parent'];
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function newfolder($parent=null){
        $this->setPermissions('uploadfile');
        $this->load->model('Filemodel');
        $folder = $this->Filemodel->getFolder($parent);
        if (is_null($parent) || (int)$parent==0){
            $parent=0;
            $folder['id']=0;
            $folder['path']='/';
            $folder['name']='';
        }
        if (empty($folder)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('explorer_File_explorer'), 'admin/explorer');
        $this->appendBreadcrumb($folder['path'], 'admin/explorer/folder/'.$folder['id']);
        $this->appendBreadcrumb(lang('explorer_Create_folder'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('foldername', 'lang:explorer_Folder_name', 'trim|required|max_length[255]|xss_clean');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['folder']=$folder;
            $out=$this->load->view('admin/explorer_newfolder', $data, true);
        }
        else {
            $newfolderId=$this->Filemodel->createFolder($folder['id'],
                $this->input->post('foldername'),
                $folder['path'].$this->input->post('foldername').'/');
            $data['title']='explorer_Create_folder';
            $data['messages'][0]['text']='explorer_Folder_successfully_created';
            $data['type']='success';
            $data['messages'][1]['href']='admin/explorer/folder/'.(int)$newfolderId;
            $data['messages'][1]['text']='explorer_Browse_new_folder';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    private function resizeImage($originalImage, $transparency, $toWidth, $toHeight, $proportional=true)
    {
        $width=imagesx($originalImage);
        $height=imagesy($originalImage);
        //list($width, $height) = getimagesize($originalImage);
        if($proportional==true){
            $xscale=$width/$toWidth;
            $yscale=$height/$toHeight;

            if ($yscale>$xscale){
                $new_width = round($width * (1/$yscale));
                $new_height = round($height * (1/$yscale));
            }
            else {
                $new_width = round($width * (1/$xscale));
                $new_height = round($height * (1/$xscale));
            }
        }
        else{
            $new_width = $toWidth;
            $new_height = $toHeight;
        }

        $imageResized = imagecreatetruecolor($new_width, $new_height);
        if($transparency){
            imagealphablending($imageResized, false);
            imagesavealpha($imageResized,true);
            $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
            imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);
        }
        imagecopyresampled($imageResized, $originalImage, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        return $imageResized;
    }
}

?>
