<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Matches extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function p($competitionId=null, $page=1) {
        $this->load->model('Competitionmodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('matches_Matches'));
        $this->load->model('Matchmodel');
        $matchesPerPage=15;
        $this->load->library('pagination');
        /* count rows */
        $n=$this->Matchmodel->count($competitionId);
        /* url for pagination */
        $this->pagination->setBaseUrl(site_url('admin/matches').'/p/'.$competitionId.'/');
        $firstOfPage=$this->pagination->setPage($n, (int)$page, $matchesPerPage);
        if($firstOfPage===false){
            show_404();
            exit();
        }
        $data['pagination']=$this->pagination->create_links();
        $data['competition']=$competition;
        $data['matches']=$this->Matchmodel->getMatches($competitionId, $firstOfPage, $matchesPerPage);
        $out=$this->load->view('admin/matches_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function create($competitionId=null) {
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        $this->load->model('Matchmodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('matches_Add_match'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('team1', 'lang:matches_First_team', 'trim|required|callback__validTeam['.$competitionId.']');
        $this->form_validation->set_rules('team2', 'lang:matches_Second_team', 'trim|required|doesNotMatch[team1]|callback__validTeam['.$competitionId.']');
        $this->form_validation->set_rules('date', 'lang:matches_Date', 'trim|required|valid_date');
        $this->form_validation->set_rules('time', 'lang:matches_Time', 'trim|required|valid_time');
        $this->form_validation->set_message('doesNotMatch', lang('matches_Same_teams_not_allowed'));
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['competition']=$competition;
            $data['teams']=$this->Competitionmodel->getTeams($competitionId);
            $out=$this->load->view('admin/match_create', $data, true);
        }
        else {
            $this->Matchmodel->createMatch($competitionId,
                $this->input->post('team1'),
                $this->input->post('team2'),
                $this->input->post('date').' '.$this->input->post('time'));
            $data['title']='matches_Add_match';
            $data['messages'][0]['text']='matches_Match_successfully_added';
            $data['type']='success';
            $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            $data['messages'][1]['text']='matches_Return_to_competition';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function delete($matchId=null, $competitionId=null, $from=null){
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        $this->load->model('Matchmodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $match = $this->Matchmodel->getMatch($matchId);
        if (empty($match)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('matches_Delete_match'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/matches/delete/'.(int)$matchId.'/'.(int)$competitionId.(!is_null($from)?'/'.$from:'');
            if($from=='l'){
                $data['formCancel']='admin/matches/p/'.(int)$competitionId;
            }
            else {
                $data['formCancel']='admin/competitions/show/'.(int)$competitionId;
            }
            $data['confirmButton']='admin_Delete';
            $data['title']='matches_Delete_match';
            $data['message']=sprintf(lang('matches_Remove_match_confirmation'), $match['teamName1'], $match['teamName2'], formatDate($match['date'], 'datetimenumeric'));
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Matchmodel->deleteMatch($matchId);
            $data['title']='matches_Delete_match';
            $data['messages'][0]['text']='matches_Match_successfully_deleted';
            $data['type']='success';
            if($from=='l'){
                $data['messages'][1]['text']='matches_Return_to_matches';
                $data['messages'][1]['href']='admin/matches/p/'.(int)$competitionId;
            }
            else {
                $data['messages'][1]['text']='matches_Return_to_competition';
                $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            }
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _validTeam($teamId, $competitionId) {
        $this->load->model('Competitionmodel');
        $teams=$this->Competitionmodel->getTeams($competitionId);
        foreach ($teams as $team) {
            if ($team['id']==(int)$teamId)
                return $teamId;
        }
        return false;
    }

    function edit($matchId=null, $competitionId=null) {
        $this->setPermissions('matchcompetitionedit');
        $this->load->model('Competitionmodel');
        $this->load->model('Matchmodel');
        $this->load->model('Playermodel');
        $competition = $this->Competitionmodel->getCompetition($competitionId);
        if (empty($competition)) {
            show_404();
        }
        $match = $this->Matchmodel->getMatch($matchId);
        if (empty($match)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('competitions_Competitions'), 'admin/competitions');
        $this->appendBreadcrumb($competition['name'], 'admin/competitions/show/'.$competition['id']);
        $this->appendBreadcrumb(lang('matches_Edit_match'));
        $playersTeam1=$this->Playermodel->getPlayers($match['teamId1']);
        $playersTeam2=$this->Playermodel->getPlayers($match['teamId2']);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('date', 'lang:matches_Date', 'trim|required|valid_date');
        $this->form_validation->set_rules('time', 'lang:matches_Time', 'trim|required|valid_time');
        $this->form_validation->set_rules('score1', 'lang:matches_Score', 'trim|is_natural');
        $this->form_validation->set_rules('score2', 'lang:matches_Score', 'trim|is_natural');
        $this->form_validation->set_rules('forfeit', 'lang:matches_Forfeit', 'trim|is_natural');
        //$this->form_validation->set_rules('dummy', 'lang:dummyfield', 'trim|required');
        foreach ($playersTeam1 as $player) {
            $this->form_validation->set_rules('players1[]', 'lang:matches_Participating_players', 'trim|is_natural');
            $this->form_validation->set_rules('playernum1'.$player['id'], 'lang:matches_Player_number', 'trim|is_natural');
        }
        foreach ($playersTeam2 as $player) {
            $this->form_validation->set_rules('players2[]', 'lang:matches_Participating_players', 'trim|is_natural');
            $this->form_validation->set_rules('playernum2'.$player['id'], 'lang:matches_Player_number', 'trim|is_natural');
        }
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['match']=$match;
            $data['competition']=$competition;
            //players present
            $playersP=$this->Matchmodel->getMatchPlayers($matchId);
            $data['players1']=array();
            $data['players2']=array();
            foreach ($playersP as $player) {
                if($player['teamId']==$match['teamId1']) {
                    $data['players1'][]=$player['playerId'];
                    foreach ($playersTeam1 as &$p){
                        if ($player['playerId']==$p['id']){
                            $p['number']=$player['number'];
                        }
                    }
                }
                else if($player['teamId']==$match['teamId2']) {
                    $data['players2'][]=$player['playerId'];
                    foreach ($playersTeam2 as &$p){
                        if ($player['playerId']==$p['id']){
                            $p['number']=$player['number'];
                        }
                    }
                }
            }
            //players of teams
            $data['playersTeam1']=$playersTeam1;
            $data['playersTeam2']=$playersTeam2;
            $goals=$this->Matchmodel->getMatchGoals($matchId);
            foreach ($goals as $goal) {
                if($goal['teamId']==$match['teamId1']) {
                    $data['goals1'][]=$goal;
                }
                else if($goal['teamId']==$match['teamId2']) {
                    $data['goals2'][]=$goal;
                }
            }
            $cards=$this->Matchmodel->getMatchCards($matchId);
            $data['cards1']=array();
            $data['cards2']=array();
            foreach ($cards as $card) {
                if($card['teamId']==$match['teamId1']) {
                    $data['cards1'][]=$card;
                }
                else if($card['teamId']==$match['teamId2']) {
                    $data['cards2'][]=$card;
                }
            }
            $out=$this->load->view('admin/match_edit', $data, true);
        }
        else {
            $score1=$this->input->post('score1');
            $score2=$this->input->post('score2');
            $forfeit=$this->input->post('forfeit');
            $goals=array();
            $cards=array();
            $players=array();
            if($score1=='' || $score2==''){
                $score1 = null;
                $score2 = null;
                $forfeit = 0;
            } else {
                $t1=array();
                $t2=array();
                //participating players
                foreach ($playersTeam1 as $player){
                    if (is_array($this->input->post('players1')) && in_array($player['id'], $this->input->post('players1'))){
                        $num=($this->input->post('playernum1'.$player['id'])==''?null:(int)$this->input->post('playernum1'.$player['id']));
                        $players[]=array('player'=>$player['id'], 'match'=>$matchId, 'team'=>$match['teamId1'], 'number'=>$num);
                        $t1[]=$player['id'];
                    }
                }
                foreach ($playersTeam2 as $player){
                    if (is_array($this->input->post('players2')) && in_array($player['id'], $this->input->post('players2'))){
                        $num=($this->input->post('playernum2'.$player['id'])==''?null:(int)$this->input->post('playernum2'.$player['id']));
                        $players[]=array('player'=>$player['id'], 'match'=>$matchId, 'team'=>$match['teamId2'], 'number'=>$num);
                        $t2[]=$player['id'];
                    }
                }
                //goals
                for ($i=0; $i<$score1; $i++) {
                    if (isset($_POST['goaler1'.$i], $_POST['goalmin1'.$i]) && (int)$_POST['goaler1'.$i]!=0) {
                        $player=(int)$_POST['goaler1'.$i];
                        $minute=($_POST['goalmin1'.$i]==""?null:(int)$_POST['goalmin1'.$i]);
                        //check that player is in team participating
                        if(in_array($player, $t1))
                            $goals[]=array('player'=>$player, 'minute'=>$minute, 'match'=>$matchId, 'team'=>$match['teamId1']);
                    }
                }
                for ($i=0; $i<$score2; $i++) {
                    if (isset($_POST['goaler2'.$i], $_POST['goalmin2'.$i]) && (int)$_POST['goaler2'.$i]!=0) {
                        $player=(int)$_POST['goaler2'.$i];
                        $minute=($_POST['goalmin2'.$i]==""?null:(int)$_POST['goalmin2'.$i]);
                        //check that player is in team participating
                        if(in_array($player, $t2))
                            $goals[]=array('player'=>$player, 'minute'=>$minute, 'match'=>$matchId, 'team'=>$match['teamId2']);
                    }
                }
                for ($i=0; $i<20; $i++) {
                    if (isset($_POST['cardplayer2'.$i], $_POST['cardcolor2'.$i], $_POST['cardmin2'.$i]) && (int)$_POST['cardplayer2'.$i]!=0) {
                        $player=(int)$_POST['cardplayer2'.$i];
                        $color=($_POST['cardcolor2'.$i]=='red'?'red':'yellow');
                        $minute=($_POST['cardmin2'.$i]==""?null:(int)$_POST['cardmin2'.$i]);
                        //check that player is in team participating
                        if(in_array($player, $t2))
                            $cards[]=array('player'=>$player, 'color'=>$color, 'minute'=>$minute, 'match'=>$matchId, 'team'=>$match['teamId2']);
                    } else {
                        break;
                    }
                }
            }
            $this->Matchmodel->updateMatch($matchId,
                $this->input->post('date').' '.$this->input->post('time'),
                $score1,
                $score2,
                $forfeit);
            //delete participating players
            $this->Matchmodel->deleteMatchPlayers($matchId);
            //insert participating players
            if(!empty($players))
                $this->Matchmodel->insertMatchPlayers($matchId, $players);
            //delete goals
            $this->Matchmodel->deleteMatchGoals($matchId);
            //insert goals
            if(!empty($goals))
                $this->Matchmodel->insertMatchGoals($matchId, $goals);
            //delete cards
            $this->Matchmodel->deleteMatchCards($matchId);
            //insert cards
            if(!empty($cards))
                $this->Matchmodel->insertMatchCards($matchId, $cards);
            $data['title']='matches_Edit_match';
            $data['messages'][0]['text']='matches_Match_successfully_edited';
            $data['type']='success';
            $data['messages'][1]['href']='admin/competitions/show/'.(int)$competitionId;
            $data['messages'][1]['text']='matches_Return_to_competition';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }
}

?>
