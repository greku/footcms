<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Modules extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
        $this->setPermissions('siteconfig');
    }

    function index(){
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('config_Modules'));
        $this->load->model('Modulemodel');
        $modules=$this->Modulemodel->getModules();
        $modulesDirs=$this->Modulemodel->getModulesDirs();
        $names=array();
        $modulesNames=array();
        foreach ($modules as $module) {
            $modulesNames[$module['name']]=$module;
            if (!in_array($module['name'], $names))
                $names[]=$module['name'];
        }
        foreach ($modulesDirs as $dir) {
            if (!in_array($dir, $names))
                $names[]=$dir;
        }
        foreach ($names as $moduleName) {
            $installed = key_exists($moduleName, $modulesNames)==1;
            $dirPresent = in_array($moduleName, $modulesDirs)==1;
            if ($installed && $dirPresent) {
                //module is installed and files OK
                $xml = $this->Modulemodel->readXml($moduleName);
                if($xml===false){
                    $modulesNames[$moduleName]['status']='corrupted';
                    $modulesNames[$moduleName]['error']=lang('modules_Error_xml_file_not_found');
                }
                elseif ($xml->name!=$modulesNames[$moduleName]['name']){
                    $modulesNames[$moduleName]['status']='corrupted';
                    $modulesNames[$moduleName]['error']=lang('modules_Error_name');
                }
                elseif ($xml->version_major!=$modulesNames[$moduleName]['version_major']
                    || $xml->version_minor!=$modulesNames[$moduleName]['version_minor']
                    || $xml->version_revision!=$modulesNames[$moduleName]['version_revision']
                    ) {
                    $modulesNames[$moduleName]['status']='outdated';
                    $modulesNames[$moduleName]['error']=lang('modules_Error_versions');
                    if ($this->checkCompatibility($xml->compatibility->version_major,
                                                    $xml->compatibility->version_minor,
                                                    $xml->compatibility->version_revision)){
                        $modulesNames[$moduleName]['compatible']='1';
                    }
                    else {
                        $modulesNames[$moduleName]['compatible']='0';
                    }
                }
                else {
                    $modulesNames[$moduleName]['status']='installed';
                }
            }
            elseif (!$installed && $dirPresent) {
                //module there but not installed
                $xml = $this->Modulemodel->readXml($moduleName);
                if($xml===false)
                    unset($modulesNames[$moduleName]);
                else {
                    $modulesNames[$moduleName]['name']=$moduleName;
                    $modulesNames[$moduleName]['status']='notinstalled';
                    $modulesNames[$moduleName]['description']=$xml->description;
                    $modulesNames[$moduleName]['author']=$xml->author;
                    $modulesNames[$moduleName]['settings']=$xml->settings;
                    $modulesNames[$moduleName]['enable']=0;
                    $modulesNames[$moduleName]['version_major']=$xml->version_major;
                    $modulesNames[$moduleName]['version_minor']=$xml->version_minor;
                    $modulesNames[$moduleName]['version_revision']=$xml->version_revision;
                    if ($this->checkCompatibility($xml->compatibility->version_major,
                                                    $xml->compatibility->version_minor,
                                                    $xml->compatibility->version_revision)){
                        $modulesNames[$moduleName]['compatible']='1';
                    }
                    else {
                        $modulesNames[$moduleName]['compatible']='0';
                    }
                }
            }
            elseif ($installed && !$dirPresent) {
                //module installed but files removed -> corrupted
                //cannot be uninstalled, can disable but not enable
                $modulesNames[$moduleName]['status']='corrupted';
                $modulesNames[$moduleName]['error']=lang('modules_Error_xml_file_not_found');
            }
        }
        $data['modules']=$modulesNames;
        $out=$this->load->view('admin/modules_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function enable($id=null){
        $this->load->model('Modulemodel');
        $this->Modulemodel->updateModuleEnable($id, 1);
        header('Location:'.site_url('admin/modules/'));
        exit();
    }

    function disable($id=null){
        $this->load->model('Modulemodel');
        $this->Modulemodel->updateModuleEnable($id, 0);
        header('Location:'.site_url('admin/modules/'));
        exit();
    }

    private function checkCompatibility($major, $minor, $revision){
        $fcms_major = $this->config->fcms('version_major');
        $fcms_minor = $this->config->fcms('version_minor');
        $fcms_revision = $this->config->fcms('version_revision');
        $revplus='';
        if (preg_match('/(\d+)(\+?)/', $revision, $matches)){
            $revision = $matches[1];
            $revplus = $matches[2];
        }
        if ($major!=$fcms_major) {
            return false;
        }
        if ($minor!=$fcms_minor) {
            return false;
        }
        if ($revision==$fcms_revision) {
            return true;
        }
        else if($revplus=='+' && $revision<=$fcms_revision) {
            return true;
        }
        else if ($revision=='x') {
            return true;
        }
        else
            return false;
    }
}

?>
