<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Blocks extends FCMS_Controller {

	function __construct(){
		parent::__construct();
        $this->setZone('admin');
        $this->setPermissions('siteconfig');
	}

	function index(){
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('config_Home_page_blocks'));
        $this->load->model('Blockmodel');
        $data['blocks']=$this->Blockmodel->getBlocks(null);
        $out=$this->load->view('admin/blocks_list', $data, true);
		$this->setContent($out);
        $this->render();
	}

    function move($direction=null, $id=null){
        $this->load->model('Blockmodel');
        if(!is_null($direction) && ($direction=="up" || $direction=="down")){
            $firstItem = $this->Blockmodel->getBlock($id, 0);
            if(empty($firstItem))
                show_404();
            if($direction=="up")
                $secondItem=$this->Blockmodel->getPreviousBlock($firstItem['rank'], 0);
            elseif($direction=="down")
                $secondItem=$this->Blockmodel->getNextBlock($firstItem['rank'], 0);
            if(!empty($secondItem)){
                $this->Blockmodel->updateBlockRank($firstItem['id'], $secondItem['rank']);
                $this->Blockmodel->updateBlockRank($secondItem['id'], $firstItem['rank']);
            }
            header('Location:'.site_url('admin/blocks/'));
            exit();
        }
        show_404();
    }

    function enable($id=null){
        $this->load->model('Blockmodel');
        $this->Blockmodel->updateBlockShow($id, 1);
        header('Location:'.site_url('admin/blocks/'));
        exit();
    }

    function disable($id=null){
        $this->load->model('Blockmodel');
        $this->Blockmodel->updateBlockShow($id, 0);
        header('Location:'.site_url('admin/blocks/'));
        exit();
    }

    function settings($id=null){
        $this->load->model('Blockmodel');
        $block=$this->Blockmodel->getBlock($id);
        if(empty($block) || $block['params']=='') {
            show_404();
        }
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('config_Home_page_blocks'), 'admin/blocks');
        $this->appendBreadcrumb(lang('config_Settings').': '.$block['name']);
        $this->load->library('form_validation');
        if($block['shortname']=='latestnews'){
            $this->form_validation->set_rules('nbnews', 'lang:blocks_Number_of_news', 'trim|required|is_natural');
            $this->form_validation->set_rules('shorttext', 'lang:blocks_Show_first_lines', 'trim|is_natural');
        }
        elseif($block['shortname']=='nextmatch'){
            $this->form_validation->set_rules('nbmatch', 'lang:blocks_Number_of_matches', 'trim|required|is_natural');
        }
        else
            show_404();
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['block']=$block;
            $data['settings']=unserialize($block['params']);
            $out=$this->load->view('admin/block_settings_'.$block['shortname'], $data, true);
        }
        else {
            if($block['shortname']=='latestnews'){
                $settings=array(
                    'nbnews'=>(int)$this->input->post('nbnews'),
                    'shorttext'=>(int)$this->input->post('shorttext')
                    );
            }
            elseif($block['shortname']=='nextmatch'){
                $settings=array(
                    'nbmatch'=>(int)$this->input->post('nbmatch')
                    );
            }
            else
                show_404();
            $params=serialize($settings);
            $this->Blockmodel->updateBlockParams($id, $params);
            $data['title']='blocks_Settings';
            $data['messages'][0]['text']='blocks_Settings_successfully_edited';
            $data['type']='success';
            $data['messages'][1]['text']='blocks_Return_to_blocks';
            $data['messages'][1]['href']='admin/blocks';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }
}

?>
