<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Config extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
        $this->setPermissions('siteconfig');
    }

    function index(){
        $this->appendBreadcrumb(lang('admin_Configuration'));
        $out=$this->load->view('admin/config_home', array(), true);
        $this->setContent($out);
        $this->render();
    }

    function menu($action=null, $id=null){
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('config_Site_menu'));
        $this->load->model('Menumodel');
        if(!is_null($action) && ($action=="moveup" || $action=="movedown")){
            $firstItem = $this->Menumodel->getItem($id, 0);
            if(empty($firstItem))
                show_404();
            if($action=="moveup")
                $secondItem=$this->Menumodel->getPreviousItem($firstItem['rank'], 0);
            elseif($action=="movedown")
                $secondItem=$this->Menumodel->getNextItem($firstItem['rank'], 0);
            if(!empty($secondItem)){
                $this->Menumodel->updateItemRank($firstItem['id'], $secondItem['rank']);
                $this->Menumodel->updateItemRank($secondItem['id'], $firstItem['rank']);
            }
            header('Location:'.site_url('admin/config/menu/'));
            exit();
        }
        $data['menuItems'] = $this->Menumodel->getItems(0);
        $out=$this->load->view('admin/config_menu', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function general(){
        $this->load->model('Settingmodel');
        $settings=$this->Settingmodel->getSettings();
        if(empty($settings)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('config_General_settings'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('clubname', 'lang:config_Name_of_your_club', 'trim|required|min_length[3]|max_length[255]|xss_clean');
        $this->form_validation->set_rules('adminemail', 'lang:config_Administrator_email', 'trim|required|min_length[3]|max_length[255]|valid_email');
        $this->form_validation->set_rules('mail_no-reply', 'lang:config_No-reply_email', 'trim|required|min_length[3]|max_length[255]|valid_email');
        $this->form_validation->set_rules('language', 'lang:config_Language', 'trim|required|callback__validLanguage');
        $this->form_validation->set_rules('template', 'lang:config_Template', 'trim|required|callback__validTemplate');
        $this->form_validation->set_rules('commentvalidation', 'lang:config_Comments_are_invisible_until_validated', 'trim|is_natural');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            foreach ($settings as $setting) {
                $data['settings'][$setting['name']]=$setting['value'];
            }
            $data['languages']=$this->Settingmodel->getAvailableLanguages();
            $data['templates']=$this->Settingmodel->getAvailableTemplates();
            $out=$this->load->view('admin/config_settings_edit', $data, true);
        }
        else {
            $this->Settingmodel->updateSetting('myClubName', $this->input->post('clubname'));
            $this->load->model('Clubmodel');
            $this->Clubmodel->updateClub($this->config->fcms('myClubId'), $this->input->post('clubname'), null);
            $this->Settingmodel->updateSetting('adminEmail', $this->input->post('adminemail'));
            $this->Settingmodel->updateSetting('mail_no-reply', $this->input->post('mail_no-reply'));
            $this->Settingmodel->updateSetting('language', $this->input->post('language'));
            $this->Settingmodel->updateSetting('template', $this->input->post('template'));
            $this->Settingmodel->updateSetting('commentValidation', (int)$this->input->post('commentvalidation'));
            $data['title']='config_Settings';
            $data['messages'][0]['text']='config_Settings_successfully_edited';
            $data['type']='success';
            $data['messages'][1]['text']='config_Go_back_to_configuration';
            $data['messages'][1]['href']='admin/config';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function nextseason(){
        $this->load->model('Settingmodel');
        $settings=$this->Settingmodel->getSettings();
        if(empty($settings)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('admin_Configuration'), 'admin/config');
        $this->appendBreadcrumb(lang('config_Go_to_next_season'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', 'lang:config_Confirmation', 'trim|callback__validConf');
        $this->form_validation->set_rules('copyteams', 'lang:config_Copy_teams', 'trim|is_natural');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['currentSeason']=$this->Settingmodel->getSeason($this->config->fcms('seasonId'));
            $data['nextSeason']=$data['currentSeason']['year']+1;
            $out=$this->load->view('admin/config_next_season', $data, true);
        }
        else {
            $currentSeason = $this->Settingmodel->getSeason($this->config->fcms('seasonId'));
            $oldId=$this->config->fcms('seasonId');
            $newId=$this->Settingmodel->createNewSeason($currentSeason['year']+1);
            $this->Settingmodel->updateSetting('seasonId', $newId);
            if ($this->input->post('copyteams')==1){
                $this->load->model('Teammodel');
                $this->load->model('Playermodel');
                $teams = $this->Teammodel->getTeamsInfo($this->config->fcms('myClubId'), $oldId);
                foreach ($teams as $team){
                    $newTeamId = $this->Teammodel->createTeam($team['name'],
                        $team['contactName'],
                        $team['contactPhone'],
                        $team['contactEmail'],
                        $team['placeId'],
                        $this->config->fcms('myClubId'),
                        $newId);
                    $players = $this->Playermodel->getPlayers($team['id']);
                    foreach ($players as $player) {
                        $this->Teammodel->addPlayer($newTeamId, $player['id'], $player['number']);
                    }
                }
            }
            $data['title']='config_Go_to_next_season';
            $data['messages'][0]['text']='config_New_season_successfully_created';
            $data['type']='success';
            $data['messages'][1]['text']='config_Go_back_to_configuration';
            $data['messages'][1]['href']='admin/config';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function _validConf($val){
        $this->form_validation->set_message('_validConf', lang('config_You_must_check_confirmation'));
        if ($val==1)
            return true;
        else
            return false;
    }

    function _validLanguage($val){
        $this->load->model('Settingmodel');
        $languages=$this->Settingmodel->getAvailableLanguages();
        if(in_array($val, $languages))
            return true;
        return false;
    }

    function _validTemplate($val){
        $this->load->model('Settingmodel');
        $templates=$this->Settingmodel->getAvailableTemplates();
        if(in_array($val, $templates))
            return true;
        return false;
    }
}

?>
