<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Comments extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->p();
    }

    function p($page=1){
        $commentsPerPage=20;
        $this->load->library('pagination');
        /* count rows */
        $this->load->model('Commentmodel');
        $n=$this->Commentmodel->count();
        /* url for pagination */
        $this->pagination->setBaseUrl(site_url('admin/comments').'/p/');
        $firstOfPage=$this->pagination->setPage($n, (int)$page, $commentsPerPage);
        if($firstOfPage===false){
            show_404();
            exit();
        }
        $this->appendBreadcrumb(lang('comments_Comments'));
        $data['pagination']=$this->pagination->create_links();
        $data['comments'] = $this->Commentmodel->listComments(null, null, $firstOfPage, $commentsPerPage);
        $out=$this->load->view('admin/comments_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function validate($id=null){
        $this->setPermissions('commentedit');
        $this->load->model('Commentmodel');
        $comment=$this->Commentmodel->getComment($id);
        if(empty($comment)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('comments_Comments'), 'admin/comments');
        $this->appendBreadcrumb(lang('comments_Validate'));
        $this->Commentmodel->validate($id);
        $data['title']='comments_Validate_comment';
        $data['messages'][0]['text']='comments_Comment_successfully_validated';
        $data['type']='success';
        $data['messages'][1]['text']='comments_Return_to_comments';
        $data['messages'][1]['href']='admin/comments';
        $out=$this->load->view('admin/message', $data, true);
        $this->setContent($out);
        $this->render();
    }


    function delete($id=null){
        $this->setPermissions('commentedit');
        $this->load->model('Commentmodel');
        if(is_null($id)){
            show_404();
        }
        else {
            $comments=$this->Commentmodel->getComment($id);
            if(empty($comments)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('comments_Comments'), 'admin/comments');
        $this->appendBreadcrumb(lang('comments_Remove_comment'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/comments/delete/'.(int)$id;
            $data['formCancel']='admin/comments';
            $data['confirmButton']='admin_Delete';
            $data['title']='comments_Remove_comment';
            $data['message']=sprintf(lang('comments_Remove_comment_confirmation'));
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Commentmodel->delete($id);
            $data['title']='comments_Remove_comment';
            $data['messages'][0]['text']='comments_Comment_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='comments_Return_to_comments';
            $data['messages'][1]['href']='admin/comments';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

}

?>
