<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Players extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->appendBreadcrumb(lang('players_Players'));
        $this->load->model('Playermodel');
        $data['players']=$this->Playermodel->getPlayers();
        $out=$this->load->view('admin/players_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($id=null, $teamId=null, $clubId=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Playermodel');
        $this->load->model('Teammodel');
        if(!is_null($teamId)){
            $this->load->model('Teammodel');
            $team=$this->Teammodel->getTeam($teamId);
            if(empty($team) || $team['clubId']!=(int)$clubId){
                show_404();
            }
        }
        if (is_null($clubId) || $clubId==0) {
            $this->appendBreadcrumb(lang('players_Players'), 'admin/players');
        }
        else {
            $this->appendBreadcrumb(lang('clubs_Clubs'), 'admin/clubs');
            $this->appendBreadcrumb($team['clubName'], 'admin/clubs/show/'.$clubId);
            $this->appendBreadcrumb($team['name'], 'admin/teams/show/'.$teamId.'/'.$clubId);
        }
        if(is_null($id)||(int)$id==0){
            $player=$this->Playermodel->getPlayerEmpty();
            $this->appendBreadcrumb(lang('players_Add_player'));
            if(!is_null($teamId))
                $player['number']='';
        }
        else {
            $player=$this->Playermodel->getPlayer($id);
            if(empty($player)) {
                show_404();
            }
            else if(!is_null($teamId)){
                $playernumber=$this->Teammodel->getPlayerNumber($teamId, $id);
                if(empty($playernumber))
                    show_404();
                else
                    $player['number']=$playernumber['number'];
            }
            $this->appendBreadcrumb(lang('players_Edit_player'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lastname', 'lang:players_Last_name', 'trim|required|min_length[3]|max_length[32]|xss_clean');
        $this->form_validation->set_rules('firstname', 'lang:players_First_name', 'trim|required|max_length[32]|xss_clean');
        $this->form_validation->set_rules('email', 'lang:players_Email', 'trim|max_length[255]|xss_clean|valid_email');
        $this->form_validation->set_rules('birthday', 'lang:players_Date_of_birth', 'trim|valid_date');
        $this->form_validation->set_rules('city', 'lang:players_City', 'trim|max_length[127]|xss_clean');
        if(!is_null($teamId)) {
            $this->form_validation->set_rules('number', 'lang:players_Shirt_number', 'trim|is_natural');
        }
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['player']=$player;
            $data['teamId']=$teamId;
            $data['clubId']=$clubId;
            $out=$this->load->view('admin/player_edit', $data, true);
        }
        else {
            if (is_null($id)||$id==0) {
                $playerId=$this->Playermodel->createPlayer($this->input->post('lastname'),
                    $this->input->post('firstname'),
                    $this->input->post('email'),
                    $this->input->post('birthday'),
                    $this->input->post('city'));
                if(!is_null($teamId)) {
                    $this->Teammodel->addPlayer($teamId,
                        $playerId,
                        ($this->input->post('number')==''?null:$this->input->post('number')));
                }
                $data['title']='players_Add_player';
                $data['messages'][0]['text']='players_Player_successfully_added';
            }
            else {
                $this->Playermodel->updatePlayer($id, $this->input->post('lastname'),
                    $this->input->post('firstname'),
                    $this->input->post('email'),
                    $this->input->post('birthday'),
                    $this->input->post('city'));
                if(!is_null($teamId)) {
                    $this->Teammodel->updatePlayerNumber($teamId,
                        $id,
                        ($this->input->post('number')==''?null:$this->input->post('number')));
                }
                $data['title']='players_Edit_player';
                $data['messages'][0]['text']='players_Player_successfully_edited';
            }
            $data['type']='success';
            if(!is_null($teamId)) {
                $data['messages'][1]['href']='admin/teams/show/'.(int)$teamId.'/'.(int)$clubId;
                $data['messages'][1]['text']='players_Return_to_team';
            }
            else{
                $data['messages'][1]['href']='admin/players';
                $data['messages'][1]['text']='players_Return_to_players';
            }
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

    function show($id=null, $teamId=null, $clubId=null){
        $this->load->model('Playermodel');
        $player=$this->Playermodel->getPlayer($id);
        if (empty($player)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('players_Players'), 'admin/players');
        $this->appendBreadcrumb($player['lastname'].' '.$player['firstname']);
        $data['player']=$player;
        $data['teams']=$this->Playermodel->getTeamStats($id);
        // Season stats
        $data['matches']=$this->Playermodel->getMatchCount($id, $this->config->fcms('seasonId'));
        $data['goals']=$this->Playermodel->getGoalCount($id, $this->config->fcms('seasonId'));
        $data['cards']=$this->Playermodel->getCardCount($id, $this->config->fcms('seasonId'));
        $out=$this->load->view('admin/player_detail', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function delete($id=null){
        $this->setPermissions('teamplayeredit');
        $this->load->model('Playermodel');
        $this->load->model('Teammodel');
        $player=$this->Playermodel->getPlayer($id);
        if(empty($player)) {
            show_404();
        }
        $this->appendBreadcrumb(lang('players_Players'), 'admin/players');
        $this->appendBreadcrumb($player['lastname'].' '.$player['firstname'], 'admin/players/show/'.(int)$player['id']);
        $this->appendBreadcrumb(lang('admin_Delete'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        $teams=$this->Playermodel->getTeamStats($id);
        if(!empty($teams)) {
            $data['title']='players_Delete_player';
            $data['messages'][0]['text']='players_Player_is_member_of_some_teams';
            $data['type']='error';
            $data['messages'][1]['text']='players_Return_to_players';
            $data['messages'][1]['href']='admin/players';
            $out=$this->load->view('admin/message', $data, true);
        }
        else if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/players/delete/'.(int)$id;
            $data['formCancel']='admin/players';
            $data['confirmButton']='admin_Delete';
            $data['title']='players_Delete_player';
            $data['message']=sprintf(lang('players_Delete_player_confirmation'), $player['lastname'].' '.$player['firstname']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->Playermodel->deletePlayer($id);
            $data['title']='players_Delete_player';
            $data['messages'][0]['text']='players_Player_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='players_Return_to_players';
            $data['messages'][1]['href']='admin/players';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

}

?>
