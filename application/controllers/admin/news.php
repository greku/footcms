<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class News extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin');
    }

    function index(){
        $this->p();
    }

    function p($page=1){
        $this->appendBreadcrumb(lang('news_News'));
        $newsPerPage=20;
        $this->load->library('pagination');
        /* count rows */
        $this->load->model('Newsmodel');
        $n=$this->Newsmodel->count();
        /* url for pagination */
        $this->pagination->setBaseUrl(site_url('admin/news').'/p/');
        $firstOfPage=$this->pagination->setPage($n, (int)$page, $newsPerPage);
        if($firstOfPage===false){
            show_404();
            exit();
        }
        $data['pagination']=$this->pagination->create_links();
        $data['news'] = $this->Newsmodel->getList($firstOfPage, $newsPerPage, true, true);
        $out=$this->load->view('admin/news_list', $data, true);
        $this->setContent($out);
        $this->render();
    }

    function edit($id=null){
        $this->appendBreadcrumb(lang('news_News'), 'admin/news');
        $this->setPermissions('newsedit');
        $this->load->model('Newsmodel');
        if(is_null($id)) {
            $this->appendBreadcrumb(lang('news_Add_news'));
            $news=$this->Newsmodel->getNewsEmpty($id);
        }
        else {
            $news=$this->Newsmodel->get($id);
            if(empty($news)) {
                show_404();
            }
            $this->appendBreadcrumb(lang('news_Edit_news').': '.$news['title']);
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'lang:news_Title', 'trim|required|max_length[255]|xss_clean');//TBC lengths
        $this->form_validation->set_rules('content', 'lang:news_Content', 'trim');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['news']=$news;
            $out=$this->load->view('admin/news_edit', $data, true);
        }
        else {
            if (is_null($id)) {
                $authInfo = $this->session->userdata('authInfo');
                $this->Newsmodel->createNews($this->input->post('title'),
                    $this->input->post('content'), $authInfo['userId']);
                $data['title']='news_Add_news';
                $data['messages'][0]['text']='news_News_successfully_added';
            }
            else {
                $this->Newsmodel->updateNews($id, $this->input->post('title'),
                    $this->input->post('content'));
                $data['title']='news_Edit_news';
                $data['messages'][0]['text']='news_News_successfully_edited';
            }
            $data['type']='success';
            $data['messages'][1]['text']='news_Return_to_news';
            $data['messages'][1]['href']='admin/news';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }


    function delete($id=null){
        $this->setPermissions('newsedit');
        $this->load->model('Newsmodel');
        if(is_null($id)){
            show_404();
        }
        else {
            $news=$this->Newsmodel->getBasic($id);
            if(empty($news)) {
                show_404();
            }
        }
        $this->appendBreadcrumb(lang('news_News'), 'admin/news');
        $this->appendBreadcrumb(lang('news_Remove_news'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirmation', '', 'trim|required');
        if($this->form_validation->run()==false) {
            $data['formToken']=$this->form_validation->formToken;
            $data['formAction']='admin/news/delete/'.(int)$id;
            $data['formCancel']='admin/news';
            $data['confirmButton']='admin_Delete';
            $data['title']='news_Remove_news';
            $data['message']=sprintf(lang('news_Remove_news_confirmation'), $news['title']);
            $out=$this->load->view('admin/confirmation', $data, true);
        }
        else {
            $this->load->model('Commentmodel');
            $comments = $this->Commentmodel->listComments('news', $id, null, null, 0);
            if(!empty($comments)){
                $ids=array();
                foreach ($comments as $comment) {
                    $ids[]=$comment['id'];
                }
                $this->Commentmodel->delete($ids);
            }
            $this->Newsmodel->deleteNews($id);
            $data['title']='news_Remove_news';
            $data['messages'][0]['text']='news_News_successfully_deleted';
            $data['type']='success';
            $data['messages'][1]['text']='news_Return_to_news';
            $data['messages'][1]['href']='admin/news';
            $out=$this->load->view('admin/message', $data, true);
        }
        $this->setContent($out);
        $this->render();
    }

}

?>
