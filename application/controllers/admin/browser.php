<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Browser extends FCMS_Controller {

    function __construct(){
        parent::__construct();
        $this->setZone('admin_frame');
    }

    function index(){
        $this->folder();
    }

    function file($folderId=null){
        $this->load->model('Filemodel');
        $folder = $this->Filemodel->getFolder($folderId);
        if (empty($folder)){
            $folder['id']=0;
            $folder['name']='root';
            $folder['path']="/";
            $this->appendBreadcrumb(lang('explorer_File_explorer'));
        }
        else {
            $this->appendBreadcrumb(lang('explorer_File_explorer'), 'admin/explorer');
            $this->appendBreadcrumb($folder['path']);
        }
        $data['folder']=$folder;
        $data['files'] = $this->Filemodel->getChildren($folder['id']);
        if($folder['id']!=0)
            array_unshift($data['files'],
                array('id'=>$folder['parent'], 'path'=>'/',
                'name'=>'..', 'date'=>'',
                'type'=>1)
                );
        $out=$this->load->view('admin/explorer_select', $data, true);
        $this->setContent($out);
        $this->render();
    }


    function picture($folderId=null){
        $this->load->model('Filemodel');
        $folder = $this->Filemodel->getFolder($folderId);
        if (empty($folder)){
            $folder['id']=0;
            $folder['name']='root';
            $folder['path']="/";
            $this->appendBreadcrumb(lang('explorer_File_explorer'));
        }
        else {
            $this->appendBreadcrumb(lang('explorer_File_explorer'), 'admin/explorer');
            $this->appendBreadcrumb($folder['path']);
        }
        $data['browserType']='picture';
        $data['folder']=$folder;
        $data['files'] = $this->Filemodel->getChildren($folder['id'], 'picture');
        if($folder['id']!=0)
            array_unshift($data['files'],
                array('id'=>$folder['parent'], 'path'=>'/',
                'name'=>'..', 'date'=>'',
                'type'=>1)
                );
        $out=$this->load->view('admin/explorer_select', $data, true);
        $this->setContent($out);
        $this->render();
    }

}

?>
