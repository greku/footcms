<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Session extends FCMS_Controller {

	function __construct(){
		parent::__construct();
        $this->setZone('site_frame');
	}
    
    function login(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'lang:login_Username', 'trim|required|min_length[3]|max_length[14]|xss_clean');
        $this->form_validation->set_rules('password', 'lang:login_Password', 'required');

        if($this->authentication->isAuthenticated()==true){
            $data['already']=true;
            if($this->authentication->hasPermission('viewadmin'))
                $data['admin']=true;
            $this->setContent($this->load->view('login_connected',$data, true));
        }
        else if($this->form_validation->run()==false) {
            $data['autherror']=$this->authentication->getAuthError();
            $data['formToken']=$this->form_validation->formToken;
            $this->setContent($this->load->view('login',$data, true));
        }
        else if($this->authentication->login($this->input->post('username'), $this->input->post('password'))!==true) {
            $data['autherror']=$this->authentication->getAuthError();
            $data['formToken']=$this->form_validation->formToken;
            $this->setContent($this->load->view('login',$data, true));
        }
        else {
            $data['success']=true;
            if($this->authentication->hasPermission('viewadmin'))
                $data['admin']=true;
            $this->setContent($this->load->view('login_connected',$data, true));
        }
        $this->render();
    }
    
    function logout(){
        if($this->authentication->isAuthenticated()){
            $this->authentication->logout();
            $data['success']=true;
        }
        else
            $data['notLogged']=true;
        $this->setContent($this->load->view('login_disconnected',$data, true));
        $this->render();
    }
}

?>
