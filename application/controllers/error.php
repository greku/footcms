<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Error extends FCMS_Controller {

	function __construct(){
		parent::__construct();
	}

    function index(){
        $this->show_404();
    }
    
	function show_error($heading='', $message='', $template=''){
        $data['heading']=$heading;
        $data['message']=$message;
        $errorPage=$this->load->view($template, $data, true);
		$this->setContent($errorPage);
        $this->render();
	}
    
    function show_404(){
        $this->show_error('404 Page Not Found', 'The page you requested was not found.', 'error_404');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
