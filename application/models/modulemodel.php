<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Modulemodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function isModuleEnabled($name){
        $request="SELECT 1"."\n"
            ."FROM `".$this->db->dbprefix."modules` AS `modules`"."\n"
            ."WHERE `modules`.`name`='".mysql_real_escape_string($name)."'"."\n"
            ."AND `modules`.`enable`=1";
        $query = $this->db->query($request);
        $module = $query->result_array();
        return (empty($module)?false:true);
    }

    /*
     * Get modules
     */
    function getModules(){
        $request="SELECT `modules`.`id`, `modules`.`name`, `modules`.`description`, `modules`.`author`, "."\n"
            ."`modules`.`version_major`, `modules`.`version_minor`, "."\n"
            ."`modules`.`version_revision`, `modules`.`url`, "."\n"
            ."`modules`.`settings`, `modules`.`enable`"."\n"
            ."FROM `".$this->db->dbprefix."modules` AS `modules`"."\n"
            ."ORDER BY `modules`.`name` ASC";
        $query = $this->db->query($request);
        $modules = $query->result_array();
        return $modules;
    }

    /*
     * Get modules
     */
    function getModule($name){
        $request="SELECT `modules`.`id`, `modules`.`name`, `modules`.`description`, `modules`.`author`, "."\n"
            ."`modules`.`version_major`, `modules`.`version_minor`, "."\n"
            ."`modules`.`version_revision`, `modules`.`url`, "."\n"
            ."`modules`.`settings`, `modules`.`enable`"."\n"
            ."FROM `".$this->db->dbprefix."modules` AS `modules`"."\n"
            ."WHERE `modules`.`name`='".mysql_real_escape_string($name)."'"."\n";
        $query = $this->db->query($request);
        $module = $query->result_array();
        return (empty($module)?array():$module[0]);
    }

    /*
     * Module installed
     */
    function isModuleInstalled($name){
        $request="SELECT 1"."\n"
            ."FROM `".$this->db->dbprefix."modules` AS `modules`"."\n"
            ."WHERE `modules`.`name`='".mysql_real_escape_string($name)."'"."\n";
        $query = $this->db->query($request);
        $module = $query->result_array();
        return (empty($module)?false:true);
    }

    /*
     * Add a module
     */
    function addModule($name, $description, $author, $version_major, $version_minor, $version_revision, $url, $settings, $enable=0){
        $request="INSERT INTO `".$this->db->dbprefix."modules` (`id`, `name`, `description`, `author`, "
            ."`version_major`, `version_minor`, `version_revision`, `url`, `settings`,`enable`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($name)."', "
            ."'".mysql_real_escape_string($description)."', "
            ."'".mysql_real_escape_string($author)."', "
            ."'".mysql_real_escape_string($version_major)."', "
            ."'".mysql_real_escape_string($version_minor)."', "
            ."'".mysql_real_escape_string($version_revision)."', "
            ."'".mysql_real_escape_string($url)."', "
            .(int)$settings.", "
            .(int)$enable." "
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Update module info
     */
    function updateModule($name, $description, $author, $version_major, $version_minor, $version_revision, $url){
        $request="UPDATE `".$this->db->dbprefix."modules` SET "."\n"
            ."`name` = '".mysql_real_escape_string($name)."', "."\n"
            ."`description` = '".mysql_real_escape_string($description)."', "."\n"
            ."`author` = '".mysql_real_escape_string($author)."', "."\n"
            ."`version_major` = '".mysql_real_escape_string($version_major)."', "."\n"
            ."`version_minor` = '".mysql_real_escape_string($version_minor)."', "."\n"
            ."`version_revision` = '".mysql_real_escape_string($version_revision)."', "."\n"
            ."`url` = '".mysql_real_escape_string($url)."' "."\n"
            ."WHERE `name` ='".mysql_real_escape_string($name)."' LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get modules dirs
     */
    function getModulesDirs(){
        $modules=array();
        if ($handle = opendir(APPPATH.'modules/')) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($file = readdir($handle))) {
                if ($file!='.' && $file!='..' && is_dir(APPPATH.'modules/'.$file)){
                    $modules[]=$file;
                }
            }
            closedir($handle);
        }
        return $modules;
    }

    /*
     * Read xml file
     */
    function readXml($name){
        if (!file_exists(APPPATH.'modules/'.$name.'/'.$name.'.xml')){
            return false;
        }
        $xml = simplexml_load_file(APPPATH.'modules/'.$name.'/'.$name.'.xml');
        return $xml;
    }

    /*
     * Update module enable
     */
    function updateModuleEnable($id, $enable){
        $request="UPDATE `".$this->db->dbprefix."modules` SET "."\n"
            ."`enable` = ".(int)$enable."\n"
            ."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete module
     */
    function deleteModule($name) {
        // get module
        $request="SELECT `id`"."\n"
            ."FROM `".$this->db->dbprefix."modules` AS `modules`"."\n"
            ."WHERE `modules`.`name`='".mysql_real_escape_string($name)."'";
        $query = $this->db->query($request);
        $module = $query->result_array();
        if(empty($module))
            return false;
        $id=$module[0]['id'];
        //blocks
        $request="DELETE FROM `".$this->db->dbprefix."blocks` "."\n"
            ."WHERE `moduleId`=".(int)$id;
        $query = $this->db->query($request);
        //menu
        $request="DELETE FROM `".$this->db->dbprefix."menu` "."\n"
            ."WHERE `moduleId`=".(int)$id;
        $query = $this->db->query($request);
        //settings
        $request="DELETE FROM `".$this->db->dbprefix."settings` "."\n"
            ."WHERE `moduleId`=".(int)$id;
        $query = $this->db->query($request);
        //get permissions
        $request="SELECT `permissions`.`id`, `permissions`.`moduleId`"."\n"
            ."FROM `".$this->db->dbprefix."permissions` as `permissions`"."\n"
            ."WHERE `moduleId`=".(int)$id;
        $query = $this->db->query($request);
        $permissions=$query->result_array();
        if(!empty($permissions)){
            $conditions=array();
            foreach ($permissions as $p) {
                $conditions[]="`permissionId`=".(int)$p['id'];
            }
            // groups permissions
            $request="DELETE FROM `".$this->db->dbprefix."groups_permissions` "."\n"
                ."WHERE ".implode($conditions, ' OR ');
            $query = $this->db->query($request);
            //permissions
            $request="DELETE FROM `".$this->db->dbprefix."permissions` "."\n"
                ."WHERE `moduleId`=".(int)$id."\n";
            $query = $this->db->query($request);
        }
        //module
        $request="DELETE FROM `".$this->db->dbprefix."modules` "."\n"
            ."WHERE `id`=".(int)$id;
        $query = $this->db->query($request);
        return $query;
    }
}

?>
