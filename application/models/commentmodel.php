<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Commentmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function getComment($id){
        $request="SELECT  `comments`.`id`, `comment`, `author`, `date`, `interlinks`.`type`, `interlinks`.`id1` "."\n"
                ."FROM `".$this->db->dbprefix."comments` as `comments`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."interlinks` as `interlinks`"."\n"
                ."ON `interlinks`.`id1` = `comments`.`id`"."\n"
                ."WHERE  `comments`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $comment=$query->result_array();
        return (isset($comment[0])?$comment[0]:false);
    }

    function count($objectType=null, $objectId=null){
         $request="SELECT  COUNT(`comments`.`id`) as `cnt`"."\n"
            ."FROM `".$this->db->dbprefix."comments` as `comments`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."interlinks` as `interlinks`"."\n"
            ."ON `interlinks`.`id1` = `comments`.`id`"."\n"
            .(is_null($objectType)?"":"WHERE `interlinks`.`id2`=".(int)$objectId." AND `validated`='YES'"."\n"
            ."AND `type` = 'COMMENT_".mysql_real_escape_string(strtoupper($objectType))."'"."\n");
        $query = $this->db->query($request);
        $result = $query->result_array();
        return $result[0]['cnt'];
    }

    function listComments($objectType, $objectId, $limitStart=0, $limitMax=10, $validated=1){
        if (is_null($objectType)) {
            $request="SELECT  `comments`.`id`, `comment`, `author`, `validated`, `date`, `interlinks`.`type`, `interlinks`.`id1` "."\n"
                ."FROM `".$this->db->dbprefix."comments` as `comments`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."interlinks` as `interlinks`"."\n"
                ."ON `interlinks`.`id1` = `comments`.`id`"."\n"
                ."AND (`interlinks`.`type` = 'COMMENT_NEWS'"."\n"
                ." OR `interlinks`.`type` = 'COMMENT_PHOTO')"."\n"
                ."ORDER BY `date` DESC"."\n"
                .(is_null($limitStart)?"":"LIMIT ".(int)$limitStart.", ".(int)$limitMax);
        }
        else {
            $request="SELECT `comments`.`id`,`comment`, `author`, `date` "."\n"
                ."FROM `".$this->db->dbprefix."comments` as `comments`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."interlinks` as `interlinks`"."\n"
                ."ON `interlinks`.`id1` = `comments`.`id`"."\n"
                ."WHERE `interlinks`.`id2`=".(int)$objectId." ".($validated==1?"AND `validated`='YES'":"")."\n"
                ."AND `type` = 'COMMENT_".mysql_real_escape_string(strtoupper($objectType))."'"."\n"
                ."ORDER BY `date` DESC"."\n"
                .(is_null($limitStart)?"":"LIMIT ".(int)$limitStart.", ".(int)$limitMax);
        }
        $query = $this->db->query($request);
        return $query->result_array();
    }

    function insert($elementId, $elementType, $author, $comment, $validated='NO'){
        $request="INSERT INTO `".$this->db->dbprefix."comments` "."\n"
            ."(`id`, `author`, `date`, `comment`, `validated`) "."\n"
            ."VALUES (NULL, '".mysql_real_escape_string($author)."', "."\n"
            ."NOW(), '".mysql_real_escape_string($comment)."', '".$validated."');";
        $query = $this->db->query($request);
        if($query==false)
            return false;

        $commentId=$this->db->insert_id();
        $request="INSERT INTO `".$this->db->dbprefix."interlinks` "."\n"
            ."(`id`, `type`, `id1`, `id2`) "."\n"
            ."VALUES (NULL, 'COMMENT_".mysql_real_escape_string(strtoupper($elementType))."', ".$commentId.", ".(int)$elementId.");";
        $query = $this->db->query($request);
        if($query==false)
            return false;
    }

    function delete($ids){
        if (!is_array($ids))
            $ids = array($ids);
        $conditions=array();
        foreach ($ids as $id){
            $conditions[]= "`id1`=".(int)$id;
        }
        $request="DELETE FROM `".$this->db->dbprefix."interlinks` "."\n"
            ."WHERE `type` LIKE 'COMMENT_%' AND (".implode($conditions, ' OR ').")";
        $query = $this->db->query($request);
        if($query==false)
            return false;
        $conditions=array();
        foreach ($ids as $id){
            $conditions[]= "`id`=".(int)$id;
        }
        $request="DELETE FROM `".$this->db->dbprefix."comments` "."\n"
            ."WHERE (".implode($conditions, ' OR ').")";
        $query = $this->db->query($request);
        return $query;
    }

    function validate($id){
        $request="UPDATE `".$this->db->dbprefix."comments` SET "."\n"
            ."`validated` = 'YES' "."\n"
            ."WHERE `id` =".(int)$id." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }
}

?>
