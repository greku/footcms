<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Playermodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function getPlayers($teamId=null){
        //players of the team
         $request="SELECT `players`.`id`,`players`.`firstname`,`players`.`lastname`, `players`.`email`, `players`.`birthday` "."\n"
            .(!is_null($teamId)?", `players_teams`.`number`"."\n":"")
            ."FROM `".$this->db->dbprefix."players` AS `players`"."\n"
            .(!is_null($teamId)?"LEFT JOIN `".$this->db->dbprefix."players_teams` AS `players_teams`"."\n"
            ."ON `players_teams`.`playerId`=`players`.`id`"."\n"
            ."WHERE `players_teams`.`teamId`=".(int)$teamId."\n":"")
            ."ORDER BY `players`.`lastname` ASC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    function getPlayer($id){
        $request="SELECT `players`.`id`,`players`.`firstname`,`players`.`lastname`, `players`.`email`, `players`.`birthday`, `players`.`city` "."\n"
            ."FROM `".$this->db->dbprefix."players` AS `players`"."\n"
            ."WHERE `players`.`id`=".(int)$id;
        $query = $this->db->query($request);
        $player = $query->result_array();
        return (empty($player)?array():$player[0]);
    }

    /*
     * Create a new player
     */
    function createPlayer($lastname, $firstname, $email, $birthday, $city){
        $request="INSERT INTO `".$this->db->dbprefix."players` (`id`, `firstname`, `lastname`, `email`, `birthday`, `city`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($firstname)."', "
            ."'".mysql_real_escape_string($lastname)."', "
            ."'".mysql_real_escape_string($email)."', "
            .(empty($birthday)?"NULL":"'".mysql_real_escape_string($birthday)."'").", "
            ."'".mysql_real_escape_string($city)."' "
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Update place information
     */
    function updatePlayer($id, $lastname, $firstname, $email, $birthday, $city){
        $request="UPDATE `".$this->db->dbprefix."players` SET "."\n"
            ."`firstname` = '".mysql_real_escape_string($firstname)."', "."\n"
            ."`lastname` = '".mysql_real_escape_string($lastname)."', "."\n"
            ."`email` = '".mysql_real_escape_string($email)."', "."\n"
            ."`birthday` = ".(empty($birthday)?"NULL":"'".mysql_real_escape_string($birthday)."'").", "."\n"
            ."`city` = '".mysql_real_escape_string($city)."' "."\n"
            ."WHERE `id` =".(int)$id." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete player
     */
    function deletePlayer($id){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."goals` "."\n"
            ."WHERE `playerId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete cards
        $request="DELETE FROM `".$this->db->dbprefix."cards` "."\n"
            ."WHERE `playerId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete players_matches
        $request="DELETE FROM `".$this->db->dbprefix."players_matches` "."\n"
            ."WHERE `playerId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete players_teams
        $request="DELETE FROM `".$this->db->dbprefix."players_teams` "."\n"
            ."WHERE `playerId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete match
        $request="DELETE FROM `".$this->db->dbprefix."players` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    function getPlayerEmpty(){
        $player = array('id'=>'',
            'firstname'=>'',
            'lastname'=>'',
            'email'=>'',
            'birthday'=>'',
            'city'=>'');
        return $player;
    }

    function getGoalersFromTeam($teamId, $seasonId=null){
            //get goalers of the team
        $request="SELECT `players`.`lastname`, `players`.`firstname`, "."\n"
                ."COUNT(`goals`.`playerId`) as `nbgoals`"."\n"
                ."FROM `".$this->db->dbprefix."goals` AS `goals`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."players` AS `players`"."\n"
                ."ON `players`.`id` = `goals`.`playerId`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."teams` as `teams`"."\n"
                ."ON `goals`.`teamId`=`teams`.`id` "."\n"
                ."AND `goals`.`teamId`=".(int)$teamId."\n"
                ."INNER JOIN `".$this->db->dbprefix."matches` AS `matches`"."\n"
                ."ON `matches`.`id`=`goals`.`matchId`"."\n"
                .(!is_null($seasonId)?"WHERE `teams`.`seasonId`=".(int)$seasonId."\n":"")
                ."GROUP BY `players`.`id`"."\n"
                ."ORDER BY `nbgoals` DESC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get list of teams the player is part of
     */
    function getTeamStats($id) {
        $request="SELECT `teams`.`name`, `clubs`.`name` as `club`, `seasons`.`year`, `players_teams`.`number`"."\n"
            ."FROM `".$this->db->dbprefix."players_teams` as `players_teams`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` as `teams`"."\n"
            ."ON `teams`.`id`=`players_teams`.`teamId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."seasons` as `seasons`"."\n"
            ."ON `seasons`.`id`=`teams`.`seasonId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."clubs` as `clubs`"."\n"
            ."ON `clubs`.`id`=`teams`.`clubId`"."\n"
            ."WHERE `players_teams`.`playerId`=".(int)$id."\n"
            ."ORDER BY `seasons`.`id` DESC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get number of match played
     */
    function getMatchCount($id, $seasonId=null) {
        if (is_null($seasonId)) {
            $request="SELECT COUNT(`players_matches`.`matchId`) as `n`"."\n"
                ."FROM `".$this->db->dbprefix."players_matches` as `players_matches`"."\n"
                ."WHERE `players_matches`.`playerId`=".(int)$id."\n"
                ."GROUP BY `players_matches`.`playerId`"."\n";
        }
        else {
            $request="SELECT COUNT(`players_matches`.`matchId`) as `n`"."\n"
                ."FROM `".$this->db->dbprefix."players_matches` as `players_matches`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."teams` as `teams`"."\n"
                ."ON `teams`.`id`=`players_matches`.`teamId`"."\n"
                ."WHERE `players_matches`.`playerId`=".(int)$id."\n"
                ."AND `teams`.`seasonId`=".(int)$seasonId."\n"
                ."GROUP BY `players_matches`.`playerId`"."\n";
        }
        $query = $this->db->query($request);
        $data = $query->result_array();
        if (empty($data))
            return 0;
        else
            return $data[0]['n'];
    }

    /*
     * Get number goals
     */
    function getGoalCount($id, $seasonId=null) {
        if (is_null($seasonId)) {
            $request="SELECT COUNT(`goals`.`id`) as `n`"."\n"
                ."FROM `".$this->db->dbprefix."goals` as `goals`"."\n"
                ."WHERE `goals`.`playerId`=".(int)$id."\n"
                ."GROUP BY `goals`.`playerId`"."\n";
        }
        else {
            $request="SELECT COUNT(`goals`.`id`) as `n`"."\n"
                ."FROM `".$this->db->dbprefix."goals` as `goals`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."teams` as `teams`"."\n"
                ."ON `teams`.`id`=`goals`.`teamId`"."\n"
                ."WHERE `goals`.`playerId`=".(int)$id."\n"
                ."AND `teams`.`seasonId`=".(int)$seasonId."\n"
                ."GROUP BY `goals`.`playerId`"."\n";
        }
        $query = $this->db->query($request);
        $data = $query->result_array();
        if (empty($data))
            return 0;
        else
            return $data[0]['n'];
    }

    /*
     * Get number of cards
     */
    function getCardCount($id, $seasonId=null) {
        if (is_null($seasonId)) {
            $request="SELECT COUNT(`cards`.`id`) as `n`"."\n"
                ."FROM `".$this->db->dbprefix."cards` as `cards`"."\n"
                ."WHERE `cards`.`playerId`=".(int)$id."\n"
                ."GROUP BY `cards`.`playerId`"."\n";
        }
        else {
            $request="SELECT COUNT(`cards`.`id`) as `n`"."\n"
                ."FROM `".$this->db->dbprefix."cards` as `cards`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."teams` as `teams`"."\n"
                ."ON `teams`.`id`=`cards`.`teamId`"."\n"
                ."WHERE `cards`.`playerId`=".(int)$id."\n"
                ."AND `teams`.`seasonId`=".(int)$seasonId."\n"
                ."GROUP BY `cards`.`playerId`"."\n";
        }
        $query = $this->db->query($request);
        $data = $query->result_array();
        if (empty($data))
            return 0;
        else
            return $data[0]['n'];
    }
}

?>
