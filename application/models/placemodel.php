<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Placemodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get place
     */
    function getPlace($id){
		$request="SELECT `places`.`id`, `places`.`name`, `places`.`shortname`, `places`.`street`, "."\n"
			."`places`.`number`, `places`.`postalcode`, `places`.`city`, `places`.`phone`"."\n"
			."FROM `".$this->db->dbprefix."places` AS `places`"."\n"
			."WHERE `places`.`id`=".(int)$id;
        $query = $this->db->query($request);
        $place = $query->result_array();
        return (empty($place)?array():$place[0]);
    }

	/*
     * Get place empty
     */
    function getPlaceEmpty(){
		$place = array('id'=>'',
			'name'=>'',
			'shortname'=>'',
			'street'=>'',
			'number'=>'',
			'postalcode'=>'',
			'city'=>'',
			'phone'=>'');
        return $place;
    }

    /*
     * Update place information
     */
    function updatePlace($id, $name, $shortname, $street, $number, $postalcode, $city, $phone){
		$request="UPDATE `".$this->db->dbprefix."places` SET "."\n"
			."`name` = '".mysql_real_escape_string($name)."', "."\n"
			."`shortname` = '".mysql_real_escape_string($shortname)."', "."\n"
			."`street` = '".mysql_real_escape_string($street)."', "."\n"
			."`number` = '".mysql_real_escape_string($number)."', "."\n"
			."`postalcode` = '".mysql_real_escape_string($postalcode)."', "."\n"
			."`city` = '".mysql_real_escape_string($city)."', "."\n"
			."`phone` = '".mysql_real_escape_string($phone)."' "."\n"
			."WHERE `id` =".(int)$id." LIMIT 1 ;";

        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Create a new place
     */
    function createPlace($name, $shortname, $street, $number, $postalcode, $city, $phone){
		$request="INSERT INTO `".$this->db->dbprefix."places` (`id`, `name`, `shortname`, `street`, `number`, `postalcode`, `city`, `phone`) VALUES ("."\n"
			."NULL, "
			."'".mysql_real_escape_string($name)."', "
			."'".mysql_real_escape_string($shortname)."', "
			."'".mysql_real_escape_string($street)."', "
			."'".mysql_real_escape_string($number)."', "
			."'".mysql_real_escape_string($postalcode)."', "
			."'".mysql_real_escape_string($city)."', "
			."'".mysql_real_escape_string($phone)."'"
			.");";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete place
     */
    function deletePlace($id){
		$request = "UPDATE `".$this->db->dbprefix."teams` "."\n"
            ."SET `placeId`=NULL "."\n"
            ."WHERE `placeId`=".(int)$id;
        $query = $this->db->query($request);
        $request="DELETE FROM `".$this->db->dbprefix."places` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get places
     */
    function getPlaces(){
		$request="SELECT `places`.`id`, `places`.`name`, `places`.`shortname`, `places`.`street`, "."\n"
			."`places`.`number`, `places`.`postalcode`, `places`.`city`, `places`.`phone`"."\n"
			."FROM `".$this->db->dbprefix."places` AS `places`"."\n"
			."ORDER BY `places`.`city` ASC";
        $query = $this->db->query($request);
        $places = $query->result_array();
        return $places;
    }


}

?>
