<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Blockmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function getBlocks($show){
        $request="SELECT `blocks`.`id`, `blocks`.`shortname`, `blocks`.`name`, "."\n"
            ."`blocks`.`show`, `blocks`.`params`, "."\n"
            ."`blocks`.`moduleId`, `modules`.`name` as `moduleName`"."\n"
            ."FROM `".$this->db->dbprefix."blocks` as `blocks`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`blocks`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            .(!is_null($show)?"AND `blocks`.`show`=".(int)$show."\n":'')
            ."ORDER BY `blocks`.`rank` ASC";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get block
     */
    function getBlock($id) {
        $request="SELECT `blocks`.`rank`, `blocks`.`id`, `blocks`.`shortname`, `blocks`.`name`, `blocks`.`params`, "."\n"
            ."`blocks`.`moduleId`, `modules`.`name` as `moduleName`"."\n"
            ."FROM `".$this->db->dbprefix."blocks` as `blocks`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`blocks`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `blocks`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $item = $query->result_array();
        return (empty($item)?array():$item[0]);
    }

    /*
     * Add a block
     */
    function addBlock($shortname, $name, $params, $moduleId){
		$request="INSERT INTO `".$this->db->dbprefix."blocks` (`id`, `shortname`, `name`, `params`, "
            ."`moduleId`, `rank`) VALUES ("."\n"
			."NULL, "
			."'".mysql_real_escape_string($shortname)."', "
			."'".mysql_real_escape_string($name)."', "
			."'".mysql_real_escape_string($params)."', "
			."".(int)$moduleId.", "
			."0 "
			.");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
        }
        else
            return false;
		$request="UPDATE `".$this->db->dbprefix."blocks` SET "."\n"
			."`rank` = ".(int)$id." \n"
			."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        if($query!==false)
            return $id;
        return $query;
    }

    /*
     * Get previous block
     */
    function getPreviousBlock($rank) {
        $request="SELECT `blocks`.`id`, `blocks`.`rank`"."\n"
            ."FROM `".$this->db->dbprefix."blocks` AS `blocks`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`blocks`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `blocks`.`rank`<".(int)$rank."\n"
            ."ORDER BY `rank` DESC LIMIT 1";
        $query = $this->db->query($request);
        $item = $query->result_array();
        return (empty($item)?array():$item[0]);
    }

    /*
     * Get next block
     */
    function getNextBlock($rank) {
        $request="SELECT `blocks`.`id`, `blocks`.`rank`"."\n"
            ."FROM `".$this->db->dbprefix."blocks` AS `blocks`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`blocks`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `blocks`.`rank`>".(int)$rank."\n"
            ."ORDER BY `rank` ASC LIMIT 1";
        $query = $this->db->query($request);
        $item = $query->result_array();
        return (empty($item)?array():$item[0]);
    }

    /*
     * Update block rank
     */
    function updateBlockRank($id, $rank){
		$request="UPDATE `".$this->db->dbprefix."blocks` SET "."\n"
			."`rank` = ".(int)$rank."\n"
			."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Update block show
     */
    function updateBlockShow($id, $show){
		$request="UPDATE `".$this->db->dbprefix."blocks` SET "."\n"
			."`show` = ".(int)$show."\n"
			."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Update block params
     */
    function updateBlockParams($id, $params){
		$request="UPDATE `".$this->db->dbprefix."blocks` SET "."\n"
			."`params` = '".mysql_real_escape_string($params)."' \n"
			."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }
}

?>
