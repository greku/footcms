<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Settingmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get settings
     */
    function getSettings($module=null){
        $request="SELECT `settings`.`id`, `settings`.`name`, `settings`.`value`, `settings`.`moduleId`"."\n"
            ."FROM `".$this->db->dbprefix."settings` AS `settings`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` AS `modules`"."\n"
            ."ON `modules`.`id`=`settings`.`moduleId`"."\n"
            ."WHERE ".(is_null($module)?"`modules`.`id` IS NULL":"`modules`.`name`='".mysql_real_escape_string($module)."'")."\n";
        $query = $this->db->query($request);
        $settings = $query->result_array();
        return $settings;
    }

    /*
     * Update setting
     */
    function updateSetting($name, $value, $moduleId=null){
        $request="UPDATE `".$this->db->dbprefix."settings` SET "."\n"
            ."`value` = '".mysql_real_escape_string($value)."'"."\n"
            ."WHERE `name` ='".mysql_real_escape_string($name)."'"."\n"
            ."AND ".(is_null($moduleId)?"`moduleId` IS NULL":"`moduleId`=".(int)$moduleId)."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Add setting
     */
    function addSetting($moduleId, $name, $value){
        $request="INSERT INTO `".$this->db->dbprefix."settings` (`id`, "
            ."`moduleId`, `name`, `value`) VALUES ("."\n"
            ."NULL, "
            .(is_null($moduleId)?"NULL":(int)$moduleId).", "
            ."'".mysql_real_escape_string($name)."', "
            ."'".mysql_real_escape_string($value)."' "
            .");";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get available languages
     */
    function getAvailableLanguages(){
        $languages=array();
        if ($handle = opendir(APPPATH.'language/')) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($file = readdir($handle))) {
                if ($file!='.' && $file!='..' && is_dir(APPPATH.'language/'.$file)){
                    if(file_exists(APPPATH.'language/'.$file.'/site_lang.php'))
                        $languages[]=$file;
                }
            }
            closedir($handle);
        }
        return $languages;
    }

    /*
     * Get available templates
     */
    function getAvailableTemplates(){
        $templates=array();
        if ($handle = opendir(APPPATH.'views/')) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($file = readdir($handle))) {
                if ($file!='.' && $file!='..' && is_dir(APPPATH.'views/'.$file))
                    $templates[]=$file;
            }
            closedir($handle);
        }
        return $templates;
    }

    /*
     * Get season
     */
    function getSeason($id){
        $request="SELECT `seasons`.`id`, `seasons`.`year`"."\n"
            ."FROM `".$this->db->dbprefix."seasons` AS `seasons`"."\n"
            ."WHERE `seasons`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $season = $query->result_array();
        return (empty($season)?array():$season[0]);
    }

    /*
     * New season
     */
    function createNewSeason($year){
        $request="INSERT INTO `".$this->db->dbprefix."seasons` (`id`, "
            ."`year`) VALUES ("."\n"
            ."NULL, "
            .(int)$year." "
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }
}

?>
