<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Matchmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get match
     */
    function getMatch($id, $competitionId=null){
        $request="SELECT `matches`.`id`, `matches`.`date`, TIME(`matches`.`date`) as `time`, "."\n"
            ."`team1`.`name` as `teamName1`, `team2`.`name` as `teamName2`, "."\n"
            ."`matches`.`teamId1`, `matches`.`teamId2`, "."\n"
            ."`matches`.`score1`, `matches`.`score2`, `matches`.`forfeit`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team1`"."\n"
            ."ON `team1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team2`"."\n"
            ."ON `team2`.`id`=`matches`.`teamId2`"."\n"
            ."WHERE `matches`.`id`=".(int)$id."\n"
            .(!is_null($competitionId)?"AND `matches`.`competitionId`=".(int)$competitionId."\n":"");
        $query = $this->db->query($request);
        $match = $query->result_array();
        return (empty($match)?array():$match[0]);
    }

    /*
     * Get matches of the competitions
     */
    function getMatches($competitionId, $limitStart=0, $limitMax=20, $teamId=null) {
        $conditions=array();
        if (!is_null($competitionId))
            $conditions[]= "`matches`.`competitionId`=".(int)$competitionId."\n";
        if (!is_null($teamId))
            $conditions[]=" (`matches`.`teamId1`=".(int)$teamId."\n"."OR `matches`.`teamId2`=".(int)$teamId.")"."\n";
        $request="SELECT `matches`.`id`, `matches`.`date`, TIME(`matches`.`date`) as `time`, "."\n"
            ."`team1`.`name` as `teamName1`, `team2`.`name` as `teamName2`,"."\n"
            ."`matches`.`score1`, `matches`.`score2`, `matches`.`forfeit`,"."\n"
            ."`interlinks`.`id1` AS `reportId`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team1`"."\n"
            ."ON `team1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team2`"."\n"
            ."ON `team2`.`id`=`matches`.`teamId2`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."AND `interlinks`.`id2`=`matches`.`id`"."\n"
            ."WHERE"."\n"
            .implode($conditions, " AND ")
            ."ORDER BY `matches`.`date` ASC"."\n"
            ."LIMIT ".(int)$limitStart.", ".(int)$limitMax;
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Delete match
     */
    function deleteMatch($id){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."goals` "."\n"
            ."WHERE `matchId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete cards
        $request="DELETE FROM `".$this->db->dbprefix."cards` "."\n"
            ."WHERE `matchId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete players_matches
        $request="DELETE FROM `".$this->db->dbprefix."players_matches` "."\n"
            ."WHERE `matchId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete news (news + interlinks)
        $request="SELECT `interlinks`.`id1`"."\n"
            ."FROM `".$this->db->dbprefix."interlinks` AS `interlinks` "."\n"
            ."WHERE `interlinks`.`type`='NEWS_MATCH' AND `interlinks`.`id2`=".(int)$id."\n";
        $query = $this->db->query($request);
        $interlinks = $query->result_array();
        foreach ($interlinks as $interlink) {
            $request="DELETE FROM `".$this->db->dbprefix."news` "."\n"
                ."WHERE `id`=".(int)$interlink['id1']."\n";
            $query = $this->db->query($request);
            $request="DELETE FROM `".$this->db->dbprefix."interlinks` "."\n"
                ."WHERE `type`='NEWS_MATCH' AND `id2`=".(int)$id;
            $query = $this->db->query($request);
        }
        //delete match
        $request="DELETE FROM `".$this->db->dbprefix."matches` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Count matches of a competition
     */
    function count($competitionId) {
        $request="SELECT COUNT(`matches`.`id`) as `cnt`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."WHERE `matches`.`competitionId`=".(int)$competitionId."\n";
        $query = $this->db->query($request);
        $result = $query->result_array();
        return $result[0]['cnt'];
    }

    /*
     * Get upcoming matches of the competitions
     */
    function getUpcomingMatches($competitionId, $n=5) {
        $request="SELECT `matches`.`id`, `matches`.`date`, TIME(`matches`.`date`) as `time`, "."\n"
            ."`team1`.`name` as `teamName1`, `team2`.`name` as `teamName2`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team1`"."\n"
            ."ON `team1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team2`"."\n"
            ."ON `team2`.`id`=`matches`.`teamId2`"."\n"
            ."WHERE `matches`.`competitionId`=".(int)$competitionId."\n"
            ." AND `matches`.`date`>NOW()"."\n"
            ."ORDER BY `matches`.`date` ASC"."\n"
            ."LIMIT ".(int)$n;
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get matches without score
     */
    function getMatchesWithoutScore($competitionId, $n=5) {
        $request="SELECT `matches`.`id`, `matches`.`date`, TIME(`matches`.`date`) as `time`, "."\n"
            ."`team1`.`name` as `teamName1`, `team2`.`name` as `teamName2`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team1`"."\n"
            ."ON `team1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team2`"."\n"
            ."ON `team2`.`id`=`matches`.`teamId2`"."\n"
            ."WHERE `matches`.`competitionId`=".(int)$competitionId."\n"
            ." AND `matches`.`score1` IS NULL"."\n"
            ." AND `matches`.`date`<NOW()"."\n"
            ."ORDER BY `matches`.`date` ASC"."\n"
            ."LIMIT ".(int)$n;
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get matches without reports
     */
    function getMatchesWithoutReport($competitionId, $clubId, $seasonId) {
        $conditions=array();
        if (!is_null($competitionId))
            $conditions[]= "`matches`.`competitionId`=".(int)$competitionId."\n";
        if (!is_null($clubId))
            $conditions[]=" (`team1`.`clubId`=".(int)$clubId."\n"."OR `team2`.`clubId`=".(int)$clubId.")"."\n";
        $conditions[]="`interlinks`.`id1` IS NULL AND `matches`.`date`<NOW()"."\n";
        if (!is_null($seasonId))
            $conditions[]="`team1`.`seasonId`=".(int)$seasonId."\n";
        $request="SELECT `matches`.`id`, `matches`.`date`, TIME(`matches`.`date`) as `time`, "."\n"
            ."`team1`.`name` as `teamName1`, `team2`.`name` as `teamName2`,"."\n"
            ."`matches`.`score1`, `matches`.`score2`, `matches`.`forfeit`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team1`"."\n"
            ."ON `team1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `team2`"."\n"
            ."ON `team2`.`id`=`matches`.`teamId2`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."AND `interlinks`.`id2`=`matches`.`id`"."\n"
            ."WHERE"."\n"
            .implode($conditions, " AND ")
            ."ORDER BY `matches`.`date` ASC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Create match
     */
    function createMatch($competitionId, $teamId1, $teamId2, $date) {
        $request="INSERT INTO `".$this->db->dbprefix."matches` (`id`, `competitionId`, `teamId1`, `teamId2`, `date`) VALUES ("."\n"
            ."NULL, "
            ."".(int)$competitionId.", "
            ."".(int)$teamId1.", "
            ."".(int)$teamId2.", "
            ."'".mysql_real_escape_string($date)."' "
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Update match information
     */
    function updateMatch($matchId, $date, $score1, $score2, $forfeit){
        $request="UPDATE `".$this->db->dbprefix."matches` SET "."\n"
            ."`date` = '".mysql_real_escape_string($date)."', "."\n"
            ."`score1` = ".(is_null($score1)?'NULL':(int)$score1).", "."\n"
            ."`score2` = ".(is_null($score2)?'NULL':(int)$score2).", "."\n"
            ."`forfeit` = ".(int)$forfeit.""."\n"
            ."WHERE `id` =".(int)$matchId." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get participating players
     */
    function getMatchPlayers($matchId) {
        $request="SELECT `teamId`, `matchId`, `playerId`, `number` "."\n"
            ."FROM `".$this->db->dbprefix."players_matches`"."\n"
            ."WHERE `matchId`=".(int)$matchId."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Insert participating players match
     */
    function insertMatchPlayers($matchId, $players) {
        $data=array();
        foreach ($players as $player){
            $data[]="( ".(int)$matchId.", "
                .(int)$player['team'].", "
                .(int)$player['player'].", "
                .(is_null($player['number'])?'NULL':(int)$player['number']).") "."\n";
        }
        $request="INSERT INTO `".$this->db->dbprefix."players_matches` (`matchId`, `teamId`, `playerId`, `number`) VALUES "."\n"
            .implode($data, ', ')
            .";";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Delete participating players
     */
    function deleteMatchPlayers($matchId){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."players_matches` "."\n"
            ."WHERE `matchId`=".(int)$matchId."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get goals
     */
    function getMatchGoals($matchId) {
        $request="SELECT `teamId`, `matchId`, `playerId`, `minute` "."\n"
            ."FROM `".$this->db->dbprefix."goals`"."\n"
            ."WHERE `matchId`=".(int)$matchId."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Insert goals
     */
    function insertMatchGoals($matchId, $goals) {
        $data=array();
        foreach ($goals as $goal){
            $data[]="( ".(int)$matchId.", "
                .(int)$goal['team'].", "
                .(int)$goal['player'].", "
                .(is_null($goal['minute'])?'NULL':(int)$goal['minute']).") "."\n";
        }
        $request="INSERT INTO `".$this->db->dbprefix."goals` (`matchId`, `teamId`, `playerId`, `minute`) VALUES "."\n"
            .implode($data, ', ').";";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Delete goals
     */
    function deleteMatchGoals($matchId){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."goals` "."\n"
            ."WHERE `matchId`=".(int)$matchId."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get cards
     */
    function getMatchCards($matchId) {
        $request="SELECT `teamId`, `matchId`, `playerId`, `color`, `minute` "."\n"
            ."FROM `".$this->db->dbprefix."cards`"."\n"
            ."WHERE `matchId`=".(int)$matchId."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Insert cards
     */
    function insertMatchCards($matchId, $cards) {
        $data=array();
        foreach ($cards as $card){
            $data[]="( ".(int)$matchId.", "
                .(int)$card['team'].", "
                .(int)$card['player'].", "
                ."'".mysql_real_escape_string($card['color'])."' , "
                .(is_null($card['minute'])?'NULL':(int)$card['minute']).") "."\n";
        }
        $request="INSERT INTO `".$this->db->dbprefix."cards` (`matchId`, `teamId`, `playerId`, `color`, `minute`) VALUES "."\n"
            .implode($data, ', ').";";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Delete cards
     */
    function deleteMatchCards($matchId){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."cards` "."\n"
            ."WHERE `matchId`=".(int)$matchId."\n";
        $query = $this->db->query($request);
        return $query;
    }

    function getNext($max){
        $request="SELECT `matches`.`id`, `matches`.`date`, "."\n"
            ."`competitions`.`name` AS `competitionName`, "."\n"
            ."`teams1`.`name` AS `teamName1`,"."\n"
            ."`teams2`.`name` AS `teamName2`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."ON `competitions`.`id`=`matches`.`competitionId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `teams1`"."\n"
            ."ON `teams1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `teams2`"."\n"
            ."ON `teams2`.`id`=`matches`.`teamId2`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."clubs` AS `clubs1`"."\n"
            ."ON `clubs1`.`id`=`teams1`.`clubId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."clubs` AS `clubs2`"."\n"
            ."ON `clubs2`.`id`=`teams2`.`clubId`"."\n"
            ."WHERE `competitions`.`seasonId`=".$this->config->fcms('seasonId')."\n"
            ."AND (`clubs1`.`id`=".$this->config->fcms('myClubId')
            ." OR `clubs2`.`id`=".$this->config->fcms('myClubId').")"."\n"
            ."AND `matches`.`date` > NOW()"."\n"
            ."ORDER BY `matches`.`date` ASC"."\n"
            ."LIMIT 0, ".(int)$max;
        $query = $this->db->query($request);
        return $query->result_array();
    }

    function getList(){
        $request="SELECT `matches`.`id`, `matches`.`date`, "."\n"
            ."`matches`.`score1`, `matches`.`score2`,"."\n"
            ."`matches`.`forfeit`,"."\n"
            ."`competitions`.`name` AS `competitionName`, "."\n"
            ."`teams1`.`name` AS `teamName1`,"."\n"
            ."`teams2`.`name` AS `teamName2`,"."\n"
            ."IF(`matches`.`date`>NOW(),1,0) AS `futur`,"."\n"
            ."`interlinks`.`id1` IS NOT NULL  AS `report`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."ON `competitions`.`id`=`matches`.`competitionId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `teams1`"."\n"
            ."ON `teams1`.`id`=`matches`.`teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `teams2`"."\n"
            ."ON `teams2`.`id`=`matches`.`teamId2`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."clubs` AS `clubs1`"."\n"
            ."ON `clubs1`.`id`=`teams1`.`clubId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."clubs` AS `clubs2`"."\n"
            ."ON `clubs2`.`id`=`teams2`.`clubId`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`id2`=`matches`.`id`"."\n"
            ."AND `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."WHERE `competitions`.`seasonId`=".$this->config->fcms('seasonId')."\n"
            ."AND (`clubs1`.`id`=".$this->config->fcms('myClubId')
                ." OR `clubs2`.`id`=".$this->config->fcms('myClubId').")"."\n"
            ."ORDER BY `matches`.`date` ASC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    function getReport($id=0){
        /* Get general items */
        $request="SELECT `score1`, `score2`, `forfeit`,"."\n"
            ."`teamId1`,"."\n"
            ."`teamId2`,"."\n"
            ."`team1`.`name` as `teamName1`,"."\n"
            ."`team2`.`name` as `teamName2`"."\n"
            ."FROM `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` as `team1`"."\n"
            ."ON `team1`.`id` = `teamId1`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` as `team2`"."\n"
            ."ON `team2`.`id` = `teamId2`"."\n"
            ."WHERE `matches`.`id`=".$id;

        $query = $this->db->query($request);

        $data = $query->result_array();
        if(count($data)==0)
            return false;
        $data=$data[0];

        $data['team1']=array();
        $data['team1']=array();
        $report['forfeit']=$data['forfeit'];
        $report['team1']['name']=$data['teamName1'];
        $report['team1']['score']=$data['score1'];
        $report['team2']['name']=$data['teamName2'];
        $report['team2']['score']=$data['score2'];

        /* Get news */
        $request="SELECT `news`.`id`,`news`.`date`,`news`.`title`, "."\n"
            ."`news`.`content`, `users`.`username` as `author`, "."\n"
            ."COUNT(`comments`.`id`) as `nbcomments` "."\n"
            ."FROM `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."news` AS `news` "."\n"
            ."ON `news`.`id`=`interlinks`.`id1` "."\n"
            ."INNER JOIN `".$this->db->dbprefix."users` as `users`"."\n"
            ."ON `users`.`id` = `news`.`authorId`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` as `interlinks2`"."\n"
            ."ON `news`.`id` = `interlinks2`.`id2`"."\n"
            ."AND `interlinks2`.`type`='COMMENT_NEWS'"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."comments` as `comments`"."\n"
            ."ON `interlinks2`.`id1` = `comments`.`id`"."\n"
            ."AND `comments`.`validated`='YES'"."\n"
            ."WHERE `interlinks`.`id2`=".(int)$id."\n"
            ."AND `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."GROUP BY `news`.`id`"."\n"
            ."LIMIT 1"."\n";
        $query = $this->db->query($request);
        $data2 = $query->result_array();
        if(count($data2)==0)
            return false;
        $report['news']=$data2[0];
        unset($data2);

        /* Get players */
        $request="SELECT `players_matches`.`number` ,"."\n"
            ."`players`.`lastName`, `players`.`firstName`,"."\n"
            ."`players_teams`.`teamId`"."\n"
            ."FROM `".$this->db->dbprefix."players_matches` AS `players_matches`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."players` AS `players`"."\n"
            ."ON `players`.`id` = `players_matches`.`playerId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."players_teams` AS `players_teams`"."\n"
            ."ON `players_teams`.`playerId`=`players`.`id`"."\n"
            ."WHERE `matchId`=".(int)$id."\n"
            ."AND (`players_teams`.`teamId`=".$data['teamId1']." "."\n"
            ."OR `players_teams`.`teamId`=".$data['teamId2'].")"."\n"
            ."ORDER BY `players`.`lastName` ASC"."\n";
        $query = $this->db->query($request);
        $players = $query->result_array();
        $report['team1']['players']=array();
        $report['team2']['players']=array();
        foreach ($players as $player){
            if($player['teamId']==$data['teamId1'])
                $report['team1']['players'][]=$player;
            else
                $report['team2']['players'][]=$player;
        }
        unset($players);

        /* Get goals */
        $request="SELECT "."\n"
            ."`players`.`lastName`, `players`.`firstName`,"."\n"
            ."`players_teams`.`teamId`"."\n"
            ."FROM `".$this->db->dbprefix."goals` AS `goals`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."players` AS `players`"."\n"
            ."ON `players`.`id` = `goals`.`playerId`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."players_teams` AS `players_teams`"."\n"
            ."ON `players_teams`.`playerId`=`players`.`id`"."\n"
            ."WHERE `matchId`=".(int)$id."\n"
            ."AND (`players_teams`.`teamId`=".$data['teamId1']." OR `players_teams`.`teamId`=".$data['teamId2'].")"."\n"
            ."ORDER BY `goals`.`id` ASC"."\n";
        $query = $this->db->query($request);
        $goals = $query->result_array();
        $report['team1']['goals']=array();
        $report['team2']['goals']=array();
        foreach ($goals as $goal){
            if($goal['teamId']==$data['teamId1'])
                $report['team1']['goals'][]=$goal;
            else
                $report['team2']['goals'][]=$goal;
        }
        unset($goals);

        /* Get cards */
        $request="SELECT `color` ,"."\n"
                ."`players`.`lastName`, `players`.`firstName`,"."\n"
                ."`players_teams`.`teamId`"."\n"
                ."FROM `".$this->db->dbprefix."cards` AS `cards`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."players` AS `players`"."\n"
                ."ON `players`.`id` = `cards`.`playerId`"."\n"
                ."INNER JOIN `".$this->db->dbprefix."players_teams` AS `players_teams`"."\n"
                ."ON `players_teams`.`playerId`=`players`.`id`"."\n"
                ."WHERE `matchId`=".(int)$id."\n"
                ."AND (`players_teams`.`teamId`=".$data['teamId1']." "."\n"
                ."OR `players_teams`.`teamId`=".$data['teamId2'].")"."\n"
                ."ORDER BY `cards`.`minute` ASC"."\n";
        $query = $this->db->query($request);
        $cards = $query->result_array();
        $report['team1']['cards']=array();
        $report['team2']['cards']=array();
        foreach ($cards as $card){
            if($card['teamId']==$data['teamId1'])
                $report['team1']['cards'][]=$card;
            else
                $report['team2']['cards'][]=$card;
        }
        unset($cards);
        return $report;
    }
}

?>
