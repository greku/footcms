<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Menumodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get menu items
     */
    function getItems($admin, $show=1) {
        $request="SELECT `menu`.`id`, `menu`.`text`, `menu`.`parent`, `menu`.`rank`, `menu`.`show`"."\n"
            ."FROM `".$this->db->dbprefix."menu` AS `menu`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`menu`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `menu`.`admin`=".(int)$admin."\n"
            .(!is_null($show)?"AND `menu`.`show`=".(int)$show."\n":"")
            ."ORDER BY `menu`.`rank` ASC";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get menu item
     */
    function getItem($itemId, $admin) {
        $request="SELECT `menu`.`id`, `menu`.`text`, `menu`.`parent`, `menu`.`rank`, `menu`.`show`"."\n"
            ."FROM `".$this->db->dbprefix."menu` AS `menu`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`menu`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `menu`.`id`=".(int)$itemId."\n"
            ."AND `menu`.`admin`=".(int)$admin."\n";
        $query = $this->db->query($request);
        $item = $query->result_array();
        return (empty($item)?array():$item[0]);
    }

    /*
     * Add an item
     */
    function addItem($moduleId, $parent, $text, $link, $admin, $show=1){
        $request="INSERT INTO `".$this->db->dbprefix."menu` (`id`, `text`, `link`, "
            ."`moduleId`, `parent`, `show`, `admin`, `rank`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($text)."', "
            ."'".mysql_real_escape_string($link)."', "
            .(is_null($moduleId)?"NULL":(int)$moduleId).", "
            .(int)$parent.", "
            .(int)$show.", "
            .(int)$admin.", "
            ."0 "
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
        }
        else
            return false;
        $request="UPDATE `".$this->db->dbprefix."menu` SET "."\n"
            ."`rank` = ".(int)$id." \n"
            ."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        if($query!==false)
            return $id;
        return $query;
    }

    /*
     * Get previous item
     */
    function getPreviousItem($rank, $admin) {
        $request="SELECT `menu`.`id`, `menu`.`rank`"."\n"
            ."FROM `".$this->db->dbprefix."menu` AS `menu`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`menu`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `menu`.`rank`<".(int)$rank."\n"
            ."AND `menu`.`admin`=".(int)$admin."\n"
            ."AND `menu`.`show`=1"."\n"
            ."ORDER BY `rank` DESC LIMIT 1";
        $query = $this->db->query($request);
        $item = $query->result_array();
        return (empty($item)?array():$item[0]);
    }

    /*
     * Get next item
     */
    function getNextItem($rank, $admin) {
        $request="SELECT `menu`.`id`, `menu`.`rank`"."\n"
            ."FROM `".$this->db->dbprefix."menu` AS `menu`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."modules` as `modules`"."\n"
            ."ON `modules`.`id`=`menu`.`moduleId`"."\n"
            ."WHERE (`modules`.`enable` IS NULL OR `modules`.`enable`=1)"."\n"
            ."AND `menu`.`rank`>".(int)$rank."\n"
            ."AND `menu`.`admin`=".(int)$admin."\n"
            ."AND `menu`.`show`=1"."\n"
            ."ORDER BY `rank` ASC LIMIT 1";
        $query = $this->db->query($request);
        $item = $query->result_array();
        return (empty($item)?array():$item[0]);
    }

    /*
     * Update item rank
     */
    function updateItemRank($itemId, $rank){
        $request="UPDATE `".$this->db->dbprefix."menu` SET "."\n"
            ."`rank` = ".(int)$rank."\n"
            ."WHERE `id` =".(int)$itemId."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Update item show
     */
    function updateItemShow($id, $show){
        $request="UPDATE `".$this->db->dbprefix."menu` SET "."\n"
            ."`show` = ".(int)$show."\n"
            ."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Update item text
     */
    function updateItemText($id, $text){
        $request="UPDATE `".$this->db->dbprefix."menu` SET "."\n"
            ."`text` = '".mysql_real_escape_string($text)."'"."\n"
            ."WHERE `id` =".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete item
     */
    function deleteItem($id){
        $request="DELETE FROM `".$this->db->dbprefix."menu` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

}

?>
