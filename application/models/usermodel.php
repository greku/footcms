<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Usermodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get user info
     */
    function getUser($id){
        $request="SELECT `users`.`id`, `users`.`username`, `users`.`email`, `groups`.`id` as `groupId`"."\n"
            ."FROM `".$this->db->dbprefix."users` as `users`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."groups` as `groups`"."\n"
            ."ON `users`.`groupId` = `groups`.`id` "."\n"
            ."WHERE `users`.`id`=".(int)$id;
        $query = $this->db->query($request);
        $user=$query->result_array();
        return (empty($user)?array():$user[0]);
    }

    /*
     * Return the id of the username if exists
     */
    function checkUsername($username){
        $request="SELECT `users`.`id` "."\n"
            ."FROM `".$this->db->dbprefix."users` as `users`"."\n"
            ."WHERE `users`.`username`='".mysql_real_escape_string($username)."'";
        $query=$this->db->query($request);
        $user=$query->result_array();
        return (empty($user)?null:$user[0]['id']);
    }

    /*
     * Update user information
     */
    function updateUser($id, $username, $email, $groupId, $password=null){
        $request = "UPDATE `".$this->db->dbprefix."users` "."\n"
            ."SET `username`='".mysql_real_escape_string($username)."', "."\n"
            ."`email`='".mysql_real_escape_string($email)."', "."\n"
            ."`groupId`=".(int)$groupId." "."\n"
            .(!is_null($password)?", `password`='".md5($password)."' "."\n":"")
            ."WHERE `id`=".(int)$id;
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Create a new user
     */
    function createUser($username, $email, $groupId, $password){
        $request="INSERT INTO `".$this->db->dbprefix."users` "."\n"
            ."(`id`, `username`, `email`, `groupId`, `password`) VALUES "."\n"
            ."(NULL, '".mysql_real_escape_string($username)."'"."\n"
            .", '".mysql_real_escape_string($email)."', ".(int)$groupId.", '".md5($password)."')";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete user
     */
    function deleteUser($id){
        $request="DELETE FROM `".$this->db->dbprefix."users` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get group info
     */
    function getGroup($id){
        $request="SELECT `groups`.`id`, `groups`.`name`, `groups`.`description` "."\n"
            ."FROM `".$this->db->dbprefix."groups` as `groups`"."\n"
            ."WHERE `groups`.`id`=".(int)$id;
        $query = $this->db->query($request);
        $group=$query->result_array();
        return (empty($group)?array():$group[0]);
    }

    /*
     * Update group information
     */
    function updateGroup($id, $name, $description){
        $request = "UPDATE `".$this->db->dbprefix."groups` "."\n"
            ."SET `name`='".mysql_real_escape_string($name)."', "."\n"
            ."`description`='".mysql_real_escape_string($description)."' "."\n"
            ."WHERE `id`=".(int)$id;
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Create a new group
     */
    function createGroup($name, $description){
        $request="INSERT INTO `".$this->db->dbprefix."groups` "."\n"
            ."(`id`, `name`, `description`) VALUES "."\n"
            ."(NULL, '".mysql_real_escape_string($name)."'"."\n"
            .", '".mysql_real_escape_string($description)."')";
        $query = $this->db->query($request);
        if($query!==false){
            $groupId=$this->db->insert_id();
            return $groupId;
        }
        return $query;
    }

    /*
     * Delete group
     */
    function deleteGroup($id){
        $request="DELETE FROM `".$this->db->dbprefix."groups` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get users
     */
    function getUsers($groupId=null){
        $request="SELECT `users`.`id`, `users`.`username`, `users`.`email`, `groups`.`name` as `groupName`"."\n"
            ."FROM `".$this->db->dbprefix."users` as `users`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."groups` as `groups`"."\n"
            ."ON `users`.`groupId` = `groups`.`id` "."\n"
            .(!is_null($groupId)?"WHERE `groups`.`id`=".(int)$groupId:"");
        $query = $this->db->query($request);
        $users=$query->result_array();
        return $users;
    }

    /*
     * Get groups
     */
    function getGroups(){
        $request="SELECT `groups`.`id`, `groups`.`name`, `groups`.`description`, COUNT(`users`.`id`) as `nbusers` "."\n"
            ."FROM `".$this->db->dbprefix."groups` as `groups`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."users` as `users`"."\n"
            ."ON `users`.`groupId` = `groups`.`id`"."\n"
            ."WHERE `groups`.`id`>1"."\n"
            ."GROUP BY `groups`.`id` ";
        $query = $this->db->query($request);
        $users=$query->result_array();
        return $users;
    }

    /*
     * Get all permissions
     */
    function getAllPermissions(){
        $request='SELECT `permissions`.`id`, `modules`.`name` as `moduleName`, `permissions`.`name`'."\n"
            .'FROM `'.$this->db->dbprefix.'permissions` as `permissions`'."\n"
            .'LEFT JOIN `'.$this->db->dbprefix.'modules` as `modules`'."\n"
            .'ON `modules`.`id`=`permissions`.`moduleId`'."\n"
            .'WHERE `permissions`.`id`>2';
        $query = $this->db->query($request);
        $permissions=$query->result_array();
        return $permissions;
    }

    /*
     * Get permissions for the group
     */
    function getPermissions($groupId){
        $request='SELECT `permissions`.`id`, `permissions`.`moduleId`, `modules`.`name` as `moduleName`, `permissions`.`shortname`'."\n"
            .'FROM `'.$this->db->dbprefix.'groups_permissions` as `groups_permissions`'."\n"
            .'INNER JOIN `'.$this->db->dbprefix.'permissions` as `permissions`'."\n"
            .'ON `permissions`.`id`=`groups_permissions`.`permissionId`'."\n"
            .'LEFT JOIN `'.$this->db->dbprefix.'modules` as `modules`'."\n"
            .'ON `modules`.`id`=`permissions`.`moduleId`'."\n"
            .'WHERE `groupId`='.(int)$groupId;
        $query = $this->db->query($request);
        $permissions=$query->result_array();
        return $permissions;
    }

    /*
     * Remove permissions of a group
     */
    function clearGroupPermissions($id){
        $request='DELETE FROM `'.$this->db->dbprefix.'groups_permissions` '."\n"
            .'WHERE `groupId`='.(int)$id;
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Create permission
     */
    function createPermission($moduleId, $shortname, $name){
		$request="INSERT INTO `".$this->db->dbprefix."permissions` (`id`, "
            ."`moduleId`, `shortname`, `name`) VALUES ("."\n"
			."NULL, "
			.(is_null($moduleId)?"NULL":(int)$moduleId).", "
			."'".mysql_real_escape_string($shortname)."', "
			."'".mysql_real_escape_string($name)."' "
			.");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
        }
        else
            return false;
        $request="INSERT INTO `".$this->db->dbprefix."groups_permissions`"."\n"
            ."(`groupId`, `permissionId`) "."\n"
            ."VALUES (1, ".(int)$id.")";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Add permissions to a group
     */
    function addGroupPermissions($id, $permissions){
        $inserts = array();
        foreach($permissions as $permission)
            if((int)$permission>2)
                $inserts[]="(".(int)$id." ,".(int)$permission.")";
        $request="INSERT INTO `".$this->db->dbprefix."groups_permissions`"."\n"
            ."(`groupId`, `permissionId`) "."\n"
            ."VALUES ".implode($inserts, ', ');
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Check username and password
     */
    function checkUsernamePassword ($username, $password){
        $request='SELECT `id`, `username`, `groupId` '."\n"
            .'FROM `'.$this->db->dbprefix.'users` '."\n"
            .'WHERE `username`="'.mysql_real_escape_string(strtolower($username)).'" '."\n"
            .'AND `password`="'.mysql_real_escape_string(md5($password)).'"';
        $query = $this->db->query($request);
        $user=$query->result_array();
        return $user;
    }

    /*
     * Create a new session for the user
     */
    function createSession($userId, $ipaddr){
        $session_id = md5(uniqid(rand(), true));
        $request='INSERT INTO `'.$this->db->dbprefix.'sessions` '."\n"
            .'(`id`, `userId`, `startTime`, `lastTime`, `lastIP`) VALUES '."\n"
            .'("'.$session_id.'", "'.(int)$userId.'", UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), "'.$ipaddr.'")';
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get existing sessions for userId (should be 1 maximum)
     */
    function getSession($userId){
        $request='SELECT `id`, `lastIP`, UNIX_TIMESTAMP() - `lasttime` as `diffTime` '."\n"
            .'FROM `'.$this->db->dbprefix.'sessions` '."\n"
            .'WHERE `userId`='.(int)$userId;
        $query = $this->db->query($request);
        $session=$query->result_array();
        //protection against double sessions
        if(count($session)>1){
            $this->clearSession($userId);
            $session=array();
        }
        return $session;
    }

    /*
     * Update the session time to current time
     */
    function updateSessionTime($userId){
        $request = 'UPDATE `'.$this->db->dbprefix.'sessions` '."\n"
            .'SET `lastTime`=UNIX_TIMESTAMP() '."\n"
            .'WHERE `userId`='.(int)$userId;
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Remove session
     */
    function clearSession($userId){
        $request='DELETE FROM `'.$this->db->dbprefix.'sessions` '."\n"
            .'WHERE `userId`='.(int)$userId;
        $query = $this->db->query($request);
        return $query;
    }

} 

?>
