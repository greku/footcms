<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Reportmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get reports of the season
     */
    function getReports($limitStart=0, $limitMax=20, $seasonId=null) {
        $request="SELECT `matches`.`id`, `news`.`date`, `news`.`title`"."\n"
            ."FROM `".$this->db->dbprefix."news` AS `news`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`id1`=`news`.`id`"."\n"
            ."AND `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."INNER JOIN `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."ON `matches`.`id`=`interlinks`.`id2`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."ON `competitions`.`id`=`matches`.`competitionId`"."\n"
            .(!is_null($seasonId)?"WHERE `competitions`.`seasonId`=".(int)$seasonId."\n":"")
            ."ORDER BY `matches`.`date` DESC"."\n"
            ."LIMIT ".(int)$limitStart.", ".(int)$limitMax;
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Count reports
     */
    function count($seasonId=null) {
         $request="SELECT COUNT(`matches`.`id`) as `cnt`"."\n"
            ."FROM `".$this->db->dbprefix."news` AS `news`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`id1`=`news`.`id`"."\n"
            ."AND `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."INNER JOIN `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."ON `matches`.`id`=`interlinks`.`id2`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."ON `competitions`.`id`=`matches`.`competitionId`"."\n"
            .(!is_null($seasonId)?"WHERE `competitions`.`seasonId`=".(int)$seasonId."\n":"");
        $query = $this->db->query($request);
        $result = $query->result_array();
        return $result[0]['cnt'];
    }

    /*
     * Get report
     */
    function getReport($id){
        $request="SELECT `matches`.`id`,`matches`.`date`,`news`.`title`, `news`.`content`, "."\n"
            ."`users`.`username` as `author`, `interlinks`.`id1` AS `newsId`"."\n"
            ."FROM `".$this->db->dbprefix."news` as `news`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`id1`=`news`.`id`"."\n"
            ."AND `interlinks`.`type`='NEWS_MATCH'"."\n"
            ."INNER JOIN `".$this->db->dbprefix."matches` AS `matches`"."\n"
            ."ON `matches`.`id`=`interlinks`.`id2`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."users` as `users`"."\n"
            ."ON `users`.`id` = `news`.`authorId`"."\n"
            ."WHERE `matches`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $report=$query->result_array();
        return (isset($report[0])?$report[0]:false);
    }

    /*
     * Create a report
     */
    function createReport($matchId, $title, $content, $authorId){
		$request="INSERT INTO `".$this->db->dbprefix."news` (`id`, `date`, `authorId`, `title`, `content`) VALUES ("."\n"
			."NULL, "
			."NOW(), "
			.(int)$authorId.", "
			."'".mysql_real_escape_string($title)."', "
			."'".mysql_real_escape_string($content)."' "
			.");";
        $query = $this->db->query($request);
        if ($query===false)
            return false;
        $newsId=$this->db->insert_id();
        $request="INSERT INTO `".$this->db->dbprefix."interlinks` "."\n"
            ."(`id`, `type`, `id1`, `id2`) "."\n"
            ."VALUES (NULL, 'NEWS_MATCH', ".(int)$newsId.", ".(int)$matchId.");";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete report
     */
    function deleteReport($matchId, $newsId){
        $request="DELETE FROM `".$this->db->dbprefix."interlinks` "."\n"
            ."WHERE `id1`=".(int)$newsId."\n"
            ."AND `id2`=".(int)$matchId."\n"
            ."AND `type`='NEWS_MATCH'";
        $query = $this->db->query($request);
        if ($query === false)
            return false;
        $request="DELETE FROM `".$this->db->dbprefix."news` "."\n"
            ."WHERE `id`=".(int)$newsId."\n";
        $query = $this->db->query($request);
        return $query;
    }
}

?>
