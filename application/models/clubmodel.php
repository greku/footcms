<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Clubmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get club
     */
    function getClub($id){
        $request="SELECT `clubs`.`id`, `clubs`.`name`, `clubs`.`matricule` "."\n"
            ."FROM `".$this->db->dbprefix."clubs` AS `clubs`"."\n"
            ."WHERE `clubs`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $club = $query->result_array();
        return (empty($club)?array():$club[0]);
    }

    /*
     * Get club from matricule
     */
    function getClubFromMatricule($matricule){
        $request="SELECT `clubs`.`id`, `clubs`.`name`, `clubs`.`matricule` "."\n"
            ."FROM `".$this->db->dbprefix."clubs` AS `clubs`"."\n"
            ."WHERE `clubs`.`matricule`='".mysql_real_escape_string($matricule)."'"."\n";
        $query = $this->db->query($request);
        $club = $query->result_array();
        return (empty($club)?array():$club[0]);
    }

    /*
     * Get club empty
     */
    function getClubEmpty(){
        $club = array('id'=>'',
            'name'=>'',
            'matricule'=>'');
        return $club;
    }

    /*
     * Update club information
     */
    function updateClub($id, $name, $matricule){
        $request="UPDATE `".$this->db->dbprefix."clubs` SET "."\n"
            ."`name` = '".mysql_real_escape_string($name)."'"."\n"
            .(!is_null($matricule)?", `matricule` = '".mysql_real_escape_string($matricule)."'"."\n":'')
            ."WHERE `id` =".(int)$id." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Create a new club
     */
    function createClub($name, $matricule){
        $request="INSERT INTO `".$this->db->dbprefix."clubs` (`id`, `name`, `matricule`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($name)."', "
            ."'".mysql_real_escape_string($matricule)."'"
            .");";
        $query = $this->db->query($request);
        if($query===false)
            return false;
        return $this->db->insert_id();
    }

    /*
     * Delete club
     */
    function deleteClub($id){
        $request="DELETE FROM `".$this->db->dbprefix."clubs` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get clubs
     */
    function getClubs(){
        $request="SELECT `clubs`.`id`, `clubs`.`name`, `clubs`.`matricule`"."\n"
            ."FROM `".$this->db->dbprefix."clubs` AS `clubs`"."\n"
            ."ORDER BY `clubs`.`name` ASC";
        $query = $this->db->query($request);
        $clubs = $query->result_array();
        return $clubs;
    }

}

?>
