<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Competitionmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get competitions
     */
    function getCompetitions($seasonId){
        $request="SELECT `competitions`.`id`, `competitions`.`name`, `competitions`.`typeId`, `competitions_type`.`name` AS `typeName`"."\n"
            ."FROM `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."type_competition` AS `competitions_type`"."\n"
            ."ON `competitions`.`typeId`=`competitions_type`.`id`"."\n"
            ."WHERE `competitions`.`seasonId`=".(int)$seasonId."\n"
            ."ORDER BY `competitions`.`name` ASC";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get competition empty
     */
    function getCompetitionEmpty(){
        $place = array('id'=>'',
            'name'=>'',
            'typeId'=>'',
            'typeName'=>'',
            'seasonId'=>'');
        return $place;
    }

    /*
     * Get a competition
     */
    function getCompetition($id){
        $request="SELECT `competitions`.`id`, `competitions`.`name`, `competitions`.`typeId`, `type_competition`.`name` AS `typeName`"."\n"
            ."FROM `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."type_competition` AS `type_competition`"."\n"
            ."ON `competitions`.`typeId`=`type_competition`.`id`"."\n"
            ."WHERE `competitions`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $competition = $query->result_array();
        return (empty($competition)?array():$competition[0]);
    }

    /*
     * Update competition
     */
    function updateCompetition($id, $name){
        $request="UPDATE `".$this->db->dbprefix."competitions` SET "."\n"
            ."`name` = '".mysql_real_escape_string($name)."' "."\n"
            ."WHERE `id` =".(int)$id." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Create competition
     */
    function createCompetition($name, $type, $seasonId){
        $request="INSERT INTO `".$this->db->dbprefix."competitions` (`id`, `name`, `typeId`, `seasonId`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($name)."', "
            .(int)$type.", "
            .(int)$seasonId." "
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Delete competition
     */
    function deleteCompetition($id){
        $request="DELETE FROM `".$this->db->dbprefix."competitions` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get competition types
     */
    function getCompetitionTypes(){
        $request="SELECT `id`, `name`"."\n"
            ."FROM `".$this->db->dbprefix."type_competition` AS `type_competition`"."\n"
            ."ORDER BY `name` ASC";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get teams in a competition
     */
    function getTeams($id) {
        $request="SELECT `teams`.`id`, `teams`.`name`, `teams`.`clubId` "."\n"
            ."FROM `".$this->db->dbprefix."teams_competitions` AS `teams_competitions`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams` AS `teams`"."\n"
            ."ON `teams`.`id`=`teams_competitions`.`teamId`"
            ."WHERE `teams_competitions`.`competitionId`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get teams not in the competition
     */
    function getTeamsNotInCompetition($competitionId, $seasonId) {
        $request="SELECT `teams`.`id`, `teams`.`name`, "."\n"
            ."BIT_OR(IF(`teams_competitions`.`competitionId`=".(int)$competitionId.", 1, 0)) as `notGood`"."\n"
            ."FROM `".$this->db->dbprefix."teams` AS `teams`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."teams_competitions` AS `teams_competitions`"."\n"
            ."ON `teams`.`id`=`teams_competitions`.`teamId`"."\n"
            ."WHERE `teams`.`seasonId`=".(int)$seasonId."\n"
            ."GROUP BY `teams`.`id`"."\n"
            ."HAVING `notGood`=0"."\n"
            ."ORDER BY `teams`.`name` ASC";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Add a team to the competition
     */
    function addTeam($competitionId, $teamId){
        $request="INSERT INTO `".$this->db->dbprefix."teams_competitions` (`competitionId`, `teamId`) VALUES ("."\n"
            .(int)$competitionId.", "
            .(int)$teamId
            .");";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Remove team
     */
    function removeTeam($competitionId, $teamId){
        //does not remove matches of the team in the competition!
        //delete from competitions
        $request="DELETE FROM `".$this->db->dbprefix."teams_competitions` "."\n"
            ."WHERE `competitionId`=".(int)$competitionId." AND `teamId`=".(int)$teamId."\n";
        $query = $this->db->query($request);
        return $query;
    }
}

?>
