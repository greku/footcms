<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class Newsmodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* 
     * Count news
     */
    function count(){
        $request="SELECT COUNT(`news`.`id`) as `cnt`"."\n"
            ."FROM `".$this->db->dbprefix."news` as `news`"."\n";
        $query = $this->db->query($request);
        $result = $query->result_array();
        return $result[0]['cnt'];
    }
    
    /* 
     * Get list of news
     */
    function getList($limitStart=0, $limitMax=10, $noShortText=false, $noReports=false){//todo add limit in request and params
        $request="SELECT `news`.`id`,`news`.`date`,`title`,"."\n"
            .($noShortText?'':"`content`,"."\n")
            ."COUNT(`comments`.`id`) as `nbcomments`,"."\n"
            ."`interlinks2`.`id2` as `matchId` "."\n"
            ."FROM `".$this->db->dbprefix."news` as `news`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` as `interlinks`"."\n"
            ."ON `news`.`id` = `interlinks`.`id2`"."\n"
            ."AND `interlinks`.`type`='COMMENT_NEWS'"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."comments` as `comments`"."\n"
            ."ON `interlinks`.`id1` = `comments`.`id`"."\n"
            ."AND `comments`.`validated`='YES'"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` AS `interlinks2`"."\n"
            ."ON `news`.`id` = `interlinks2`.`id1`"."\n"
            ."AND `interlinks2`.`type` = 'NEWS_MATCH'"."\n"
            .($noReports==true?"WHERE `interlinks2`.`id1` IS NULL"."\n":'')
            ."GROUP BY `news`.`id`"."\n"
            ."ORDER BY `date` DESC"."\n"
            ."LIMIT ".(int)$limitStart.", ".(int)$limitMax;
        $query = $this->db->query($request);
        $news=$query->result_array();
        if(!$noShortText){
            foreach ($news as &$item){
                //take 350 characters
                //PREG s makes the dot match all charcacters, ? to take minimum of range
                //preg_match('/.{350,1000}?\./s', strip_tags($item['content']), $arr2);
                preg_match('/.{0,350}[ .\w]/s', strip_tags($item['content']), $arr2);
                $item['shorttext']=(count($arr2)>0?$arr2[0]:strip_tags($item['content']));
                unset($item['content']);
            }
        }
        return $news;
    }

    /*
     * Get place empty
     */
    function getNewsEmpty(){
		$place = array('id'=>'',
			'date'=>'',
			'title'=>'',
			'author'=>'',
			'content'=>'',
			'nbcomments'=>'');
        return $place;
    }

    /*
     * Get news, content and comments 
     */
    function get($id){
        $request="SELECT `news`.`id`,`news`.`date`,`news`.`title`,`users`.`username` as `author`, `news`.`content`, COUNT(`comments`.`id`) as `nbcomments` "."\n"
            ."FROM `".$this->db->dbprefix."news` as `news`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."users` as `users`"."\n"
            ."ON `users`.`id` = `news`.`authorId`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` as `interlinks`"."\n"
            ."ON `news`.`id` = `interlinks`.`id2`"."\n"
            ."AND `interlinks`.`type`='COMMENT_NEWS'"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."comments` as `comments`"."\n"
            ."ON `interlinks`.`id1` = `comments`.`id`"."\n"
            ."AND `comments`.`validated`='YES'"."\n"
            .($id=='last'?'':"WHERE `news`.`id`=".(int)$id."\n")
            ."GROUP BY `news`.`id`"."\n"
            ."LIMIT 0, 1";
        $query = $this->db->query($request);
        $news=$query->result_array();
        return (isset($news[0])?$news[0]:false);
    }

    /*
     * Get news without content
     */
    function getBasic($id){
        $request="SELECT `news`.`id`,`news`.`date`,`news`.`title`,`users`.`username` as `author`"."\n"
            ."FROM `".$this->db->dbprefix."news` as `news`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."users` as `users`"."\n"
            ."ON `users`.`id` = `news`.`authorId`"."\n"
            ."WHERE `news`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $news=$query->result_array();
        return (isset($news[0])?$news[0]:false);
    }

    /*
     * Create a news
     */
    function createNews($title, $content, $authorId){
		$request="INSERT INTO `".$this->db->dbprefix."news` (`id`, `date`, `authorId`, `title`, `content`) VALUES ("."\n"
			."NULL, "
			."NOW(), "
			.(int)$authorId.", "
			."'".mysql_real_escape_string($title)."', "
			."'".mysql_real_escape_string($content)."' "
			.");";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Update news
     */
    function updateNews($id, $title, $content){
		$request="UPDATE `".$this->db->dbprefix."news` SET "."\n"
			."`title` = '".mysql_real_escape_string($title)."', "."\n"
			."`content` = '".mysql_real_escape_string($content)."' "."\n"
			."WHERE `id` =".(int)$id." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete news
     */
    function deleteNews($id){
        $request="DELETE FROM `".$this->db->dbprefix."news` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }
} 

?>
