<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Teammodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get team
     */
    function getTeam($id) {
        $request="SELECT `teams`.`id`, `teams`.`name`, `teams`.`contactName`,"."\n"
            ." `teams`.`contactPhone`, `teams`.`contactEmail`, `teams`.`placeId`, `teams`.`clubId`,"."\n"
            ."`places`.`name` as `placeName`, `clubs`.`name` as `clubName`"."\n"
            ."FROM `".$this->db->dbprefix."teams` AS `teams`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."clubs` AS `clubs`"."\n"
            ."ON `clubs`.`id`=`teams`.`clubId`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."places` AS `places`"."\n"
            ."ON `places`.`id`=`teams`.`placeId`"."\n"
            ."WHERE `teams`.`id`=".(int)$id;
        $query = $this->db->query($request);
        $team = $query->result_array();
        return (empty($team)?array():$team[0]);
    }

    /*
     * Get team picture
     */
    function getTeamPicture($id) {
        $request="SELECT `files`.`id`, `files`.`name`, `files`.`path` "."\n"
            ."FROM `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."files` as `files`"."\n"
            ."ON `files`.`id` = `interlinks`.`id1`"."\n"
            ."AND `interlinks`.`type`='PHOTO_TEAM'"."\n"
            ."WHERE `interlinks`.`id2`=".(int)$id;
        $query = $this->db->query($request);
        $picture = $query->result_array();
        return (empty($picture)?array():$picture[0]);
    }

    /*
     * Remove team picture
     */
    function removePicture($teamId){
        $request="DELETE FROM `".$this->db->dbprefix."interlinks` "."\n"
            ."WHERE `type`='PHOTO_TEAM'"."\n"
            ."AND `id2`=".(int)$teamId."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Add team picture
     */
    function addPicture($teamId, $fileId){
        $request="INSERT INTO `".$this->db->dbprefix."interlinks` (`id`, `type`, `id1`, `id2`) VALUES ("."\n"
            ."NULL, "
            ."'PHOTO_TEAM', "
            .(int)$fileId.", "
            .(int)$teamId
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Update team picture
     */
    function updatePicture($teamId, $fileId){
        $request="UPDATE `".$this->db->dbprefix."interlinks` SET "."\n"
            ."`id1` = ".(int)$fileId." "."\n"
            ."WHERE `type`='PHOTO_TEAM'"."\n"
            ."AND `id2`=".(int)$teamId."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get team empty
     */
    function getTeamEmpty(){
        $team = array('id'=>'',
            'name'=>'',
            'contactName'=>'',
            'contactPhone'=>'',
            'contactEmail'=>'',
            'placeId'=>null,
            'clubId'=>'');
        return $team;
    }

    /*
     * Create a new team
     */
    function createTeam($name, $contactName, $contactPhone, $contactEmail, $placeId, $clubId, $seasonId){
        $request="INSERT INTO `".$this->db->dbprefix."teams` (`id`, `name`, `contactName`, `contactPhone`, `contactEmail`, `placeId`, `clubId`, `seasonId`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($name)."', "
            ."'".mysql_real_escape_string($contactName)."', "
            ."'".mysql_real_escape_string($contactPhone)."', "
            ."'".mysql_real_escape_string($contactEmail)."', "
            ."".(is_null($placeId)?"NULL":(int)$placeId).", "
            .(int)$clubId.", "
            .(int)$seasonId
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Update team information
     */
    function updateTeam($id, $name, $contactName, $contactPhone, $contactEmail, $placeId){
        $request="UPDATE `".$this->db->dbprefix."teams` SET "."\n"
            ."`name` = '".mysql_real_escape_string($name)."', "."\n"
            ."`contactName` = '".mysql_real_escape_string($contactName)."', "."\n"
            ."`contactPhone` = '".mysql_real_escape_string($contactPhone)."', "."\n"
            ."`contactEmail` = '".mysql_real_escape_string($contactEmail)."', "."\n"
            ."`placeId` = ".(is_null($placeId)?"NULL":(int)$placeId)."\n"
            ."WHERE `id` =".(int)$id." LIMIT 1 ;";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Delete team
     */
    function deleteTeam($id){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."goals` "."\n"
            ."WHERE `teamId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete cards
        $request="DELETE FROM `".$this->db->dbprefix."cards` "."\n"
            ."WHERE `teamId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete players_matches
        $request="DELETE FROM `".$this->db->dbprefix."players_matches` "."\n"
            ."WHERE `teamId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete from competitions
        $request="DELETE FROM `".$this->db->dbprefix."teams_competitions` "."\n"
            ."WHERE `teamId`=".(int)$id."\n";
        $query = $this->db->query($request);
        //delete team
        $request="DELETE FROM `".$this->db->dbprefix."teams` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Add player to team
     */
    function addPlayer($teamId, $playerId, $number=null){
        $request="INSERT INTO `".$this->db->dbprefix."players_teams` (`playerId`, `teamId`, `number`) VALUES ("."\n"
            .(int)$playerId.", "
            .(int)$teamId.", "
            .(is_null($number)?"NULL":(int)$number)
            .");";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Update player number
     */
    function updatePlayerNumber($teamId, $playerId, $number=null){
        $request="UPDATE `".$this->db->dbprefix."players_teams` SET "."\n"
            ."`number` = ".(is_null($number)?"NULL":(int)$number)."\n"
            ."WHERE `teamId` =".(int)$teamId."\n"
            ."AND `playerId`=".(int)$playerId;
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Remove player form team
     */
    function removePlayer($teamId, $playerId){
        //delete goals
        $request="DELETE FROM `".$this->db->dbprefix."goals` "."\n"
            ."WHERE `teamId`=".(int)$teamId."\n"
            ."AND `playerId`=".(int)$playerId."\n";
        $query = $this->db->query($request);
        //delete cards
        $request="DELETE FROM `".$this->db->dbprefix."cards` "."\n"
            ."WHERE `teamId`=".(int)$teamId."\n"
            ."AND `playerId`=".(int)$playerId."\n";
        $query = $this->db->query($request);
        //delete players_matches
        $request="DELETE FROM `".$this->db->dbprefix."players_matches` "."\n"
            ."WHERE `teamId`=".(int)$teamId."\n"
            ."AND `playerId`=".(int)$playerId."\n";
        $query = $this->db->query($request);
        //delete players_teams
        $request="DELETE FROM `".$this->db->dbprefix."players_teams` "."\n"
            ."WHERE `playerId`=".(int)$playerId."\n"
            ."AND `teamId`=".(int)$teamId;
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get player number
     */
    function getPlayerNumber($teamId, $playerId) {
        $request="SELECT `players_teams`.`teamId`, `players_teams`.`playerId`, `players_teams`.`number`"."\n"
            ."FROM `".$this->db->dbprefix."players_teams` AS `players_teams`"."\n"
            ."WHERE `players_teams`.`teamId`=".(int)$teamId."\n"
            ."AND `players_teams`.`playerId`=".(int)$playerId;
        $query = $this->db->query($request);
        $team = $query->result_array();
        return (empty($team)?array():$team[0]);
    }

    function getTeams($clubId, $seasonId=null){
        //teams of the club this season
        $request="SELECT `teams`.`id`,`teams`.`name`, "."\n"
            ."CONCAT('upload',`files`.`path`) AS `photopath` "."\n"
            ."FROM `".$this->db->dbprefix."teams` AS `teams`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."interlinks` AS `interlinks`"."\n"
            ."ON `interlinks`.`id2`=`teams`.`id` "."\n"
            ."AND `interlinks`.`type`='PHOTO_TEAM'"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."files` AS `files`"."\n"
            ."ON `files`.`id`=`interlinks`.`id1`"."\n"
            ."WHERE `clubId`=".(int)$clubId."\n";
        if(!is_null($seasonId))
            $request.="AND `seasonId`=".(int)$seasonId."\n";
        $request.="ORDER BY `name` ASC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    function getTeamsInfo($clubId, $seasonId=null) {
        $request="SELECT `teams`.`id`,`teams`.`name`, `teams`.`contactName`, "."\n"
            ."`teams`.`contactPhone`, `teams`.`contactEmail`, "."\n"
            ."`places`.`id` as `placeId`, `places`.`name` as `placeName`"."\n"
            ."FROM `".$this->db->dbprefix."teams` AS `teams`"."\n"
            ."LEFT JOIN `".$this->db->dbprefix."places` AS `places`"."\n"
            ."ON `places`.`id`=`teams`.`placeId`"."\n"
            ."WHERE `clubId`=".(int)$clubId."\n";
        if(!is_null($seasonId))
            $request.="AND `seasonId`=".(int)$seasonId."\n";
        $request.="ORDER BY `teams`.`name` ASC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get competitions of the team
     */
    function getCompetitions($teamId) {
        $request="SELECT `competitions`.`id`, `competitions`.`name`"."\n"
            ."FROM `".$this->db->dbprefix."competitions` AS `competitions`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."teams_competitions` AS `teams_competitions`"."\n"
            ."ON `teams_competitions`.`competitionId`=`competitions`.`id`"."\n"
            ."AND `teams_competitions`.`teamId`=".(int)$teamId."\n"
            ."ORDER BY `competitions`.`name` ASC"."\n";
        $query = $this->db->query($request);
        return $query->result_array();
    }

    /*
     * Get players not in team
     */
    function getPlayersNotInTeam($id) {
        $request="SELECT `players`.`id`,"."\n"
            ."`players`.`lastName`, `players`.`firstName`,"."\n"
            ."BIT_OR(IF(`players_teams`.`teamId`=".(int)$id.", 1, 0)) as `notGood`"."\n"
            ."FROM `".$this->db->dbprefix."players` as `players` "."\n"
            ."LEFT JOIN `".$this->db->dbprefix."players_teams` as `players_teams`"."\n"
            ."ON `players`.`id` = `players_teams`.`playerId`"."\n"
            ."GROUP BY `players`.`id`"."\n"
            ."HAVING `notGood`=0";
        $query = $this->db->query($request);
        return $query->result_array();
    }
}

?>
