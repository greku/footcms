<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright 2011 Gregory Baudet
 *
 * This file is part of footcms
 * footcms is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * footcms is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with footcms.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class Filemodel extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Get children
     */
    function getChildren($parent, $type='all'){

        $pictureWhere="`file_types`.`mime`='image/jpeg' "
                    ."OR `file_types`.`mime`='image/png' "
                    ."OR `file_types`.`mime`='image/gif' "
                    ."OR `file_types`.`mime`='image/pjpeg'"
                    ."OR `file_types`.`mime`='folder'";

        $request="SELECT `files`.`id`, `files`.`type`, `files`.`date`, `files`.`name`, "."\n"
            ."`files`.`path`, IF(`files`.`type`=1, 1, 0) AS `isDirectory`, `file_types`.`mime`"."\n"
            ."FROM `".$this->db->dbprefix."files` AS `files`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."file_types` AS `file_types`"."\n"
            ."ON `files`.`type`=`file_types`.`id`"."\n"
            ."WHERE `files`.`parent`=".(int)$parent."\n"
            .($type=='picture'?"AND (".$pictureWhere.")":"")."\n"
            ."ORDER BY `isDirectory` DESC, `files`.`name`";
        $query = $this->db->query($request);
        $children = $query->result_array();
        return $children;
    }

    /*
     * Get folder
     */
    function getFolder($id){
        $request="SELECT `files`.`id`,`files`.`name`, `files`.`parent`, `files`.`path`"."\n"
            ."FROM `".$this->db->dbprefix."files` AS `files`"."\n"
            ."WHERE `files`.`type`=1 AND `files`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $dir = $query->result_array();
        return (empty($dir)?array():$dir[0]);
    }

    /*
     * Get file
     */
    function getFile($id){
        $request="SELECT `files`.`id`,`files`.`name`, `files`.`parent`"."\n"
            .", `files`.`path`, `files`.`type`, `file_types`.`mime`"."\n"
            ."FROM `".$this->db->dbprefix."files` AS `files`"."\n"
            ."INNER JOIN `".$this->db->dbprefix."file_types` AS `file_types`"."\n"
            ."ON `files`.`type`=`file_types`.`id`"."\n"
            ."WHERE `files`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $file = $query->result_array();
        return (empty($file)?array():$file[0]);
    }

    /*
     * Delete file or folder
     */
    function deleteFile($id){
        $request="SELECT `files`.`name`, `files`.`path`, `files`.`type` "."\n"
            ."FROM `".$this->db->dbprefix."files` AS `files`"."\n"
            ."WHERE `files`.`id`=".(int)$id."\n";
        $query = $this->db->query($request);
        $file = $query->result_array();
        if (empty($file))
            return false;
        $file=$file[0];
        if ($file['type']==1){
            $dir = str_replace('system/','', BASEPATH).'upload'.$file['path'];
            foreach (glob($dir.'/*') as $file) {
                if (is_dir($file)) {
                    rmrf("$file/*");
                    rmdir($file);
                } else {
                    unlink($file);
                }
            }
            if(!rmdir($dir))
                return false;
        }
        else {
            $destinationthumb = str_replace('system/','', BASEPATH).'upload'.substr( $file['path'], 0, strrpos($file['path'], '/')).'/thumbs/'.$file['name'];
            if(file_exists($destinationthumb))
                unlink($destinationthumb);
            if(!unlink(str_replace('system/','', BASEPATH).'upload'.$file['path']))
                return false;
        }
        $request="DELETE FROM `".$this->db->dbprefix."files` "."\n"
            ."WHERE `id`=".(int)$id."\n";
        $query = $this->db->query($request);
        return $query;
    }

    /*
     * Get allowed types
     */
    function getTypesAllowed(){
        $request="SELECT `file_types`.`id`, `file_types`.`mime`"."\n"
            ."FROM `".$this->db->dbprefix."file_types` AS `file_types`"."\n"
            ."WHERE `file_types`.`uploadAllowed`=1 "."\n";
        $query = $this->db->query($request);
        $types = $query->result_array();
        return $types;
    }

    /*
     * Add file
     */
    function addFile($filename, $filetype, $folderId, $path){
        $request="INSERT INTO `".$this->db->dbprefix."files` (`id`, `name`, `type`, `parent`, `path`, `date`) VALUES ("."\n"
            ."NULL, "
            ."'".mysql_real_escape_string($filename)."', "
            .(int)$filetype.", "
            .(int)$folderId.", "
            ."'".mysql_real_escape_string($path)."', "
            ."NOW()"
            .");";
        $query = $this->db->query($request);
        if($query!==false){
            $id=$this->db->insert_id();
            return $id;
        }
        return $query;
    }

    /*
     * Create folder
     */
    function createFolder($parent, $name, $path){
        if(!@mkdir(str_replace('system/','', BASEPATH).'upload'.$path, 0775))
            return false;
        else{
            $request="INSERT INTO `".$this->db->dbprefix."files` (`id`, `name`, `type`, `parent`, `path`, `date`) VALUES ("."\n"
                ."NULL, "
                ."'".mysql_real_escape_string($name)."', "
                ."1, "
                .(int)$parent.", "
                ."'".mysql_real_escape_string($path)."', "
                ."NOW()"
                .");";
            $query = $this->db->query($request);
            if($query!==false){
                $id=$this->db->insert_id();
                return $id;
            }
            return $query;
        }
    }
}

?>
